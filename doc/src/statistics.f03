!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module statistics

!==================================================================================================================
! Description:
!! This module implements the data structures and the methods to collect flow statistics.
!==================================================================================================================

    use flubioDictionaries
    use fieldvar
    use globalMeshVar
    use physicalConstants
    use runTimeObject

    implicit none

    type, public, extends(runTimeObj) :: flowStats

        integer :: statsFlag
        !! flag to collect statistics

        integer :: fstats
        !! samplig frequency

        real :: tstats
        !! start collecting sample after tstats

        real :: tsample
        !! number of time/iteration samples

        type(flubioField) :: Umean
        !! mean valocity field

        type(flubioField) :: Pmean
        !! mean pressure field

        type(flubioField) :: ppmean
        !! pressure rms

        real, dimension(:,:), allocatable :: RST
        !! Reynolds stresses

    contains

        procedure :: initialise => createStatistics
        procedure :: run => updateStatistics

    end type flowStats

contains

! *********************************************************************************************************************

    subroutine createStatistics(this)

    !==================================================================================================================
    ! Description:
    !! createStatistics is the class constructor.
    !==================================================================================================================

        class(flowStats) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: optName
        !! option name
    !------------------------------------------------------------------------------------------------------------------

        logical :: found, monitorsFound, monitorFound
    !------------------------------------------------------------------------------------------------------------------

        type(json_core) :: jcore

        type(json_value), pointer :: dictPointer, monitorsPointer, monitorPointer
    !------------------------------------------------------------------------------------------------------------------

        ! Get the pointer to the dictionary
        call flubioMonitors%json%get(dictPointer)

        ! Initialize json factory
        call jcore%initialize()
        call jcore%get(dictPointer, 'Monitors', monitorsPointer, monitorsFound)

        ! Get monitors pointer
        call jcore%get_child(monitorsPointer, this%objName, monitorPointer, monitorFound)

        call jcore%get(monitorPointer, 'startAveraging', this%tstats, found)
        call raiseKeyErrorJSON('startAveraging', found)

        call jcore%get(monitorPointer, 'collectEvery',this%fstats, found)
        call raiseKeyErrorJSON('collectEvery', found)

        ! Allocate

        this%tsample = 0.0

        call this%Umean%createField(fieldName='Umean', classType='momentum', nComp=3)
        call this%Pmean%createField(fieldName='Pmean', classType='pressure', nComp=1)
        call this%ppmean%createField(fieldName='ppmean', classType='pressure', nComp=1)

        allocate(this%RST(numberOfElements+numberOfBFaces,6))
        this%RST = 0.0

        ! Clean up
        nullify(dictPointer)
        if(monitorsFound) nullify(monitorsPointer)
        if(monitorFound) nullify(monitorPointer)
        call jcore%destroy()

    end subroutine createStatistics

! *********************************************************************************************************************

    subroutine resetStatistics(this)

    !==================================================================================================================
    ! Description:
    !! resetStatistics resets the statistics.
    !==================================================================================================================

        class(flowStats) :: this
    !------------------------------------------------------------------------------------------------------------------

        this%tsample = 0.0

        this%Umean%phi = 0.0
        this%Pmean%phi = 0.0
        this%ppmean%phi = 0.0
        this%RST = 0.0

    end subroutine resetStatistics

! *********************************************************************************************************************

    subroutine destroyStatistics(this)

    !==================================================================================================================!
    ! Description:
    !! destroyStatistics deletes statistics from the memory.
    !==================================================================================================================!

        class(flowStats) :: this
    !------------------------------------------------------------------------------------------------------------------

        call this%Umean%destroyField()
        call this%Pmean%destroyField()
        call this%ppmean%destroyField()
        deallocate(this%RST)

    end subroutine destroyStatistics

! *********************************************************************************************************************

    subroutine updateStatistics(this)

    !==================================================================================================================
    ! Description:
    !! updateStatistics updates the flow statistics.
    !==================================================================================================================

        class(flowStats) :: this
    !------------------------------------------------------------------------------------------------------------------

        real :: up(numberOfElements+numberOfBFaces), vp(numberOfElements+numberOfBFaces), wp(numberOfElements+numberOfBFaces)

        real :: pp(numberOfElements+numberOfBFaces)
    !------------------------------------------------------------------------------------------------------------------
        
    !--------------------!
    ! Collect Statistics !
    !--------------------!

        if(time>this%tstats .and. mod(itime,this%fstats)==0) then

            this%tsample=this%tsample+1.0

            ! Velocity and pressure
            this%Umean%phi = (this%Umean%phi + velocity%phi)/this%tsample
            this%Pmean%phi = (this%Pmean%phi + pressure%phi)/this%tsample

            ! Reynolds stress tensor

            up=(velocity%phi(:,1)-this%Umean%phi(:,1))
            vp=(velocity%phi(:,2)-this%Umean%phi(:,2))
            wp=(velocity%phi(:,3)-this%Umean%phi(:,3))

            pp=(pressure%phi(:,1) - this%Pmean%phi(:,1))
            this%RST(:,1) = (this%RST(:,1) + up*up)/this%tsample
            this%RST(:,2) = (this%RST(:,2) + vp*vp)/this%tsample
            this%RST(:,3) = (this%RST(:,3) + wp*wp)/this%tsample
            this%RST(:,4) = (this%RST(:,4) + up*vp)/this%tsample
            this%RST(:,5) = (this%RST(:,5) + up*wp)/this%tsample
            this%RST(:,6) = (this%RST(:,6) + vp*wp)/this%tsample

            this%ppMean%phi(:,1) = (this%ppMean%phi(:,1) + pp)/this%tsample

        ! More entries here, if needed

        endif

    end subroutine updateStatistics

! *********************************************************************************************************************

end module statistics
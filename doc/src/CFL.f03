module CoField

    use flubioDictionaries, only: flubioMonitors, raiseKeyErrorJSON
    use globalMeshVar
    use globalTimeVar
    use meshvar, only: mesh
    use fieldvar, only: velocity
    use flubioMpi
    use physicalConstants, only: rho, nu
    use json_module
    use runTimeObject

    implicit none

    type, public, extends(runTimeObj) :: Co

        character(len=30) :: fieldName
        !! Name of the field

        integer :: nComp
        !! number of components

        real, dimension(:,:), allocatable :: phi
        !! field variable

    contains

        procedure :: initialise => createCo
        procedure :: run => computeCo
        procedure :: writeFieldFunction

    end type Co

contains
    
    subroutine createCo(this)

    !==================================================================================================================
    ! Description:
    !! createCo is the field constructor and it allocates the memory for a new flubioField.
    !==================================================================================================================

        class(Co) :: this
    !-------------------------------------------------------------------------------------------------------------------

        logical :: found, monitorsFound, monitorFound
    !------------------------------------------------------------------------------------------------------------------

        type(json_core) :: jcore

        type(json_value), pointer :: dictPointer, monitorsPointer, monitorPointer
    !------------------------------------------------------------------------------------------------------------------

        ! Get the pointer to the dictionary
        call flubioMonitors%json%get(dictPointer)

        ! Initialize json factory
        call jcore%initialize()
        call jcore%get(dictPointer, 'Monitors', monitorsPointer, monitorsFound)

        ! Get monitor pointer
        call jcore%get_child(monitorsPointer, this%objName, monitorPointer, monitorFound)

        call jcore%get(monitorPointer, 'saveEvery', this%runAt, found)
        call raiseKeyErrorJSON('saveEvery', found)

        this%fieldName = 'Co'
        this%nComp = 1
        allocate(this%phi(numberOfElements+numberOfBFaces,this%nComp))

        ! Clean up
        nullify(dictPointer)
        if(monitorsFound) nullify(monitorsPointer)
        if(monitorFound) nullify(monitorPointer)
        call jcore%destroy()

    end subroutine createCo

! *********************************************************************************************************************

    subroutine computeCo(this)

    !==================================================================================================================
    ! Description:
    !! computeCo computes the CFL number.
    !==================================================================================================================

        class(Co):: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, iFace, iBFace, iBoundary, iOwner, iNeighbour, iProc, is, ie, pNeigh
    !------------------------------------------------------------------------------------------------------------------

        real :: CFL(numberOfElements)
        !! total CFL

        real :: CFLc(numberOfElements)
        !! convective CFL

        real :: CFLv(numberOfElements)
        !! viscous CFL

        real :: Ueqv(numberOfElements)
        !! equivalnt velocity scale
    !------------------------------------------------------------------------------------------------------------------

        real :: LambdaC(3), LambdaV(3), u(3), dS(numberOfElements,3), coef, nuLocal, vol, Cvis
    !------------------------------------------------------------------------------------------------------------------

        if(mod(itime, this%runAt)==0) then

            Cvis = 2.0

            !-----------------------!
            ! Compute average areas !
            !-----------------------!

            dS = 0.0

            do iFace=1,numberOfIntFaces
                iOwner = mesh%owner(iFace)
                iNeighbour = mesh%neighbour(iFace)
                dS(iOwner,:) = dS(iOwner,:) + 0.5*abs(mesh%Sf(iFace,:))
                dS(iNeighbour,:) = dS(iNeighbour,:) + 0.5*abs(mesh%Sf(iFace,:))
            enddo

            ! Processor boundaries
            do iBoundary=1,numberOfProcBound

                iProc = mesh%boundaries%procBound(iBoundary)
                is = mesh%boundaries%startFace(iProc)
                ie = is+mesh%boundaries%nFace(iProc)-1
                pNeigh=0

                do iBFace=is,ie
                    pNeigh=pNeigh+1
                    iOwner = mesh%owner(iBFace)
                    dS(iOwner,:) = dS(iOwner,:) + 0.5*abs(mesh%Sf(iBFace,:))
                enddo
            enddo

            ! Real boundaries
            do iBoundary=1,mesh%boundaries%numberOfRealBound
                is = mesh%boundaries%startFace(iBoundary)
                ie = is+mesh%boundaries%nFace(iBoundary)-1
                do iBFace=is,ie
                    iOwner = mesh%owner(iBFace)
                    dS(iOwner,:) = dS(iOwner,:) + 0.5*abs(mesh%Sf(iBFace,:))
                enddo
            enddo

            !-------------------!
            ! Compute CFL field !
            !-------------------!

            do iElement=1,numberOfElements

                vol = mesh%volume(iElement)

                !----------------!
                ! Convective CFL !
                !----------------!

                u = abs(velocity%phi(iElement,:))

                LambdaC(1) = u(1)*dS(iElement,1)
                LambdaC(2) = u(2)*dS(iElement,2)
                LambdaC(3) = u(3)*dS(iElement,3)

                CFLc(iElement) = dtime*(LambdaC(1)+LambdaC(2)+LambdaC(3))/vol

                !-------------!
                ! Viscous CFL !
                !-------------!

                coef = 4.0/(3.0*rho%phi(iElement,1))

                nuLocal = nu%phi(iElement,1)/rho%phi(iElement,1)

                LambdaV(1)=(coef*nuLocal*dS(iElement,1)**2)/vol
                LambdaV(2)=(coef*nuLocal*dS(iElement,2)**2)/vol
                LambdaV(3)=(1-bdim)*(coef*nuLocal*dS(iElement,3)**2)/vol

                CFLv(iElement) = Cvis*dtime*(LambdaV(1)+LambdaV(2)+LambdaV(3))/vol

                !---------------------!
                ! Equivalent velocity !
                !---------------------!

                Ueqv(iElement) = (LambdaC(1)+LambdaC(2)+LambdaC(3)) + Cvis*(LambdaV(1)+LambdaV(2)+LambdaV(3))

            enddo

            !-----------!
            ! Total CFL !
            !-----------!

            this%phi(1:numberOfElements,1) = CFLc(1:numberOfElements) + CFLv(1:numberOfElements)

            ! write to file
            call this%writeFieldFunction()

        endif

    end subroutine computeCo

!**********************************************************************************************************************

    subroutine writeFieldFunction(this)

    !==================================================================================================================
    ! Description:
    !! writeToFile writes the field to a file to be post processed.
    !==================================================================================================================

        class(Co) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: postDir
        !! post processing directory

        character(len=:), allocatable :: fieldDir
        !! field directory

        character(len=20) :: timeStamp
        !! time stamp

        character(len=20) ::  procID
        !! processor ID
    !------------------------------------------------------------------------------------------------------------------

        logical :: dirExists
        !! boolean to check if the directory exists
    !------------------------------------------------------------------------------------------------------------------

        integer :: iComp
        !! target components
   !------------------------------------------------------------------------------------------------------------------

        write(procID,'(i0)') id

        postDir='postProc/fields/'
        write(timeStamp,'(i0)') itime
        fieldDir = postDir//trim(timeStamp)//'/'

        ! Create a directory for the field. If it does not exists create it.

        inquire(file=trim(fieldDir)//'/.', exist=dirExists)

        if(dirExists) then

            ! write Field
            open(1,file=fieldDir//trim(this%fieldName)//'-d'//trim(procID)//'.bin', form='unformatted')

            write(1) this%nComp
            write(1) this%fieldName

            do iComp = 1,this%nComp
                write(1) this%phi(:,iComp)
                write(1) iComp
                write(1) iComp
            end do

            close(1)

        elseif(.not. dirExists) then

            ! create the folder
            call execute_command_line ('mkdir -p ' // adjustl(trim(fieldDir) ) )

            ! write Field
            open(1,file=fieldDir//trim(this%fieldName)//'-d'//trim(procID)//'.bin', form='unformatted')

            write(1) this%nComp
            write(1) this%fieldName

            do iComp=1,this%nComp
                write(1) this%phi(:,iComp)
                write(1) iComp
                write(1) iComp
            end do

            close(1)

        end if

    end subroutine writeFieldFunction
    
end module CoField
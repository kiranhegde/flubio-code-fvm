!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module runTimeObject

    implicit none

    type, public :: runTimeObj

        character(len=:), allocatable :: objName

        integer :: runAt

        contains
            procedure :: initialise =>  initialiseBaseObj
            procedure :: run  => runBaseObj
    end type runTimeObj

contains

    subroutine initialiseBaseObj(this)

    !==================================================================================================================
    ! Description:
    !! runTimeObjects is the base constructor for run-time objects.
    !==================================================================================================================

        class(runTimeObj) :: this
    !------------------------------------------------------------------------------------------------------------------

        write(*,*) 'I am fucking calling this one'

    end subroutine initialiseBaseObj

!**********************************************************************************************************************

    subroutine runBaseObj(this)

    !==================================================================================================================
    ! Description:
    !! runBaseObj is the base runner for run-time objects.
    !==================================================================================================================

        class(runTimeObj) :: this
    !------------------------------------------------------------------------------------------------------------------

    end subroutine runBaseObj

end module runTimeObject

!**********************************************************************************************************************
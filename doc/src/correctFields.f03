!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module correctFields

!==================================================================================================================
! Description:
!! This module implements the field correction procedures.
!==================================================================================================================

    use fieldvar
    use gradient
    use meshvar
    use physicalConstants
    use pressureCorrection

    implicit none

contains

    subroutine correctVelocityAndPressure

    !==================================================================================================================
    ! Description:
    !! correctFields provides the corretions to the velocity and pressure fields as required by SIMPLE/PISO alghoritm.
    !==================================================================================================================

        integer :: i, iElement, iBoundary, iFace, iBFace, iProc, iComp, is, ie
    !------------------------------------------------------------------------------------------------------------------

        real :: gradP_f(numberOfFaces,3,1)
        !! pressure gradient interpolated at mesh faces

        real :: mcorr
        !! mass flow correction

        real :: velCorrUrf
        !! velocity correction under-relaxation
    !------------------------------------------------------------------------------------------------------------------

        if(flubioOptions%relaxVelocityCorrection==1 .and. steadySim==1) then
            velCorrUrf = Peqn%urf
        else
            velCorrUrf = 1.0
        end if

        !----------------!
        ! Correct fields !
        !----------------!

        do i=1,velocity%nComp
            velocity%phi(1:numberOfElements,i) = velocity%phi(1:numberOfElements,i) &
                    - Peqn%Dv%phi(1:numberOfElements,i)*pcorr%phiGrad(1:numberOfElements,i,1)*velCorrUrf
        end do
        pressure%phi(1:numberOfElements,1) = pressure%phi(1:numberOfElements,1) + pcorr%phi(1:numberOfElements,1)*Peqn%urf

        !---------------------!
        ! Correct mass fluxes !
        !---------------------!

        gradP_f = 0.0
        call interpolateGradientCorrected(gradP_f, pcorr, opt=1, fComp=1)

        ! InternalFaces
        do iFace=1,numberOfIntFaces

            mcorr = Peqn%Dv%phif(iFace,1)*mesh%Sf(iFace,1)*gradP_f(iFace,1,1) + &
                    Peqn%Dv%phif(iFace,2)*mesh%Sf(iFace,2)*gradP_f(iFace,2,1) + &
                    Peqn%Dv%phif(iFace,3)*mesh%Sf(iFace,3)*gradP_f(iFace,3,1)

            mf(iFace,1) = mf(iFace,1) - rho%phif(iFace,1)*mcorr

            vf(iFace,:) = vf(iFace,:) - Peqn%Dv%phif(iFace,:)*gradP_f(iFace,:,1)

        enddo

        ! Processor Boundaries
        do iBoundary=1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)

            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1

            do iBFace = is,ie

                mcorr = Peqn%Dv%phif(iBFace,1)*mesh%Sf(iBFace,1)*gradP_f(iBFace,1,1) + &
                        Peqn%Dv%phif(iBFace,2)*mesh%Sf(iBFace,2)*gradP_f(iBFace,2,1) + &
                        Peqn%Dv%phif(iBFace,3)*mesh%Sf(iBFace,3)*gradP_f(iBFace,3,1)

                mf(iBFace,1) = mf(iBFace,1) - rho%phif(iBFace,1)*mcorr
                vf(iBFace,:) = vf(iBFace,:) - Peqn%Dv%phif(iBFace,:)*gradP_f(iBFace,:,1)

            enddo

        enddo

        ! Update pressure at boundaries
        call pressure%boundaryExtrapolation()

        ! Correct mass fluxes at boundaries
        call correctBoundaryMassFluxes()

        ! Update velocity field at boundaries
        call velocity%updateBoundaryVelocityField()

        !------------------!
        ! Update gradients !
        !------------------!

        call computeGradient(velocity)

        call computeGradient(pressure)

        ! Put w=0 for 2D problems
        if (bdim==1) call velocity%componentToZero(3)

    end subroutine correctVelocityAndPressure

!**********************************************************************************************************************

    subroutine correctVelocity

    !==================================================================================================================
    ! Description:
    !! correctVelocity provides the corretions to the velocity as required by the modified SIMPLE/PISO alghoritm.
    !==================================================================================================================

        type(flubioField) :: ones
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iElement, iBoundary, iFace, iBFace, iProc, iComp, is, ie, ierr
    !------------------------------------------------------------------------------------------------------------------

        real :: dot

        real :: mcorr
        !! mass flow correction
    !------------------------------------------------------------------------------------------------------------------

        call ones%createWorkField(fieldName='ones', classType='scalar', nComp=1)
        ones%phi = 1.0

        call Peqn%diffFluxes%totalDiffusionFluxes(pressure, ones)

        !---------------------!
        ! Correct mass fluxes !
        !---------------------!

        ! InternalFaces
        mf(1:numberOfIntFaces,1) = mf(1:numberOfIntFaces,1) + Peqn%diffFluxes%fluxTf(1:numberOfIntFaces,1)

        ! Processor Boundaries
        do iBoundary=1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)

            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1

            do iBFace = is,ie
                mcorr = Peqn%diffFluxes%fluxTf(iBFace,1)
                mf(iBFace,1) = mf(iBFace,1) + mcorr
            enddo

        enddo

        ! Correct mass fluxes at boundaries
        call correctBoundaryMassFluxes2()

        !--------------------!
        ! Correct velocities !
        !--------------------!

        ! Relax pressure
        call pressure%relax(Peqn%urf)
        call pressure%boundaryExtrapolation()
        call computeGradient(pressure)

        do i=1,velocity%nComp
            velocity%phi(1:numberOfElements,i) = velocity%phi(1:numberOfElements,i) &
                    -Peqn%Dv%phi(1:numberOfElements,i)*pressure%phiGrad(1:numberOfElements,i,1)
        end do

        ! Update velocity field at boundaries
        call velocity%updateBoundaryVelocityField()

        !------------------!
        ! Update gradients !
        !------------------!

        call computeGradient(velocity)

        ! Put w=0 for 2D problems
        if (bdim==1) call velocity%componentToZero(3)

        ! Clean up
        call ones%destroyWorkField()

    end subroutine correctVelocity

! *********************************************************************************************************************

    subroutine correctBoundaryMassFluxes()

    !==================================================================================================================
    ! Description:
    !! Correct mass fluxes at boundaries (e.g. outlet condition).
    !==================================================================================================================

        integer :: i, iBoundary, iBface,  iOwner, is, ie, bFlag
    !------------------------------------------------------------------------------------------------------------------

        real :: corr, valueFraction
    !------------------------------------------------------------------------------------------------------------------

        do iBoundary=1,numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1
            bFlag = velocity%bcFlag(iBoundary)

            ! Loop over the faces of boundary number "iBoundary"
            do iBFace=is, ie

                iOwner = mesh%owner(iBFace)

                ! Outlet BC
                if(bFlag==5) then

                    corr = Peqn%diffFluxes%fluxC1f(iBFace,1)*pcorr%phi(iOwner,1)
                    mf(iBFace,1) = mf(iBFace,1) + rho%phif(iBFace,1)*corr

                ! Inlet/outlet or Free stream BC
                elseif(bFlag==11) then

                    if(mf(iBFace,1) > 0.0) then
                        corr = Peqn%diffFluxes%fluxC1f(iBFace,1)*pcorr%phi(iOwner,1)
                    else
                        corr=0.0
                    end if

                    mf(iBFace,1) = mf(iBFace,1) + rho%phif(iBFace,1)*corr

                endif

            enddo

        enddo

    end subroutine correctBoundaryMassFluxes

! *********************************************************************************************************************

    subroutine correctBoundaryMassFluxes2()

    !==================================================================================================================
    ! Description:
    !! Correct mass fluxes at boundaries (e.g. outlet condition).
    !==================================================================================================================

        integer :: i, iBoundary, iBface,  iOwner, is, ie, bFlag
    !------------------------------------------------------------------------------------------------------------------

        real :: corr, valueFraction
    !------------------------------------------------------------------------------------------------------------------

        do iBoundary=1,numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1
            bFlag = velocity%bcFlag(iBoundary)

            ! Loop over the faces of boundary number "iBoundary"
            do iBFace=is, ie

                iOwner = mesh%owner(iBFace)

                ! Outlet BC
                if(bFlag==5) then

                    corr = Peqn%diffFluxes%fluxTf(iBFace,1)
                    mf(iBFace,1) = mf(iBFace,1) + corr

                ! Inlet/outlet or Free stream BC
                elseif(bFlag==11) then

                    if(mf(iBFace,1) > 0.0) then
                        corr = Peqn%diffFluxes%fluxTf(iBFace,1)
                    else
                        corr = 0.0
                    end if

                    mf(iBFace,1) = mf(iBFace,1) + corr

                endif

            enddo

        enddo

    end subroutine correctBoundaryMassFluxes2

! *********************************************************************************************************************

end module correctFields
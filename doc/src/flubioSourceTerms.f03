!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module sources

    !===================================================================================================================
    ! Description:
    !! This module implements the dictionary storing the entries of settings/Sources and associated flags.
    !===================================================================================================================

    use generalDictionaries
    use qvector_m

    implicit none

    type, extends(generalDict) :: sourcesDict

        type(qhashtbl_t) :: sourcesTable

        character(len=10), dimension(:), allocatable :: eqNames

        integer, dimension(:), allocatable :: numberOfSources
        !! numberOfSources

        logical :: found
        !! flag to check the presence of the dictionary

        logical :: sourcesFound
        !! flag to check the presence of the volumeSources sub dictionary

        logical :: customSourcesFound
        !! flag to check the presence of the custoumVolumeSources sub dictionary

    contains
        procedure :: set => setSources

    end type sourcesDict

contains

    subroutine setSources(this)

    !===================================================================================================================
    ! Description:
    !! setSources fills the the run time processing dictionary.
    !===================================================================================================================

        class(sourcesDict) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: dictName
        !! First array element lenght

        character(len=:), allocatable  :: path

        character(len=50), dimension(:), allocatable  :: splitPath
    !------------------------------------------------------------------------------------------------------------------

        integer :: n, nEqn, isize
    !------------------------------------------------------------------------------------------------------------------

        type(json_core) :: jcore

        type(json_value), pointer :: dictPointer, SourcesPointer, eqnPointer
    !------------------------------------------------------------------------------------------------------------------

        logical :: sourcesFound, eqnFound, found
    !------------------------------------------------------------------------------------------------------------------

        dictName = 'settings/volumeSources'

        isize = storage_size(n)/8
        call this%sourcesTable%new(1, isize)

        ! Initialise the JSON dictionary
        inquire(file=trim(dictName), exist=this%found)

        if (this%found) then

            call this%initJSON(dictName)

            ! Get the pointer to the dictionary
            call this%json%get(dictPointer)

            ! Initialise json factory
            call jcore%initialize()

            call jcore%get(dictPointer, 'customVolumeSources', sourcesPointer, sourcesFound)
            this%customSourcesFound = sourcesFound

            call jcore%get(dictPointer, 'volumeSources', sourcesPointer, sourcesFound)
            this%sourcesFound = sourcesFound

            if(this%sourcesFound) then

                call this%json%info('volumeSources', n_children=nEqn)

                allocate(this%eqNames(nEqn))
                allocate(this%numberOfSources(nEqn))
                this%numberOfSources = 0

                do n=1,nEqn

                    ! Get sources pointer
                    call jcore%get_child(sourcesPointer, n, eqnPointer, eqnFound)

                    ! Get eqn pointer
                    call jcore%get_path(eqnPointer, path, found)

                    if(found) then
                        call split_in_array(path, splitPath, '.')
                        this%eqNames(n) = splitPath(2)
                        call this%json%info(splitPath(1)//'%'//splitPath(2), n_children=this%numberOfSources(n))
                        call this%sourcesTable%put(trim(splitPath(2)), this%numberOfSources(n))
                    else
                        call flubiostopMsg('FLUBIO ERROR: cannot find an equation name. Please check your dictionary!')
                    end if

                end do

            else
                allocate(this%numberOfSources(1))
                this%numberOfSources = 0
            end if

        end if

        ! Clean
        nullify(dictPointer)
        if(sourcesFound) nullify(sourcesPointer)
        if(eqnFound) nullify(eqnPointer)
        call jcore%destroy()

    end subroutine setSources

end module sources

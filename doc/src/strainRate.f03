module strainRateField

    use flubioDictionaries, only: flubioMonitors, raiseKeyErrorJSON
    use globalMeshVar
    use globalTimeVar
    use fieldvar, only: velocity
    use flubioMpi
    use json_module
    use runTimeObject
    use velocityTensors

    implicit none

    type, public, extends(runTimeObj) :: E

        character(len=30) :: fieldName
        !! Name of the field

        integer :: nComp
        !! number of components

        real, dimension(:,:), allocatable :: phi
        !! field variable

    contains

        procedure :: initialise => createStrainRate
        procedure :: run =>  computeStrainRate
        procedure :: writeFieldFunction

    end type E

contains

    subroutine createStrainRate(this)

    !==================================================================================================================
    ! Description:
    !! createstrainRate is the field constructor and it allocates the memory for a new flubioField.
    !==================================================================================================================

        class(E) :: this
    !-------------------------------------------------------------------------------------------------------------------

        logical :: found, monitorsFound, monitorFound
    !------------------------------------------------------------------------------------------------------------------

        type(json_core) :: jcore

        type(json_value), pointer :: dictPointer, monitorsPointer, monitorPointer
    !------------------------------------------------------------------------------------------------------------------

        ! Get the pointer to the dictionary
        call flubioMonitors%json%get(dictPointer)

        ! Initialize json factory
        call jcore%initialize()
        call jcore%get(dictPointer, 'Monitors', monitorsPointer, monitorsFound)

        ! Get monitor pointer
        call jcore%get_child(monitorsPointer, this%objName, monitorPointer, monitorFound)

        call jcore%get(monitorPointer, 'saveEvery', this%runAt, found)
        call raiseKeyErrorJSON('saveEvery', found)

        this%fieldName = 'StrainRate'
        this%nComp = 1
        allocate(this%phi(numberOfElements+numberOfBFaces,this%nComp))

        ! Clean up
        nullify(dictPointer)
        if(monitorsFound) nullify(monitorsPointer)
        if(monitorFound) nullify(monitorPointer)
        call jcore%destroy()

    end subroutine createstrainRate

! *********************************************************************************************************************

    subroutine computestrainRate(this)

    !==================================================================================================================
    ! Description:
    !! computeStrainRate computes the strain rate tensor module.
    !==================================================================================================================

        class(E):: this
	!------------------------------------------------------------------------------------------------------------------

		this%phi = 0.0
        if(mod(itime, this%runAt)==0) then
            this%phi(1:numberOfElements,1) = strainTensorModule()
            call this%writeFieldFunction()
        endif

    end subroutine computestrainRate

!**********************************************************************************************************************

    subroutine writeFieldFunction(this)

    !==================================================================================================================
    ! Description:
    !! writeToFile writes the field to a file to be post processed.
    !==================================================================================================================

        class(E) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: postDir
        !! post processing directory

        character(len=:), allocatable :: fieldDir
        !! field directory

        character(len=20) :: timeStamp
        !! time stamp

        character(len=20) ::  procID
        !! processor ID
    !------------------------------------------------------------------------------------------------------------------

        logical :: dirExists
        !! boolean to check if the directory exists
    !------------------------------------------------------------------------------------------------------------------

        integer :: iComp
        !! target components
    !------------------------------------------------------------------------------------------------------------------

        write(procID,'(i0)') id

        postDir='postProc/fields/'
        write(timeStamp,'(i0)') itime
        fieldDir = postDir//trim(timeStamp)//'/'

        ! Create a directory for the field. If it does not exists create it.

        inquire(file=trim(fieldDir)//'/.', exist=dirExists)

        if(dirExists) then

            ! write Field
            open(1,file=fieldDir//trim(this%fieldName)//'-d'//trim(procID)//'.bin', form='unformatted')

            write(1) this%nComp
            write(1) this%fieldName

            do iComp = 1,this%nComp
                write(1) this%phi(:,iComp)
                write(1) iComp
                write(1) iComp
            end do

            close(1)

        elseif(.not. dirExists) then

            ! create the folder
            call execute_command_line ('mkdir -p ' // adjustl(trim(fieldDir) ) )

            ! write Field
            open(1,file=fieldDir//trim(this%fieldName)//'-d'//trim(procID)//'.bin', form='unformatted')

            write(1) this%nComp
            write(1) this%fieldName

            do iComp=1,this%nComp
                write(1) this%phi(:,iComp)
                write(1) iComp
                write(1) iComp
            end do

            close(1)

        end if

    end subroutine writeFieldFunction

end module strainRateField

!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module convectiveFluxes

!==================================================================================================================
! Description:
!! ConvectiveFluxes contains the data structure and the methods to assemble the convective fluxes at
!! mesh faces, arising from the numerical approximation of the convective term.
!! The fluxes are then used to assemble the convective term in the equation matrix and rhs.
!==================================================================================================================

    use fieldvar
    use meshvar
    use math
    use interpolation
    use physicalConstants

    implicit none

    type, public :: convective_fluxes

        real, dimension(:,:), allocatable :: fluxC1f
        !! Owner contribuiton to the convective flux.

        real, dimension(:,:), allocatable :: fluxC2f
        !! Neighbour contribuiton to the convective flux

        real, dimension(:,:), allocatable :: fluxVf
        !! Explicit contribution to the convective flux (Cross diffsuion + boundary connditions).

        contains
            procedure :: create => createConvectiveFluxes
            procedure :: destroy => destroyConvectiveFluxes
            generic ::   assembleConvectiveTerm => assembleConvectiveTermStd, assembleConvectiveTermWithCoeff
            generic ::   assembleConvectiveTermImplicit => assembleConvectiveTermImplicitStd, assembleConvectiveTermImplicitWithCoeff
            procedure :: assembleConvectiveTermStd
            procedure :: assembleConvectiveTermImplicitStd
            procedure :: assembleConvectiveTermExplicit
            procedure :: assembleConvectiveTermWithCoeff
            procedure :: assembleConvectiveTermImplicitWithCoeff
            procedure :: assembleConvectiveTermFf
            procedure :: assembleConvectiveTermImplicitFf
            procedure :: assembleBoundaryConvectiveFluxes
    end type

contains

    subroutine createConvectiveFluxes(this, nComp)

    !==================================================================================================================
    ! Description:
    !! CreateConvectiveFluxes is the class constructor.
    !==================================================================================================================

        class(convective_fluxes) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: nComp
        !! number of field component
    !------------------------------------------------------------------------------------------------------------------

        allocate(this%fluxC1f(numberOfFaces,nComp))
        allocate(this%fluxC2f(numberOfFaces,nComp))
        allocate(this%fluxVf(numberOfFaces,nComp))

        this%fluxC1f = 0.0
        this%fluxC2f = 0.0
        this%fluxVf = 0.0

    end subroutine createconvectiveFluxes

! *********************************************************************************************************************

    subroutine destroyConvectiveFluxes(this)

    !------------------------------------------------------------------------------------------------------------------
    ! Description:
    !! DestroyConvectiveFluxes is the class deconstructor
    !------------------------------------------------------------------------------------------------------------------

        class(convective_fluxes) :: this
    !------------------------------------------------------------------------------------------------------------------

        deallocate(this%fluxC1f)
        deallocate(this%fluxC2f)
        deallocate(this%fluxVf)

    end subroutine destroyconvectiveFluxes

! *********************************************************************************************************************

    subroutine assembleConvectiveTermStd(this, field, convOpt)

    !===================================================================================================================
    ! Description:
    !! AssembleConvectiveTerm computes the convective fluxes
    !! to be used in the linear system assembling of the convective term. In FLUBIO there are two main ways
    !! to get the common face values between two neighbouring cells.
    !! "Gradient Based" uses the current gradient field to compute the face value of a transported varaible according to different
    !! known formulas (e.g. central, second order upwind etc.).
    !! TVD is a class of methods which allows to compute the face value relying on a total diminishing variation formulas (e.g. Van leer)
    !! For each cell face we can define three quantities:
    !! \begin{eqnarray}
    !!  FluxC_1_f = ||m_f, 0|| \\
    !!  FluxC_2_f = -||-m_f, 0|| \\
    !!  FluxV_f = m_f*\phi_f
    !! \begin{eqnarray}
    !! The operator ||a, b|| returns the maximum of a and b and it is commonly used to define an upwind scheme.
    !===================================================================================================================

        class(convective_fluxes) :: this
        !! current class
        
        type(flubioField) :: field
        !! Transported field (e.g. velocity or concentration)
    !------------------------------------------------------------------------------------------------------------------

        integer :: iFace, iBFace, iOwner, iNeighbour, iUpwind, iDownwind, iBoundary, iComp, fComp, is, ie, iProc, pNeigh

        integer :: convOpt
        !! convection scheme to be used

        integer :: boundedCorr
        !! flag for the addition of the bounded correction term
    !------------------------------------------------------------------------------------------------------------------

        integer :: pos(numberOfFaces,1)
        !! auxiliary index to defie the upwind cell

        real :: phiC(field%nComp)
        !! cell center value

        real :: phi_f
        !! face value

        real :: L
        !! distance weighting factor

        real :: switch
        !! auxiliary variable

        real :: psi
        !! face limiter for tvd schemes

        real :: corr(field%nComp)
        !! correction to be assigned to fluxVf when used the deferred correction (default) approach.

        real :: massFluxOwner, massFluxNeigh
    !------------------------------------------------------------------------------------------------------------------

        pos = 0
        fComp = field%nComp

        !--------------------------------------------------!
        ! Interpolate the gradient for deferred correction !
        !--------------------------------------------------!

        if(convOpt==1) then
            call interpolate(field, interpType='linear', opt=-1)
        elseif(convOpt>=2 .and. convOpt<=4) then
            call interpolate(field, interpType='gradientbased', opt=convOpt)
        else
            call interpolate(field, interpType='tvd', opt=convOpt)
        end if

        !--------------------------!
        ! Update convective fluxes !
        !--------------------------!

        do iFace=1,numberOfIntFaces

            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)

            ! Find index of the upwind cell
            if (mf(iFace,1)>=0) pos(iFace,1)=1

            iUpwind = pos(iFace,1)*iOwner + (1-pos(iFace,1))*iNeighbour

            phiC = field%phi(iUpwind,:)
            corr = field%phif(iFace,:) - phiC

            this%FluxC1f(iFace,:) = mf(iFace,1)*pos(iFace,1)
            this%FluxC2f(iFace,:) = mf(iFace,1)*(1-pos(iFace,1))
            this%FluxVf(iFace,:) = mf(iFace,1)*corr

        enddo

        !----------------------!
        ! Processor Boundaries !
        !----------------------!

        do iBoundary=1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1
            pNeigh = 0

            do iBFace=is,ie

                pNeigh = pNeigh+1
                iOwner = mesh%owner(iBFace)

                ! Compute correction for HO schemes (Deferred correction approach)
                if(mf(iBFace,1)>=0) pos(iBFace,1)=1

                switch = pos(iBFace,1)

                ! Compute phiC
                phiC = field%phi(iOwner,:)*switch + field%ghosts(pNeigh,:,iBoundary)*(1-switch)

                call doubleBar(mf(iBFace,1), 0.0, massFluxOwner)
                call doubleBar(-mf(iBFace,1), 0.0, massFluxNeigh)

                corr = field%phif(iBFace,:) - phiC
                this%fluxC1f(iBFace,:) = massFLuxOwner
                this%fluxC2f(iBFace,:) = massFluxNeigh
                this%FluxVf(iBFace,:) = mf(iBFace,1)*corr

            enddo

        enddo

    end subroutine assembleConvectiveTermStd
    
! *********************************************************************************************************************

    subroutine assembleConvectiveTermImplicitStd(this, field, convOpt)

    !==================================================================================================================
    ! Description:
    !! assembleConvectiveTermImplicit assembles the fluxes without deferred correction. It is used when the convective term
    !! has to be discretized fully implicitly.
    !! For each face, we can define three quantities:
    !! \begin{eqnarray}
    !!  switch = sign(mf) \\
    !!  FluxC_1_f =  mf*(1-psi)*switch + mf*psi*(1-switch)  \\
    !!  FluxC_2_f = mf(iFace,1)*psi*switch + mf(iFace,1)*(1-psi)*(1-switch) \\
    !!  FluxV_f = 0.0
    !! \begin{eqnarray}
    !! where $L$ is the weighting factor equal to 0.5 for equispaced meshes.
    !==================================================================================================================

        class(convective_fluxes) :: this
        
        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        integer :: iFace, iBFace, iOwner, iNeighbour, iUpwind, iDownwind, iBoundary, iComp, fComp, is, ie, iProc, pNeigh

        integer :: convOpt
        !! convection scheme to be used

        integer :: boundedCorr
        !! flag for the addition of the bounded correction term

        integer :: pos(numberOfFaces,1)
        !! auxiliary index to defie the upwind cell
    !------------------------------------------------------------------------------------------------------------------

        real :: upwindGrad(3,field%nComp)
        !! field gradient at the upwind cell

        real :: rCf(3)
        !! cell-center to cell-face distance

        real :: CN(3)
        !! distance between two neighbouring cells

        real :: dUD(3)
        !! distance between upwind and dowind cell

        real :: dot
        !! auxiliary value storing a dot product

        real :: d1
        !! auxiliary value

        real :: d2
        !! auxiliary value

        real:: phiD(field%nComp)
        !! downwind cell center value

        real :: phiU(field%nComp)
        !! far upwind cell center value computed using gradients

        real :: phiC(field%nComp)
        !! upwind cell center value

        real :: phi_f
        !! face value

        real :: L
        !! distance weighting factor

        real :: switch
        !! auxiliary variable

        real :: psi
        !! face limiter for tvd schemes

        real :: corr
        !! correction to be assigned to fluxVf when used the deferred correction (default) approach.

        real :: d, absGradPhi, theta

        real :: small
    !------------------------------------------------------------------------------------------------------------------

        pos = 0
        small = 0.0000000001

        fComp = field%nComp

        ! Update convective fluxes
        do iFace=1,numberOfIntFaces

            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)

            ! Find index of the upwind cell
            if (mf(iFace,1)>=0) pos(iFace,1)=1

            switch = pos(iFace,1)

            iUpwind   =  pos(iFace,1)*iOwner + (1-pos(iFace,1))*iNeighbour
            iDownwind =  pos(iFace,1)*iNeighbour + (1-pos(iFace,1))*iOwner
            upwindGrad = field%phiGrad(iUpwind,:,:)

            ! Compute correction for HO schemes (Defferred correction approach)
            rCf = mesh%fcentroid(iFace,:) - mesh%centroid(iUpwind,:)
            dUD = mesh%centroid(iUpwind,:) - mesh%centroid(iDownwind,:)
            d = sqrt(dUD(1)**2+dUD(2)**2+dUD(3)**2)

            ! Compute inverse distance factor L
            call mag(mesh%CN(iFace,:), d1)
            call mag(rCf, d2)
            L = d1/d2

            ! Compute phiC, phiD, phiU
            phiC = field%phi(iUpwind, :)
            phiD = field%phi(iDownwind, :)

            do iComp=1, fComp

                dot = dot_product(field%phiGrad(iUpwind,:,iComp),dUD)
                phiU(iComp) = phiD(iComp) + 2.0*dot

                ! compute theta
                absGradPhi = sqrt(upwindGrad(1,iComp)**2+upwindGrad(2,iComp)**2+upwindGrad(3,iComp)**2)
                dot = dot_product(upwindGrad(:,iComp), dUD)
                theta = acos(abs(dot/(absGradPhi*d+small)))

                ! TVD Schemes
                call TVD_HR_Schemes(phiC(iComp), phiD(iComp), phiU(iComp), L, phi_f, corr, psi, theta, convOpt)

                this%FluxC1f(iFace,iComp) = mf(iFace,1)*(1-psi)*switch + mf(iFace,1)*psi*(1-switch)
                this%FluxC2f(iFace,iComp) = mf(iFace,1)*psi*switch + mf(iFace,1)*(1-psi)*(1-switch)

            enddo

        enddo

        !----------------------!
        ! Processor Boundaries !
        !----------------------!

        do iBoundary=1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1
            pNeigh = 0

            do iBFace=is,ie

                pNeigh = pNeigh+1
                iOwner = mesh%owner(iBFace)

                ! Compute correction for HO schemes (Defferred correction approach)
                if(mf(iBFace,1)>=0) pos(iBFace,1)=1

                switch = pos(iBFace,1)
                rCf = mesh%fcentroid(iBFace,:) - mesh%centroid(iOwner,:)*switch - mesh%ghost(pNeigh,:,iBoundary)*(1-switch)
                upwindGrad = field%phiGrad(iOwner,:,:)*switch + field%ghostsGrad(pNeigh,:,iBoundary,:)*(1-switch)

                ! Compute phiC, phiD, phiU
                phiC = field%phi(iOwner,:)*switch + field%ghosts(pNeigh,:,iBoundary)*(1-switch)
                phiD = field%ghosts(pNeigh,:,iBoundary)*switch + field%phi(iOwner,:)*(1-switch)
                dUD = mesh%centroid(iOwner,:)*(2*switch-1) - mesh%ghost(pNeigh,:,iBoundary)*(2*switch-1)
                d = sqrt(dUD(1)**2+dUD(2)**2+dUD(3)**2)

                ! Compute inverse distance factor L
                call mag(mesh%CN(iBFace,:), d1)
                call mag(rCf,d2)
                L = d1/d2

                do iComp=1, fComp

                    dot = dot_product(upwindGrad(:,iComp), dUD)
                    phiU(iComp) = phiD(iComp) + 2*dot

                    ! Compute theta
                    absGradPhi = sqrt(upwindGrad(1,iComp)**2+upwindGrad(2,iComp)**2+upwindGrad(3,iComp)**2)
                    dot = dot_product(upwindGrad(:,iComp), dUD)
                    theta = acos(abs(dot/(absGradPhi*d+small)))

                    ! TVD Schemes
                    call TVD_HR_Schemes(phiC(iComp), phiD(iComp), phiU(iComp), L, phi_f, corr, psi, theta, convOpt)

                    this%FluxC1f(iBFace,iComp) = mf(iBFace,1)*(1-psi)*switch + mf(iBFace,1)*psi*(1-switch)
                    this%FluxC2f(iBFace,iComp) = mf(iBFace,1)*psi*switch + mf(iBFace,1)*(1-psi)*(1-switch)

                enddo

            enddo

        enddo
        
    end subroutine assembleConvectiveTermImplicitStd

! *********************************************************************************************************************

    subroutine assembleConvectiveTermExplicit(this, field, convOpt)

    !===================================================================================================================
    ! Description:
    !! AssembleConvectiveTermExplicit computes the convective fluxes
    !! to be used in the linear system assembling of the convective term in fully explicit way.
    !! For each cell face we can define three quantities:
    !! \begin{eqnarray}
    !!  FluxC_1_f = 0.0 \\
    !!  FluxC_2_f = 0.0 \\
    !!  FluxV_f = m_f*\phi_f
    !! \begin{eqnarray}
    !===================================================================================================================

        class(convective_fluxes) :: this
        !! current class

        type(flubioField) :: field
        !! Transported field (e.g. velocity or concentration)
    !------------------------------------------------------------------------------------------------------------------

        integer :: iFace, iBFace, iOwner, iNeighbour, iUpwind, iDownwind, iBoundary, iComp, fComp, is, ie, iProc, pNeigh

        integer :: convOpt
        !! convection scheme to be used

        integer :: boundedCorr
        !! flag for the addition of the bounded correction term
    !------------------------------------------------------------------------------------------------------------------

        real :: phi_f
        !! face value

        real :: L
        !! distance weighting factor

        real :: switch
        !! auxiliary variable

        real :: psi
        !! face limiter for tvd schemes

        real :: corr(field%nComp)
        !! correction to be assigned to fluxVf when used the deferred correction (default) approach.
    !------------------------------------------------------------------------------------------------------------------

        fComp = field%nComp

        !--------------------------------------------------!
        ! Interpolate the gradient for deferred correction !
        !--------------------------------------------------!

        if(convOpt==1) then
            call interpolate(field, interpType='linear', opt=-1)
        elseif(convOpt>=2 .and. convOpt<=4) then
            call interpolate(field, interpType='gradientbased', opt=convOpt)
        else
            call interpolate(field, interpType='tvd', opt=convOpt)
        end if

        !--------------------------!
        ! Update convective fluxes !
        !--------------------------!

        do iFace=1,numberOfIntFaces

            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)

            corr = field%phif(iFace,:)

            this%FluxC1f(iFace,:) = 0.0
            this%FluxC2f(iFace,:) = 0.0
            this%FluxVf(iFace,:) = mf(iFace,1)*corr

        enddo

        !----------------------!
        ! Processor Boundaries !
        !----------------------!

        do iBoundary=1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1
            pNeigh = 0

            do iBFace=is,ie

                pNeigh = pNeigh+1
                iOwner = mesh%owner(iBFace)

                corr = field%phif(iBFace,:)
                this%FluxVf(iBFace,:) = mf(iBFace,1)*corr

            enddo

        enddo

        !---------------------!
        ! Physical Boundaries !
        !---------------------!

        do iBoundary=1,numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1

            do iBFace=is,ie

                pNeigh = pNeigh+1
                iOwner = mesh%owner(iBFace)

                corr = field%phif(iBFace,:)
                this%FluxVf(iBFace,:) = mf(iBFace,1)*corr

            enddo

        enddo

    end subroutine assembleConvectiveTermExplicit

! *********************************************************************************************************************

    subroutine assembleBoundaryConvectiveFluxes(this, field, convOpt)

    !===================================================================================================================
    ! Description:
    !! assembleBoundaryConvectiveFluxes computes the convective fluxes at boundaries explicitly.
    !===================================================================================================================

        class(convective_fluxes) :: this
        !! current class

        type(flubioField) :: field
        !! Transported field (e.g. velocity or concentration)
    !------------------------------------------------------------------------------------------------------------------

        integer :: iFace, iBFace, iOwner, iNeighbour, iUpwind, iDownwind, iBoundary, iComp, fComp, is, ie, iProc

        integer :: convOpt
        !! convection scheme to be used

        integer :: boundedCorr
        !! flag for the addition of the bounded correction term
    !------------------------------------------------------------------------------------------------------------------

        integer :: pos(numberOfFaces,1)
        !! auxiliary index to defie the upwind cell

        real :: phiC(field%nComp)
        !! cell center value

        real :: phi_f
        !! face value

        real :: L
        !! distance weighting factor

        real :: switch
        !! auxiliary variable

        real :: psi
        !! face limiter for tvd schemes

        real :: corr(field%nComp)
        !! correction to be assigned to fluxVf when used the deferred correction (default) approach.
    !------------------------------------------------------------------------------------------------------------------

        pos = 0
        fComp = field%nComp

        !--------------------------------------------------!
        ! Interpolate the gradient for deferred correction !
        !--------------------------------------------------!

        if(convOpt==1) then
            call interpolate(field, interpType='linear', opt=-1)
        elseif(convOpt>=2 .and. convOpt<=4) then
            call interpolate(field, interpType='gradientbased', opt=convOpt)
        else
            call interpolate(field, interpType='tvd', opt=convOpt)
        end if

        !-----------------!
        ! Real boundaries !
        !-----------------!

        do iBoundary=1,numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1

            do iBFace=is,ie

                iOwner = mesh%owner(iBFace)

                ! Compute correction for HO schemes
                corr = field%phif(iBFace,:)
                this%FluxVf(iBFace,:) = mf(iBFace,1)*corr

            enddo

        enddo

    end subroutine assembleBoundaryConvectiveFluxes

! *********************************************************************************************************************

    subroutine assembleConvectiveTermWithCoeff(this, field, coeff, convOpt)

    !===================================================================================================================
    ! Description:
    !! assembleConvectiveTermWithCoeff computes the convective fluxes
    !! to be used in the linear system assembling of the convective term. In FLUBIO there are two main ways
    !! to get the common face values between two neighbouring cells.
    !! "Gradient Based" uses the current gradient field to compute the face value of a transported varaible according to different
    !! known formulas (e.g. central, second order upwind etc.).
    !! TVD is a class of methods which allows to compute the face value relying on a total diminishing variation formulas (e.g. Van leer)
    !! For each cell face we can define three quantities:
    !! \begin{eqnarray}
    !!  FluxC_1_f = ||m_f, 0|| \\
    !!  FluxC_2_f = -||-m_f, 0|| \\
    !!  FluxV_f = m_f*\phi_f
    !! \begin{eqnarray}
    !! The operator ||a, b|| returns the maximum of a and b and it is commonly used to define an upwind scheme.
    !===================================================================================================================

        class(convective_fluxes) :: this
        !! current class

        type(flubioField) :: field
        !! Transported field (e.g. velocity or concentration)

        type(flubioField) :: coeff
        !! coefficient multiplied the convective term
    !------------------------------------------------------------------------------------------------------------------

        integer :: iFace, iBFace, iOwner, iNeighbour, iUpwind, iDownwind, iBoundary, iComp, fComp, is, ie, iProc, pNeigh

        integer :: convOpt
        !! convection scheme to be used

        integer :: boundedCorr
        !! flag for the addition of the bounded correction term
    !------------------------------------------------------------------------------------------------------------------

        integer :: pos(numberOfFaces,1)
        !! auxiliary index to defie the upwind cell

        real :: phiC(field%nComp)
        !! cell center value

        real :: phi_f
        !! face value

        real :: L
        !! distance weighting factor

        real :: switch
        !! auxiliary variable

        real :: psi
        !! face limiter for tvd schemes

        real :: corr(field%nComp)
        !! correction to be assigned to fluxVf when used the deferred correction (default) approach.

        real :: cf, massFluxOwner, massFluxNeigh
    !------------------------------------------------------------------------------------------------------------------

        pos = 0
        fComp = field%nComp

        !--------------------------------------------------!
        ! Interpolate the gradient for deferred correction !
        !--------------------------------------------------!

        if(convOpt==1) then
            call interpolate(field, interpType='linear', opt=-1)
        elseif(convOpt>=2 .and. convOpt<=4) then
            call interpolate(field, interpType='gradientbased', opt=convOpt)
        else
            call interpolate(field, interpType='tvd', opt=convOpt)
        end if

        call interpolate(coeff, interpType='linear', opt=-1)

        !--------------------------!
        ! Update convective fluxes !
        !--------------------------!

        do iFace=1,numberOfIntFaces

            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)

            cf = coeff%phif(iFace,1)

            ! Find index of the upwind cell
            if (mf(iFace,1)>=0) pos(iFace,1)=1

            iUpwind = pos(iFace,1)*iOwner + (1-pos(iFace,1))*iNeighbour

            phiC = field%phi(iUpwind, :)
            corr = field%phif(iFace,:) - phiC

            this%fluxC1f(iFace,:) = cf*mf(iFace,1)*pos(iFace,1)
            this%fluxC2f(iFace,:) = cf*mf(iFace,1)*(1-pos(iFace,1))
            this%fluxVf(iFace,:) = cf*mf(iFace,1)*corr

        enddo

        !----------------------!
        ! Processor Boundaries !
        !----------------------!

        do iBoundary=1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1
            pNeigh = 0

            do iBFace=is,ie

                pNeigh = pNeigh+1
                iOwner = mesh%owner(iBFace)

                cf = coeff%phif(iBFace,1)

                ! Compute correction for HO schemes (Deferred correction approach)
                if(mf(iBFace,1)>=0) pos(iBFace,1)=1

                switch = pos(iBFace,1)

                ! Compute phiC
                phiC = field%phi(iOwner,:)*switch + field%ghosts(pNeigh,:,iBoundary)*(1-switch)

                call doubleBar(mf(iBFace,1), 0.0, massFluxOwner)
                call doubleBar(-mf(iBFace,1), 0.0, massFluxNeigh)

                corr = field%phif(iBFace,:) - phiC
                this%fluxC1f(iBFace,:) = cf*massFLuxOwner
                this%fluxC2f(iBFace,:) = cf*massFluxNeigh
                this%fluxVf(iBFace,:) = cf*mf(iBFace,1)*corr

            enddo

        enddo

    end subroutine assembleConvectiveTermWithCoeff

! *********************************************************************************************************************

    subroutine assembleConvectiveTermImplicitWithCoeff(this, field, coeff, convOpt)

        !==================================================================================================================
        ! Description:
        !! assembleConvectiveTermImplicitWithCoeff assembles the fluxes without deferred correction.
        !! It is used when the convective term has to be discretized fully implicitly.
        !! For each face, we can define three quantities:
        !! \begin{eqnarray}
        !!  switch = sign(mf) \\
        !!  FluxC_1_f =  mf*(1-psi)*switch + mf*psi*(1-switch)  \\
        !!  FluxC_2_f = mf(iFace,1)*psi*switch + mf(iFace,1)*(1-psi)*(1-switch) \\
        !!  FluxV_f = 0.0
        !! \begin{eqnarray}
        !! where $L$ is the weighting factor equal to 0.5 for equispaced meshes.
        !==================================================================================================================

        class(convective_fluxes) :: this

        type(flubioField) :: field, coeff
    !------------------------------------------------------------------------------------------------------------------

        integer :: iFace, iBFace, iOwner, iNeighbour, iUpwind, iDownwind, iBoundary, iComp, fComp, is, ie, iProc, pNeigh

        integer :: convOpt
        !! convection scheme to be used

        integer :: boundedCorr
        !! flag for the addition of the bounded correction term

        integer :: pos(numberOfFaces,1)
        !! auxiliary index to defie the upwind cell
    !------------------------------------------------------------------------------------------------------------------

        real :: upwindGrad(3,field%nComp)
        !! field gradient at the upwind cell

        real :: rCf(3)
        !! cell-center to cell-face distance

        real :: CN(3)
        !! distance between two neighbouring cells

        real :: dUD(3)
        !! distance between upwind and dowind cell

        real :: dot
        !! auxiliary value storing a dot product

        real :: d1
        !! auxiliary value

        real :: d2
        !! auxiliary value

        real:: phiD(field%nComp)
        !! downwind cell center value

        real :: phiU(field%nComp)
        !! far upwind cell center value computed using gradients

        real :: phiC(field%nComp)
        !! upwind cell center value

        real :: phi_f
        !! face value

        real :: L
        !! distance weighting factor

        real :: switch
        !! auxiliary variable

        real :: psi
        !! face limiter for tvd schemes

        real :: corr
        !! correction to be assigned to fluxVf when used the deferred correction (default) approach.

        real :: d, absGradPhi, theta

        real :: cf, small
    !------------------------------------------------------------------------------------------------------------------

        pos = 0
        small = 1e-8

        fComp = field%nComp

        call interpolate(coeff, interpType='linear', opt=-1)

        ! Update convective fluxes
        do iFace=1,numberOfIntFaces

            cf = coeff%phif(iFace,1)

            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)

            ! Find index of the upwind cell
            if (mf(iFace,1)>=0) pos(iFace,1)=1

            switch = pos(iFace,1)

            iUpwind   =  pos(iFace,1)*iOwner + (1-pos(iFace,1))*iNeighbour
            iDownwind =  pos(iFace,1)*iNeighbour + (1-pos(iFace,1))*iOwner
            upwindGrad = field%phiGrad(iUpwind,:,:)

            ! Compute correction for HO schemes (Defferred correction approach)
            rCf = mesh%fcentroid(iFace,:) - mesh%centroid(iUpwind,:)
            dUD = mesh%centroid(iUpwind,:) - mesh%centroid(iDownwind,:)
            d = sqrt(dUD(1)**2+dUD(2)**2+dUD(3)**2)

            ! Compute inverse distance factor L
            call mag(mesh%CN(iFace,:), d1)
            call mag(rCf,d2)
            L = d1/d2

            ! Compute phiC, phiD, phiU
            phiC = field%phi(iUpwind, :)
            phiD = field%phi(iDownwind, :)

            do iComp=1, fComp

                dot = dot_product(field%phiGrad(iUpwind,:,iComp),dUD)
                phiU(iComp) = phiD(iComp) + 2*dot

                ! Compute theta
                absGradPhi = sqrt(upwindGrad(1,iComp)**2+upwindGrad(2,iComp)**2+upwindGrad(3,iComp)**2)
                dot = dot_product(upwindGrad(:,iComp), dUD)
                theta = acos(abs(dot/(absGradPhi*d+small)))

                ! TVD Schemes
                call TVD_HR_Schemes(phiC(iComp), phiD(iComp), phiU(iComp), L, phi_f, corr, psi, theta, convOpt)

                this%FluxC1f(iFace,iComp) = cf*mf(iFace,1)*(1-psi)*switch + mf(iFace,1)*psi*(1-switch)
                this%FluxC2f(iFace,iComp) = cf*mf(iFace,1)*psi*switch + mf(iFace,1)*(1-psi)*(1-switch)

            enddo

        enddo

        !----------------------!
        ! Processor Boundaries !
        !----------------------!

        do iBoundary=1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1
            pNeigh = 0

            do iBFace=is,ie

                pNeigh = pNeigh+1
                iOwner = mesh%owner(iBFace)

                cf = coeff%phif(iBFace,1)

                ! Compute correction for HO schemes (Defferred correction approach)
                if(mf(iBFace,1)>=0) pos(iBFace,1)=1

                switch = pos(iBFace,1)
                rCf = mesh%fcentroid(iBFace,:) - mesh%centroid(iOwner,:)*switch - mesh%ghost(pNeigh,:,iBoundary)*(1-switch)
                upwindGrad = field%phiGrad(iOwner,:,:)*switch + field%ghostsGrad(pNeigh,:,iBoundary,:)*(1-switch)

                ! Compute phiC, phiD, phiU
                phiC = field%phi(iOwner,:)*switch + field%ghosts(pNeigh,:,iBoundary)*(1-switch)
                phiD = field%ghosts(pNeigh,:,iBoundary)*switch + field%phi(iOwner,:)*(1-switch)
                dUD = mesh%centroid(iOwner,:)*(2*switch-1) - mesh%ghost(pNeigh,:,iBoundary)*(2*switch-1)
                d = sqrt(dUD(1)**2+dUD(2)**2+dUD(3)**2)

                ! Compute inverse distance factor L
                call mag(mesh%CN(iBFace,:), d1)
                call mag(rCf,d2)
                L = d1/d2

                do iComp=1, fComp

                    dot = dot_product(upwindGrad(:,iComp), dUD)
                    phiU(iComp) = phiD(iComp) + 2*dot

                    ! Compute theta
                    absGradPhi = sqrt(upwindGrad(1,iComp)**2+upwindGrad(2,iComp)**2+upwindGrad(3,iComp)**2)
                    dot = dot_product(upwindGrad(:,iComp), dUD)
                    theta = acos(abs(dot/(absGradPhi*d+small)))

                    ! TVD Schemes
                    call TVD_HR_Schemes(phiC(iComp), phiD(iComp), phiU(iComp), L, phi_f, corr, psi, theta, convOpt)

                    this%FluxC1f(iBFace,iComp) = cf*mf(iBFace,1)*(1-psi)*switch + mf(iBFace,1)*psi*(1-switch)
                    this%FluxC2f(iBFace,iComp) = cf*mf(iBFace,1)*psi*switch + mf(iBFace,1)*(1-psi)*(1-switch)

                enddo

            enddo

        enddo

    end subroutine assembleConvectiveTermImplicitWithCoeff

! *********************************************************************************************************************

    subroutine assembleConvectiveTermFf(this, field, convOpt, fComp)

    !===================================================================================================================
    ! Description:
    !! AssembleConvectiveTerm computes the convective fluxes using the face velocity (mf/rho_f) and not the mass flux.
    !===================================================================================================================

        class(convective_fluxes) :: this
        !! current class

        type(flubioField) :: field
        !! Transported field (e.g. velocity or concentration)
    !------------------------------------------------------------------------------------------------------------------

        integer :: iFace, iBFace, iOwner, iNeighbour, iUpwind, iDownwind, iBoundary, iComp, fComp, is, ie, iProc, pNeigh

        integer :: convOpt
        !! convection scheme to be used

        integer :: boundedCorr
        !! flag for the addition of the bounded correction term
    !------------------------------------------------------------------------------------------------------------------

        integer :: pos(numberOfFaces,1)
        !! auxiliary index to defie the upwind cell

        real :: phiC(fComp)
        !! cell center value

        real :: phi_f
        !! face value

        real :: L
        !! distance weighting factor

        real :: switch
        !! auxiliary variable

        real :: psi
        !! face limiter for tvd schemes

        real :: corr(field%nComp)
        !! correction to be assigned to fluxVf when used the deferred correction (default) approach.

        real :: Ff
    !------------------------------------------------------------------------------------------------------------------

        pos = 0

        !--------------------------------------------------!
        ! Interpolate the gradient for deferred correction !
        !--------------------------------------------------!

        if(convOpt==1) then
            call interpolate(field, interpType='linear', opt=-1)
        elseif(convOpt>=2 .and. convOpt<=4) then
            call interpolate(field, interpType='gradientbased', opt=convOpt)
        else
            call interpolate(field, interpType='tvd', opt=convOpt)
        end if

        !--------------------------!
        ! Update convective fluxes !
        !--------------------------!

        do iFace=1,numberOfIntFaces

            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)

            !-------------------------------!
            ! Find index of the upwind cell !
            !-------------------------------!

            if (mf(iFace,1)>=0) pos(iFace,1)=1

            Ff = mf(iFace,1)/rho%phif(iFace,1)
            iUpwind = pos(iFace,1)*iOwner + (1-pos(iFace,1))*iNeighbour

            phiC = field%phi(iUpwind, :)
            corr = field%phif(iFace,:) - phiC

            this%FluxC1f(iFace,:) = Ff*pos(iFace,1)
            this%FluxC2f(iFace,:) = Ff*(1-pos(iFace,1))
            this%FluxVf(iFace,:) = Ff*corr

        enddo

        !----------------------!
        ! Processor Boundaries !
        !----------------------!

        do iBoundary=1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1
            pNeigh = 0

            do iBFace=is,ie

                pNeigh = pNeigh+1
                iOwner = mesh%owner(iBFace)

                ! Compute correction for HO schemes
                if(mf(iBFace,1)>=0) pos(iBFace,1)=1

                Ff = mf(iBFace,1)/rho%phif(iBFace,1)
                switch = pos(iBFace,1)

                ! Compute phiC
                phiC = field%phi(iOwner,:)*switch + field%ghosts(pNeigh,:,iBoundary)*(1-switch)

                corr = field%phif(iBFace, :) - phiC
                this%FluxVf(iBFace, :) = Ff*corr

            enddo

        enddo

    end subroutine assembleConvectiveTermFf

! *********************************************************************************************************************

    subroutine assembleConvectiveTermImplicitFf(this, field, convOpt)

    !==================================================================================================================
    ! Description:
    !! assembleConvectiveTermImplicitFf assembles the convective fluxes using the face velocity (mf/rho_f)instead of the mass flux.
    !! It behaves exactly as assembleConvectiveTermImplicit.
    !==================================================================================================================

        class(convective_fluxes) :: this

        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        integer :: iFace, iBFace, iOwner, iNeighbour, iUpwind, iDownwind, iBoundary, iComp, fComp, is, ie, iProc, pNeigh

        integer :: convOpt
        !! convection scheme to be used

        integer :: boundedCorr
        !! flag for the addition of the bounded correction term

        integer :: pos(numberOfFaces,1)
        !! auxiliary index to defie the upwind cell
    !------------------------------------------------------------------------------------------------------------------

        real :: upwindGrad(3,field%nComp)
        !! field gradient at the upwind cell

        real :: rCf(3)
        !! cell-center to cell-face distance

        real :: CN(3)
        !! distance between two neighbouring cells

        real :: dUD(3)
        !! distance between upwind and dowind cell

        real :: dot
        !! auxiliary value storing a dot product

        real :: d1
        !! auxiliary value

        real :: d2
        !! auxiliary value

        real:: phiD(field%nComp)
        !! downwind cell center value

        real :: phiU(field%nComp)
        !! far upwind cell center value computed using gradients

        real :: phiC(field%nComp)
        !! upwind cell center value

        real :: phi_f
        !! face value

        real :: L
        !! distance weighting factor

        real :: switch
        !! auxiliary variable

        real :: psi
        !! face limiter for tvd schemes

        real :: corr
        !! correction to be assigned to fluxVf when used the deferred correction (default) approach.

        real :: d, absGradPhi, theta

        real :: Ff
        !! Face flux

        real :: small
    !------------------------------------------------------------------------------------------------------------------

        pos = 0
        small = 1e-8
        fComp = field%nComp

        ! Update convective fluxes
        do iFace=1,numberOfIntFaces

            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)

            ! Find index of the upwind cell
            if (mf(iFace,1)>=0) pos(iFace,1)=1
            Ff = mf(iFace,1)/rho%phif(iFace,1)

            switch = pos(iFace,1)

            iUpwind   =  pos(iFace,1)*iOwner + (1-pos(iFace,1))*iNeighbour
            iDownwind =  pos(iFace,1)*iNeighbour + (1-pos(iFace,1))*iOwner
            upwindGrad = field%phiGrad(iUpwind,:,:)

            ! Compute correction for HO schemes (Defferred correction approach)
            rCf = mesh%fcentroid(iFace,:) - mesh%centroid(iUpwind,:)
            dUD = mesh%centroid(iUpwind,:) - mesh%centroid(iDownwind,:)
            d = sqrt(dUD(1)**2+dUD(2)**2+dUD(3)**2)

            ! Compute inverse distance factor L
            call mag(mesh%CN(iFace,:), d1)
            call mag(rCf,d2)
            L = d1/d2

            ! Compute phiC, phiD, phiU
            phiC = field%phi(iUpwind, :)
            phiD = field%phi(iDownwind, :)

            do iComp=1, fComp

                dot = dot_product(field%phiGrad(iUpwind,:,iComp),dUD)
                phiU(iComp) = phiD(iComp) + 2*dot

                ! Compute theta
                absGradPhi = sqrt(upwindGrad(1,iComp)**2+upwindGrad(2,iComp)**2+upwindGrad(3,iComp)**2)
                dot = dot_product(upwindGrad(:,iComp), dUD)
                theta = acos(abs(dot/(absGradPhi*d+small)))

                ! TVD Schemes
                call TVD_HR_Schemes(phiC(iComp), phiD(iComp), phiU(iComp), L, phi_f, corr, psi, theta, convOpt)

                this%FluxC1f(iFace,iComp) = Ff*(1-psi)*switch + Ff*psi*(1-switch)
                this%FluxC2f(iFace,iComp) = Ff*psi*switch + Ff*(1-psi)*(1-switch)

            enddo

        enddo

        !----------------------!
        ! Processor Boundaries !
        !----------------------!

        do iBoundary=1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1
            pNeigh = 0

            do iBFace=is,ie

                pNeigh = pNeigh+1
                iOwner = mesh%owner(iBFace)

                ! Compute correction for HO schemes (Defferred correction approach)
                if(mf(iBFace,1)>=0) pos(iBFace,1)=1

                Ff = mf(iBFace,1)/rho%phif(iBFace,1)
                switch = pos(iBFace,1)
                rCf = mesh%fcentroid(iBFace,:) - mesh%centroid(iOwner,:)*switch - mesh%ghost(pNeigh,:,iBoundary)*(1-switch)
                upwindGrad = field%phiGrad(iOwner,:,:)*switch + field%ghostsGrad(pNeigh,:,iBoundary,:)*(1-switch)

                ! Compute phiC, phiD, phiU
                phiC = field%phi(iOwner,:)*switch + field%ghosts(pNeigh,:,iBoundary)*(1-switch)
                phiD = field%ghosts(pNeigh,:,iBoundary)*switch + field%phi(iOwner,:)*(1-switch)
                dUD = mesh%centroid(iOwner,:)*(2*switch-1) - mesh%ghost(pNeigh,:,iBoundary)*(2*switch-1)
                d = sqrt(dUD(1)**2+dUD(2)**2+dUD(3)**2)

                ! Compute inverse distance factor L
                call mag(mesh%CN(iBFace,:), d1)
                call mag(rCf,d2)
                L = d1/d2

                do iComp=1, fComp

                    dot = dot_product(upwindGrad(:,iComp), dUD)
                    phiU(iComp) = phiD(iComp) + 2*dot

                    ! Compute theta
                    absGradPhi = sqrt(upwindGrad(1,iComp)**2+upwindGrad(2,iComp)**2+upwindGrad(3,iComp)**2)
                    dot = dot_product(upwindGrad(:,iComp), dUD)
                    theta = acos(abs(dot/(absGradPhi*d+small)))

                    ! TVD Schemes
                    call TVD_HR_Schemes(phiC(iComp), phiD(iComp), phiU(iComp), L, phi_f, corr, psi, theta, convOpt)

                    this%FluxC1f(iBFace,iComp) = Ff*(1-psi)*switch + Ff*psi*(1-switch)
                    this%FluxC2f(iBFace,iComp) = Ff*psi*switch + Ff*(1-psi)*(1-switch)

                enddo

            enddo

        enddo

    end subroutine assembleConvectiveTermImplicitFf

! *********************************************************************************************************************

end module convectiveFluxes
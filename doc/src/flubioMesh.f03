!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module flubioMesh

!==================================================================================================================
! Description:
!! flubioMesh contains the mesh data structure and the methods to operate with it.
!==================================================================================================================

    use elements
    use globalMeshVar
    use flubioBoundaries
    use mpi
    use math
    use kdtree2_module

    implicit none

    type, public, extends(grid_elements) :: fvMesh

        type(fvBoundaries) :: boundaries

        type(kdtree2), pointer :: meshTree

        contains
            procedure :: readMeshFromFiles

            procedure :: setGlobalconn
            procedure :: findNeighbours
            procedure :: createMeshSearchTree
            procedure :: findPointOwnership

            procedure :: compute_geom
            procedure :: computeAreas
            procedure :: computeVolumes
            procedure :: computeInterpolationWeights
            procedure :: nonOrthogonalCorrection
            procedure :: pressureNonOrthogonalCorrection

            procedure :: mpi_create_ghosts_I
            procedure :: mpi_create_periodic_ghost_I
            procedure :: syncProcessorBoundaries

            procedure :: leastSquareMatrix

            procedure :: saveMesh

    end type fvMesh

contains

    subroutine readMeshFromFiles(this)

    !==================================================================================================================
    ! Description:
    !! readMeshFromFiles reads the mesh from the mesh files in the "grid/" folder.
    !==================================================================================================================

        class(fvMesh) :: this
    !------------------------------------------------------------------------------------------------------------------

        ! Starting message
        call flubioMsg('FLUBIO: initialising the mesh')

        !---------------!
        ! Internal mesh !
        !---------------!

        ! Read Points
        call this%readMeshPoints()

        ! Read Faces, Owner, Neighbours
        call this%readMeshFaces()

        ! Read cell global adress
        call this%readGlobalCellAddr()

        ! Build elements
        call this%buildElements()

        !---------------!
        ! Boundary mesh !
        !---------------!

        call this%boundaries%readBoundariesFromFiles()

        !----------------------------------------!
        ! Compute Control Volume characteristics !
        !----------------------------------------!

        call this%compute_geom()

        !--------------------!
        ! Set connectivities !
        !--------------------!

        call this%setGlobalConn()

        !----------------------!
        ! Save Decomposed Mesh !
        !----------------------!

        call this%saveMesh()

    end subroutine readMeshFromFiles

! *********************************************************************************************************************

    subroutine compute_geom(this)

    !==================================================================================================================
    ! Description:
    !! compute_geom calculates all the mesh geometrical information needed by FLUBIO (e.g. volumes, faces normals, etc. ).
    !==================================================================================================================

        use flubioDictionaries, only : flubioMeshRegions

        class(fvMesh) :: this
    !------------------------------------------------------------------------------------------------------------------

        ! Set number of processor boundaries (cyclic patches are recasted as processor boundaries)
        numberOfProcBound = this%boundaries%numberOfBoundaries-this%boundaries%numberOfRealBound+numberOfPeriodicBound

        ! Avoid 0-index running in serial
        if(numberOfProcBound==0) then
            numberOfProcBound1 = numberOfProcBound+1
        else
            numberOfProcBound1 = numberOfProcBound
        endif

        !------------------------------------------!
        ! Compute faces areas and elements volumes !
        !------------------------------------------!

        call this%computeAreas()
        call this%computeVolumes()

        !---------------------------------------!
        ! Create ghost nodes between processors !
        !---------------------------------------!

        call this%mpi_create_ghosts_I()

        !-------------------------------!
        ! Compute interpolation weights !
        !-------------------------------!

        call this%computeInterpolationWeights()

        !-------------------------!
        ! Find out the neighbours !
        !-------------------------!

        ! N.B: This routine is VITAL for the well beahviour of the parallelization and linear system assembling.
        ! DO NOT touch it unless you know very well what you are doing!

        call this%findNeighbours()

        !----------------------!
        ! Least Squares matrix !
        !----------------------!

        call this%leastSquareMatrix()

        !--------------------!
        ! Build Mesh Regions !
        !--------------------!

        if(flubioMeshRegions%found) call this%createMeshRegions()

    end subroutine compute_geom

! *********************************************************************************************************************

    subroutine computeVolumes(this)

    !==================================================================================================================
    ! Description:
    !! computeVolumes computes the cell volumes.
    !==================================================================================================================

        class(fvMesh) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, iNode, iFace, iBFace, nFaces, numberOfiNodes, csize

        integer, dimension(:), allocatable :: iFaces
    !------------------------------------------------------------------------------------------------------------------

        real :: centre(3), centroid(3), Sf(3), Cf(3)

        real :: localFaceSign, localVolumeSum, localVolume, volume, localVolumeCentroidSum(3), localCentroid(3)
    !------------------------------------------------------------------------------------------------------------------

        ! Starting message
        call flubioMsg('FLUBIO: computing elements centroids and volumes...')

        !-------------------------------------!
        ! Compute volume and element centroid !
        !-------------------------------------!

        allocate(this%centroid(numberOfElements,3))
        allocate(this%cellCenters(3,numberOfElements+numberOfBFaces+1))
        allocate(this%volume(numberOfElements))
        allocate(this%oldVolume(numberOfElements))

        do iElement=1,numberOfElements

            csize = this%iFaces(iElement)%csize
            allocate(iFaces(csize))
            numberOfElementFaces(iElement) = csize
            iFaces = this%iFaces(iElement)%col(1:csize)

            nFaces=numberOfElementFaces(iElement)
            centre = 0.0

            do iFace=1,nFaces
                centre = centre + this%fcentroid(iFaces(iFace),:)
            enddo

            centroid = 0.0
            Sf = 0.0

            centre = centre/nFaces
            localVolumeCentroidSum = 0.0

            localVolumeSum = 0.0

            do iFace=1,nFaces

                localFaceSign = this%elmFaceSign(iElement)%col(iFace)

                Sf = this%Sf(iFaces(iFace),:)*localFaceSign
                Cf = this%fcentroid(iFaces(iFace),:)-centre

                localVolume = Sf(1)*Cf(1)+Sf(2)*Cf(2)+Sf(3)*Cf(3)
                localVolume = localVolume/3.0
                localCentroid = 0.75*this%fcentroid(iFaces(iFace),:)+0.25*centre
                localVolumeCentroidSum = localVolumeCentroidSum + localCentroid*localVolume
                localVolumeSum = localVolumeSum + localVolume

            enddo

            centroid = localVolumeCentroidSum/localVolumeSum

            volume = localVolumeSum

            this%volume(iElement) = volume
            this%OldVolume(iElement) = volume
            this%centroid(iElement,:) = centroid

            deallocate(iFaces)

        enddo

    end subroutine computeVolumes

! *********************************************************************************************************************

    subroutine computeAreas(this)

    !==================================================================================================================
    ! Description:
    !! computeAreas computes the normal vector (direction and magnitude) mesh faces.
    !==================================================================================================================

        class(fvMesh) :: this
    !-------------------------------------------------------------------------------------------------------------------

        integer  iFace, iBFace, iNode, iTriangle, nFaces, numberOfiNodes

        integer, dimension(:), allocatable :: iNodes
    !------------------------------------------------------------------------------------------------------------------

        real point1(3), point2(3), point3(3), centre(3), v1(3), v2(3), cross(3)

        real centroid(3), local_centroid(3), Sf(3), area, local_area, local_Sf(3)
    !------------------------------------------------------------------------------------------------------------------

        ! Starting message
        call flubioMsg('FLUBIO: computing faces area and normals...')

        !---------------------!
        ! Compute Face center !
        !---------------------!

        nFaces = this%numberOfFaces

        allocate(this%fcentroid(nFaces,3))
        allocate(this%Sf(nFaces,3))
        allocate(this%nf(nFaces,3))
        allocate(this%area(nFaces))

        this%fcentroid = 0.0
        this%Sf = 0.0
        this%nf = 0.0
        this%area = 0.0

        !-----------------!
        ! Loop over faces !
        !-----------------!

        do iFace=1,nFaces

            numberOfiNodes = numberOfFaceNodes(iFace)
            allocate(iNodes(numberOfiNodes))

            iNodes = this%fvertex(iFace)%col(1:numberOfiNodes)
            centre = 0.0

            !--------------!
            ! Face Centers !
            !--------------!

            do iNode=1,numberOfiNodes
                centre = centre + this%vertex(iNodes(iNode),:)
            enddo

            centre = centre/numberOfiNodes

            !-------------------!
            ! Areas and normals !
            !-------------------!

            Sf = 0.0
            area = 0.0
            centroid = 0.0

            do iTriangle=1,numberOfiNodes

                point1 = centre
                point2 = this%vertex(iNodes(iTriangle),:)

                if (iTriangle<numberOfiNodes) then
                    point3 = this%vertex(iNodes(iTriangle+1),:)
                else
                    point3 = this%vertex(iNodes(1),:)
                endif

                local_centroid = (point1+point2+point3)/3.0
                v1 = point2-point1
                v2 = point3-point1

                call cross_prod(v1, v2, cross)

                local_Sf = 0.5*cross
                local_area=sqrt(local_Sf(1)**2+local_Sf(2)**2+local_Sf(3)**2)

                centroid = centroid + local_area*local_centroid
                Sf = Sf + local_Sf
                area = area + local_area

            enddo

            centroid = centroid/area
            this%fcentroid(iFace,:) = centroid
            this%Sf(iFace,:) = Sf
            this%area(iFace) = area
            this%nf(iFace,:) = Sf/area

            deallocate(iNodes)

        enddo

    end subroutine computeAreas

! *********************************************************************************************************************

    subroutine computeInterpolationWeights(this)

    !==================================================================================================================
    ! Description:
    !! computeInterpolationWeights computes the linear interpolation weights at each face.
    !==================================================================================================================

        class(fvMesh) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer i, iElement, iFace, iBFace, iBoundary, iProc, iOwn, iNeigh, pNeigh, is, ie
    !------------------------------------------------------------------------------------------------------------------

        real centroid(3), local_centroid(3), Sf(3), area, normSf, q, dot1, dot2

        real  CN(3), eCN(3), Cf(3), fF(3), ffp(3), Nfp(3), nf(3), Ef(3)
    !------------------------------------------------------------------------------------------------------------------

        ! Starting message
        call flubioMsg('FLUBIO: computing interpolation weights...')

        !---------!
        ! Prepare !
        !---------!

        allocate(this%CN(numberOfFaces,3))
        allocate(this%eCN(numberOfFaces,3))
        allocate(this%Tf(numberOfFaces,3))
        allocate(this%Ef(numberOfFaces,3))
        allocate(this%walldist(numberOfFaces))
        allocate(this%gf(numberOfFaces))
        allocate(this%gDiff(numberOfFaces))
        allocate(this%skw_gf(numberOfFaces))
        allocate(this%fcentroid_skw(numberOfFaces,3))

        this%gf = 0.0
        this%skw_gf = 0.0
        this%fcentroid_skw = 0.0

        ! Loop over internal faces
        do iFace=1,numberOfIntFaces

            nf = this%Sf(iFace,:)/this%area(iFace)

            iOwn = this%owner(iFace)
            iNeigh = this%neighbour(iFace)

            CN = this%centroid(iNeigh,:)-this%centroid(iOwn,:)
            this%CN(iFace,:) = CN

            eCN = CN/sqrt((CN(1))**2+(CN(2))**2+(CN(3))**2)
            this%eCN(iFace,:) = eCN

            !----------------------------------!
            ! Non orthogonal correction method !
            !----------------------------------!

            dot1 = dot_product(eCN,this%Sf(iFace,:))
            call this%nonOrthogonalCorrection(iFace, Ef)

            this%Ef(iFace,:) = Ef
            this%Tf(iFace,:) = this%Sf(iFace,:)-Ef
            this%gDiff(iFace) = sqrt((Ef(1))**2+(Ef(2))**2+(Ef(3))**2)/sqrt((CN(1))**2+(CN(2))**2+(CN(3))**2)

            !-----------------------!
            ! Interpolation weigths !
            !-----------------------!

            Cf = this%fcentroid(iFace,:) - this%centroid(iOwn,:)
            fF = this%centroid(iNeigh,:) - this%fcentroid(iFace,:)

            dot1 = dot_product(Cf,nf)
            dot2 = dot_product(fF,nf)

            this%gf(iFace) = dot1/(dot1+dot2)
            this%walldist(iFace) = 0.0

            !--------------------------------!
            ! Skewness interpolation weigths !
            !--------------------------------!

            dot1 = dot_product(Cf,CN)
            dot2 = dot_product(CN,CN)

            q = dot1/dot2 ! + neigh to own

            ffp = this%centroid(iOwn,:) + q*CN
            Nfp = this%centroid(iNeigh,:) - ffp

            call mag(Nfp,dot1)
            call mag(CN,dot2)

            this%skw_gf(iFace) = dot1/dot2
            this%fcentroid_skw(iFace,:) = ffp

        enddo

        !----------------------------------!
        ! Boundary faces: real + processor !
        !----------------------------------!

        i = 0
        do iBoundary=1,numberOfBoundaries

            is = this%boundaries%startFace(iBoundary)
            ie = is+this%boundaries%nFace(iBoundary)-1
            pNeigh = 0

            if(this%boundaries%realBound(iBoundary)==-1) i=i+1

            do iBFace=is, ie

                iOwn = this%owner(iBFace)

                nf = this%Sf(iBFace,:)/this%area(iBFace)

                if(this%boundaries%realBound(iBoundary)/=-1) then
                    CN = this%fcentroid(iBFace,:)-this%centroid(iOwn,:)
                else
                    pNeigh = pNeigh+1
                    CN = (this%ghost(pNeigh,:,i)-this%centroid(iOwn,:))
                endif

                this%CN(iBFace,:) = CN

                eCN = CN/sqrt((CN(1))**2+(CN(2))**2+(CN(3))**2)
                this%eCN(iBFace,:) = eCN

                !----------------------------------!
                ! Non orthogonal correction method !
                !----------------------------------!

                call this%nonOrthogonalCorrection(iBFace, Ef)

                this%Ef(iBFace,:) = Ef
                this%Tf(iBFace,:) = this%Sf(iBFace,:)-Ef
                this%gDiff(iBFace) = sqrt((Ef(1))**2+(Ef(2))**2+(Ef(3))**2)/sqrt((CN(1))**2+(CN(2))**2+(CN(3))**2)

                !--------------------------------!
                ! Skewness interpolation weigths !
                !--------------------------------!

                this%walldist(iBFace) = 0.0

                dot1 = dot_product(CN,this%Sf(iBFace,:))

                normSf = (this%Sf(iBFace,1))**2+(this%Sf(iBFace,2))**2+(this%Sf(iBFace,3))**2

                this%walldist(iBFace) = normSf/dot1

                if(this%boundaries%realBound(iBoundary)/=-1) then

                    this%gf(iBFace) = 1.0
                    this%skw_gf(iBFace) = 1.0

                else

                    Cf = this%fcentroid(iBFace,:) - this%centroid(iOwn,:)
                    fF = this%ghost(pNeigh,:,i) - this%fcentroid(iBFace,:)

                    dot1 = dot_product(Cf,nf)
                    dot2 = dot_product(fF,nf)

                    this%gf(iBFace) = dot1/(dot1+dot2)

                    !--------------------------------!
                    ! Skewness interpolation weigths !
                    !--------------------------------!

                    dot1 = dot_product(Cf,CN)
                    dot2 = dot_product(CN,CN)

                    q = dot1/dot2 ! + neigh to own

                    ffp = this%centroid(iOwn,:) + q*CN
                    Nfp = this%ghost(pNeigh,:,i) - ffp

                    call mag(Nfp,dot1)
                    call mag(CN,dot2)

                    this%skw_gf(iBFace) = dot1/dot2
                    this%fcentroid_skw(iBFace,:) = ffp

                endif

            enddo

        enddo

    end subroutine computeInterpolationWeights

! *********************************************************************************************************************

    subroutine nonOrthogonalCorrection(this, iFace, Ef)

    !==================================================================================================================
    ! Description:
    !! nonOrthogonalCorrection returns one of the non-orthogolal decomposion method.
    !==================================================================================================================

        use flubioDictionaries
    !------------------------------------------------------------------------------------------------------------------

        class(fvMesh) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iFace
    !------------------------------------------------------------------------------------------------------------------

        real :: dot, Ef(3)
    !------------------------------------------------------------------------------------------------------------------

        ! Over relaxed approach
        if(flubioOptions%nonOrthMethod==0) then

            dot = dot_product(this%eCN(iFace,:),this%Sf(iFace,:))
            Ef = (this%area(iFace)**2/dot)*this%eCN(iFace,:)

        ! Orhogonal correction approach
        elseif(flubioOptions%nonOrthMethod==1) then

            Ef = this%area(iFace)*this%eCN(iFace,:)

        ! Mimum correction approach
        elseif(flubioOptions%nonOrthMethod==2) then

            dot = dot_product(this%eCN(iFace,:),this%Sf(iFace,:))
            Ef = dot*this%eCN(iFace,:)

        else

            dot = dot_product(this%eCN(iFace,:),this%Sf(iFace,:))
            Ef = (this%area(iFace)**2/dot)*this%eCN(iFace,:)

        endif

    end subroutine nonOrthogonalCorrection

! *********************************************************************************************************************

    subroutine pressureNonOrthogonalCorrection(this, iFace, Sf, Df)

    !==================================================================================================================
    ! Description:
    !! nonOrthogonalCorrection returns a non-orthogolal decomposion for the pressure-correction laplace operator.
    !==================================================================================================================

        use flubioDictionaries
    !------------------------------------------------------------------------------------------------------------------

        class(fvMesh) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iFace
    !------------------------------------------------------------------------------------------------------------------

        real :: dot, dot1, Df, Sf(3)
    !------------------------------------------------------------------------------------------------------------------

        ! Over relaxed approach
        if(flubioOptions%nonOrthMethod==0) then

            dot = dot_product(Sf,Sf)
            dot1 = dot_product(this%CN(iFace,:),Sf)
            Df = dot/dot1

        ! Orhogonal correction approach
        elseif(flubioOptions%nonOrthMethod==1) then

            dot = dot_product(this%CN(iFace,:),Sf)
            dot1 = dot_product(this%CN(iFace,:),this%CN(iFace,:))
            Df = sqrt(dot/dot1)

        ! Mimum correction approach
        elseif(flubioOptions%nonOrthMethod==2) then

            dot = dot_product(this%CN(iFace,:),Sf)
            dot1 = dot_product(this%CN(iFace,:),this%CN(iFace,:))
            Df = dot/dot1

        else

            dot = dot_product(Sf,Sf)
            dot1 = dot_product(this%CN(iFace,:),Sf)
            Df = dot/dot1

        endif

    end subroutine  pressureNonOrthogonalCorrection

! *********************************************************************************************************************

    subroutine setGlobalConn(this)

    !==================================================================================================================
    ! Description:
    !! setGlobalConn build the global connectivity table between the elements.
    !==================================================================================================================

        class(fvMesh) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iBoundary, iElement, iFace, iBFace, iOwner, iNeigh, iProc, pNeigh

        integer :: iOwnerNeighbourCoef, localNeigh, is ,ie
    !------------------------------------------------------------------------------------------------------------------

        ! This must be deleted once the structure thing is done
        maxNeigh = 6

        !--------------------------!
        ! Build connectivity table !
        !--------------------------!

        do iElement=1,numberOfElements
            do iNeigh=1, this%iNeighbours(iElement)%csize
                if(this%iNeighbours(iElement)%col(iNeigh)>0) then
                    localNeigh = this%iNeighbours(iElement)%col(iNeigh)
                    this%conn(iElement)%col(iNeigh) = this%cellGlobalAddr(localNeigh)
                endif
            enddo

        enddo

        ! Extend to Processor boundary
        i=0
        do iBoundary=1,numberOfProcBound

            iProc = this%boundaries%procBound(iBoundary)
            is = this%boundaries%startFace(iProc)
            ie = is+this%boundaries%nFace(iProc)-1
            pNeigh = 0
            i = i+1

            do iBFace=is,ie

                pNeigh = pNeigh+1

                iOwner = this%owner(iBFace)
                iOwnerNeighbourCoef = this%iOwnerNeighbourCoef(iBFace)

                this%conn(iOwner)%col(iOwnerNeighbourCoef) = this%neigh(pNeigh,i)
                this%iNeighbours(iOwner)%col(iOwnerNeighbourCoef) = this%neigh(pNeigh,i)

            enddo

        enddo

    end subroutine setGlobalConn

! *********************************************************************************************************************

    subroutine findNeighbours(this)

    !===================================================================================================================
    ! Description:
    !! findNeighbours finds the number of neighbours for each element composing the mesh.
    !===================================================================================================================

        class(fvMesh) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iElement, iFace, iBFace, iBoundary, iProc, iOwn, iNeigh, pNeigh, kf, is ,ie
    !------------------------------------------------------------------------------------------------------------------

        integer :: nn, nFaces, numberOfLocalInteriorFaces

        integer, dimension(:), allocatable :: iFaces, iNeighbours
    !------------------------------------------------------------------------------------------------------------------

        ! Starting message
        call flubioMsg('FLUBIO: creating mesh connectivities...')

        !---------!
        ! Prepare !
        !---------!

        allocate(this%iOwnerNeighbourCoef(numberOfFaces))
        allocate(this%iNeighbourOwnerCoef(numberOfFaces))

        this%iOwnerNeighbourCoef = 0
        this%iNeighbourOwnerCoef = 0

        ! Loop over elements
        do iElement=1,numberOfElements

            nFaces = this%iFaces(iElement)%csize
            allocate(iFaces(nFaces))
            iFaces = this%iFaces(iElement)%col(1:nFaces)

            nn = this%iNeighbours(iElement)%csize
            allocate(iNeighbours(nn))

            iNeighbours = this%iNeighbours(iElement)%col(1:nn)
            kf = 1

            numberOfLocalInteriorFaces = numberOfElementFaces(iElement)

            do i=1, nn !numberOfLocalInteriorFaces

                if(this%owner(iFaces(i))==iElement .and. iNeighbours(i)/=0 ) then

                    this%iOwnerNeighbourCoef(iFaces(i)) = kf

                elseif(this%Neighbour(iFaces(i))==iElement .and. iNeighbours(i)/=0) then

                    this%iNeighbourOwnerCoef(iFaces(i)) = kf

                endif

                kf = kf+1

            enddo

            deallocate(iNeighbours)
            deallocate(iFaces)

        enddo

        !------------------------------------------------------!
        ! Increase by 1 the neighbours at processor's boundary !
        !------------------------------------------------------!

        do iBoundary=1,numberOfProcBound

            iProc = this%boundaries%procBound(iBoundary)
            is = this%boundaries%startFace(iProc)
            ie = is+this%boundaries%nFace(iProc)-1

            do iBFace=is,ie

                call getNeighSize(this, kf, iBFace)

                this%iOwnerNeighbourCoef(iBFace)=kf+1
                iOwn = this%owner(iBFace)
                this%numberOfNeighbours(iOwn) = this%numberOfNeighbours(iOwn)+1

            enddo

        enddo

    end subroutine findNeighbours

! *********************************************************************************************************************

    subroutine leastSquareMatrix(this)

    !==================================================================================================================
    ! Description:
    !! leastSquareMatrix computes the least square matrix to be used in least squares gradient calculation or interolation.
    !==================================================================================================================

        class(fvMesh) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, is, ie

        integer :: pNeigh, iBoundary
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, iFace, iBFace, iOwner, iNeighbour, iProc
    !------------------------------------------------------------------------------------------------------------------

        real :: w
        !! LSQ weight

    !------------------------------------------------------------------------------------------------------------------

        !---------!
        ! Prepare !
        !---------!

        allocate(this%lsq_mat(numberOfElements,6))
        this%lsq_mat = 0.

        !----------------!
        ! Internal faces !
        !----------------!

        do iFace=1,numberOfIntFaces

            iOwner = this%owner(iFace)
            iNeighbour = this%neighbour(iFace)

            ! Compute weights
            w = 1./sqrt(this%CN(iFace,1)**2 + this%CN(iFace,2)**2 + this%CN(iFace,3)**2)

            ! Compute least square matrix (ordering: xx, yy, zz, xy, xz, yz)

            ! Owner cell
            this%lsq_mat(iOwner,1) = this%lsq_mat(iOwner,1) + w*this%CN(iFace,1)**2
            this%lsq_mat(iOwner,2) = this%lsq_mat(iOwner,2) + w*this%CN(iFace,2)**2
            this%lsq_mat(iOwner,3) = this%lsq_mat(iOwner,3) + w*this%CN(iFace,3)**2
            this%lsq_mat(iOwner,4) = this%lsq_mat(iOwner,4) + w*this%CN(iFace,1)*this%CN(iFace,2)
            this%lsq_mat(iOwner,5) = this%lsq_mat(iOwner,5) + w*this%CN(iFace,1)*this%CN(iFace,3)
            this%lsq_mat(iOwner,6) = this%lsq_mat(iOwner,6) + w*this%CN(iFace,2)*this%CN(iFace,3)

            ! Neighbour cell
            this%lsq_mat(iNeighbour,1) = this%lsq_mat(iNeighbour,1) + w*this%CN(iFace,1)**2
            this%lsq_mat(iNeighbour,2) = this%lsq_mat(iNeighbour,2) + w*this%CN(iFace,2)**2
            this%lsq_mat(iNeighbour,3) = this%lsq_mat(iNeighbour,3) + w*this%CN(iFace,3)**2
            this%lsq_mat(iNeighbour,4) = this%lsq_mat(iNeighbour,4) + w*this%CN(iFace,1)*this%CN(iFace,2)
            this%lsq_mat(iNeighbour,5) = this%lsq_mat(iNeighbour,5) + w*this%CN(iFace,1)*this%CN(iFace,3)
            this%lsq_mat(iNeighbour,6) = this%lsq_mat(iNeighbour,6) + w*this%CN(iFace,2)*this%CN(iFace,3)

        enddo

        !----------------------!
        ! Processor Boundaries !
        !----------------------!

        do iBoundary=1,numberOfProcBound

            iProc=this%boundaries%procBound(iBoundary)
            is=this%boundaries%startFace(iProc)
            ie=is+this%boundaries%nFace(iProc)-1

            pNeigh=0

            do iBFace=is,ie

                pNeigh=pNeigh+1
                iOwner=this%owner(iBFace)

                ! Compute weights
                w = 1./sqrt(this%CN(iBFace,1)**2 + this%CN(iBFace,2)**2 + this%CN(iBFace,3)**2)

                ! compute least square matrix (ordering: xx, yy, zz, xy, xz, yz)
                this%lsq_mat(iOwner,1) = this%lsq_mat(iOwner,1) + w*this%CN(iBFace,1)**2
                this%lsq_mat(iOwner,2) = this%lsq_mat(iOwner,2) + w*this%CN(iBFace,2)**2
                this%lsq_mat(iOwner,3) = this%lsq_mat(iOwner,3) + w*this%CN(iBFace,3)**2
                this%lsq_mat(iOwner,4) = this%lsq_mat(iOwner,4) + w*this%CN(iBFace,1)*this%CN(iBFace,2)
                this%lsq_mat(iOwner,5) = this%lsq_mat(iOwner,5) + w*this%CN(iBFace,1)*this%CN(iBFace,3)
                this%lsq_mat(iOwner,6) = this%lsq_mat(iOwner,6) + w*this%CN(iBFace,2)*this%CN(iBFace,3)

            enddo

        enddo

        !-----------------!
        ! Real Boundaries !
        !-----------------!

        do iBoundary=1,this%boundaries%numberOfRealBound

            is=this%boundaries%startFace(iBoundary)
            ie=is+this%boundaries%nFace(iBoundary)-1

            do iBFace=is,ie

                iOwner=this%owner(iBFace)

                ! Compute weights
                w = 1./sqrt(this%CN(iBFace,1)**2 + this%CN(iBFace,2)**2 + this%CN(iBFace,3)**2)

                ! compute least square matrix (ordering: xx, yy, zz, xy, xz, yz)
                this%lsq_mat(iOwner,1) = this%lsq_mat(iOwner,1) + w*this%CN(iBFace,1)**2
                this%lsq_mat(iOwner,2) = this%lsq_mat(iOwner,2) + w*this%CN(iBFace,2)**2
                this%lsq_mat(iOwner,3) = this%lsq_mat(iOwner,3) + w*this%CN(iBFace,3)**2
                this%lsq_mat(iOwner,4) = this%lsq_mat(iOwner,4) + w*this%CN(iBFace,1)*this%CN(iBFace,2)
                this%lsq_mat(iOwner,5) = this%lsq_mat(iOwner,5) + w*this%CN(iBFace,1)*this%CN(iBFace,3)
                this%lsq_mat(iOwner,6) = this%lsq_mat(iOwner,6) + w*this%CN(iBFace,2)*this%CN(iBFace,3)

            enddo

        enddo

    end subroutine leastSquareMatrix

! *********************************************************************************************************************

    subroutine saveMesh(this)

    !==================================================================================================================
    ! Description:
    !! save the mesh data in a binary file.
    !==================================================================================================================

        class(fvMesh) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(10) :: procID
        !! processor ID
    !------------------------------------------------------------------------------------------------------------------

        integer :: m, n

        integer, dimension(:,:), allocatable :: faceVertices

        integer, dimension(:,:), allocatable :: elementFaces
    !------------------------------------------------------------------------------------------------------------------

        write(procID,'(i0)') id

        n = maxval(numberOfFaceNodes)
        allocate(faceVertices(numberOfFaces, n))

        m = maxval(numberOfElementFaces)
        allocate(elementFaces(numberOfFaces, m))

        faceVertices = cellToIntArray(this%fvertex, numberOfFaces, n)
        elementFaces = cellToIntArray(this%iFaces, numberOfElements, m)

        open(1,file='grid/bin/mesh-d'//trim(procID)//'.bin',form='unformatted')
            write(1) numberOfPoints, numberOfFaces, numberOfIntFaces, numberOfBFaces, numberOfElements, numberOfBElements
            write(1) numberOfBoundaries
            write(1) this%vertex
            write(1) faceVertices
            write(1) elementFaces
            write(1) this%centroid

            ! boundaries
            write(1) numberOfBoundaries,  this%boundaries%numberOfRealBound, this%boundaries%startFace, this%boundaries%nFace
        close(1)

        deallocate(faceVertices)

        call flubioMsg('FLUBIO: mesh imported successfully! ')

    end subroutine saveMesh

!**********************************************************************************************************************!
!						                         Auxiliary subroutines							                                 !
!**********************************************************************************************************************!

    subroutine getNeighSize(this, k, iBFace)

    !==================================================================================================================
    ! Description:
    !! getNeighSize finds out the number of neighbours sorrounding one target cell.
    !==================================================================================================================

        class(fvMesh) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, k, iFace, iBFace, iOwner
    !------------------------------------------------------------------------------------------------------------------

        iOwner=this%owner(iBFace)

        k=0
        do i=1, numberOfElementFaces(iOwner)
            iFace = this%iFaces(iOwner)%col(i)
            if(this%iOwnerNeighbourCoef(iFace)>0) k=k+1
        enddo

    end subroutine getNeighSize

!**********************************************************************************************************************

    subroutine mpi_create_ghosts_I(this)

    !==================================================================================================================
    ! Description:
    !! mpi_create_ghosts_I creates the ghost cells and connectivities between two partions.
    !==================================================================================================================

        class(fvMesh) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: ierr
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, j, iBoundary, iElement, iFace, iOwner, is, ie

        integer :: nbuf, nFace
    !------------------------------------------------------------------------------------------------------------------

        integer :: iSend, iRecv, istat(2*numberOfProcBound1), ireq(2*numberOfProcBound1)

        integer :: nSend, nRecv, idr, istats(MPI_STATUS_SIZE)

        integer :: buffSize

        integer, dimension(:,:), allocatable :: sbufn, rbufn

        real, dimension(:,:,:), allocatable :: sbuf, rbuf
    !------------------------------------------------------------------------------------------------------------------

        ! Starting message
        call flubioMsg('FLUBIO: creating the ghosts cells at the processor boundaries...')

        ! ---------------------------------------------------------!
        ! Send and Receive the cell center of neighbour processors !
        ! ---------------------------------------------------------!

        allocate(this%ghost(this%boundaries%maxProcFaces,3,numberOfProcBound1))
        this%ghost = 0.0

        ! Prepare the send buffers
        buffSize = this%boundaries%maxProcFaces

        allocate(sbuf(buffSize,3,numberOfProcBound1))
        allocate(rbuf(buffSize,3,numberOfProcBound1))

        sbuf = 0.0
        rbuf = 0.0

        do iBoundary=1,numberOfProcBound

            i = this%boundaries%procBound(iBoundary)
            is = this%boundaries%startFace(i)
            ie = is+this%boundaries%nFace(i)-1

            ! Fill the send buffer
            nbuf = 0

            do iFace=is,ie
                nbuf = nbuf+1
                iOwner = this%owner(iFace)
                sbuf(nbuf,:,iBoundary) = this%centroid(iOwner,:)
            enddo

        enddo

        ! Perform the comunication between the processors
        nSend = numberOfProcBound
        nRecv = numberOfProcBound

        ! Open senders
        do iSend=1,nSend

            i = this%boundaries%procBound(iSend)
            idr = this%boundaries%neighProc(i)

            buffSize = this%boundaries%maxProcFaces*3
            call mpi_isend(sbuf(1:this%boundaries%maxProcFaces,:,iSend),buffSize,MPI_REAL8,idr,0,MPI_COMM_WORLD,ireq(iSend),ierr)

        enddo

        ! Open receivers
        do iRecv=1,nRecv

            i = this%boundaries%procBound(iRecv)
            idr = this%boundaries%neighProc(i)

            buffSize = this%boundaries%maxProcFaces*3
            call mpi_irecv(rbuf(1:this%boundaries%maxProcFaces,:,iRecv),buffSize,MPI_REAL8,idr,0,MPI_COMM_WORLD,ireq(nSend+iRecv),ierr)

        enddo

        ! Wait the end of the communications
        call mpi_waitAll(nSend+nRecv,ireq,MPI_STATUSES_IGNORE,ierr)
        this%ghost = rbuf
        ! Wait all the processor before advancing
        call mpi_barrier(MPI_COMM_WORLD, ierr)

        deallocate(sbuf)
        deallocate(rbuf)

        !------------------------------------------------------------------!
        ! Send and Receive the cell global address of neighbour processors !
        !------------------------------------------------------------------!

        allocate(this%neigh(this%boundaries%maxProcFaces,numberOfProcBound1))
        this%neigh = 0

        ! Prepare the send buffers
        buffSize = this%boundaries%maxProcFaces

        allocate(sbufn(buffSize,numberOfProcBound1))
        allocate(rbufn(buffSize,numberOfProcBound1))

        sbufn = 0
        rbufn = 0

        do iBoundary=1,numberOfProcBound

            i = this%boundaries%procBound(iBoundary)
            is = this%boundaries%startFace(i)
            ie = is+this%boundaries%nFace(i)-1

            ! Fill the send buffer
            nbuf = 0

            do iFace=is,ie
                nbuf = nbuf+1
                iOwner = this%owner(iFace)
                sbufn(nbuf,iBoundary) = this%cellGlobalAddr(iOwner)
            enddo

        enddo

        ! Open senders
        do iSend=1,nSend

            i = this%boundaries%procBound(iSend)
            idr = this%boundaries%neighProc(i)

            buffSize = this%boundaries%maxProcFaces
            call mpi_isend(sbufn(1:this%boundaries%maxProcFaces,iSend),buffSize,MPI_INT,idr,0,MPI_COMM_WORLD,ireq(iSend),ierr)

        enddo

        ! Open receivers
        do iRecv=1,nRecv

            i = this%boundaries%procBound(iRecv)
            idr = this%boundaries%neighProc(i)

            buffSize = this%boundaries%maxProcFaces
            call mpi_irecv(rbufn(1:this%boundaries%maxProcFaces,iRecv),buffSize,MPI_INT,idr,0,MPI_COMM_WORLD,ireq(nSend+iRecv),ierr)

        enddo

        ! Wait the end of the communications
        call mpi_waitAll(nSend+nRecv,ireq,MPI_STATUSES_IGNORE,ierr)

        this%neigh = rbufn

        ! Deallocate buffers
        deallocate(sbufn)
        deallocate(rbufn)

        if(totalPerBound/=0) then
            ! Invert halos for the cyclic patches owned by the same processor
            !if(numberOfProcCyclic/=0) call correctCyclicHalos
            ! Compute the offset vector for periodic faces
            call this%mpi_create_periodic_ghost_I()
        endif

        ! Wait all the processor before advancing
        call mpi_barrier(MPI_COMM_WORLD, ierr)

    end subroutine mpi_create_ghosts_I

!**********************************************************************************************************************

    subroutine mpi_create_periodic_ghost_I(this)

    !==================================================================================================================
    ! Description:
    !! mpi_create_periodic_ghost_I creates periodicity between two partitions.
    !==================================================================================================================

        class(fvMesh) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=3) procID
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, j, iBoundary, iFace, iBFace, iOwner, is, ie

        integer :: nFace, nPeriodic, pNeigh, iNeigh, iProc
    !------------------------------------------------------------------------------------------------------------------

        real :: delta(3)
        !! distance between cell center and boundary face center
    !------------------------------------------------------------------------------------------------------------------

        write(procID,'(i0)') id

        ! Get the maximum sizes of the periodic faces
        call this%boundaries%getPeriodicFacesSize()

        allocate(this%periodicFaces(this%boundaries%maxPerFaces,3,totalPerBound))
        this%periodicFaces = 0.0

        allocate(this%poffset(this%boundaries%maxPerFaces,3,totalPerBound))
        this%poffset = 0.0

        ! Syncronize periodic ghosts
        call this%syncProcessorBoundaries()

        ! Compute offset vector
        do iBoundary=1,totalPerBound

            i = this%boundaries%perBound(iBoundary)
            j = this%boundaries%getCyclicPatch(iBoundary)
            is = this%boundaries%startFace(i)
            ie = is+this%boundaries%nFace(i)-1
            pNeigh = 0

            do iFace=is,ie

                pNeigh=pNeigh+1
                iOwner = this%owner(iFace)

            !    this%poffset(pNeigh,:,iBoundary) = this%periodicFaces(pNeigh,:,iBoundary)-this%fcentroid(iFace,:)
                delta = 2.0*(this%centroid(iOwner,:)- this%fcentroid(iFace,:))
                this%poffset(pNeigh,:,iBoundary) = (this%centroid(iOwner,:) - this%ghost(pNeigh,:,j)) - delta

                ! Correct the cell centroid with the offset
                this%ghost(pNeigh,:,j) = this%ghost(pNeigh,:,j) + this%poffset(pNeigh,:,iBoundary)

            enddo
        enddo

    end subroutine mpi_create_periodic_ghost_I

! *********************************************************************************************************************

    subroutine syncProcessorBoundaries(this)

    !==================================================================================================================
    ! Description:
    !! syncProcessorBoundaries reoders the position of the ghost cells beloging to processor boundaries.
    !==================================================================================================================

        use kdtree2_module
    !------------------------------------------------------------------------------------------------------------------

        class(fvMesh) :: this

        type(kdtree2), pointer :: tree

        type(kdtree2_result), allocatable :: nearestNeighbours(:)
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, j, iBoundary, iElement, iFace, iBFace, iGhost, iOwner, is, ie

        integer :: nFace, nPeriodic, pNeigh, iNeigh, iProc

        integer :: numberOfNeighbours

        integer, dimension(:,:), allocatable :: new_neigh
    !------------------------------------------------------------------------------------------------------------------

        real, dimension(:,:), allocatable :: ghosts, new_ghosts
    !------------------------------------------------------------------------------------------------------------------

        numberOfNeighbours = 1

        ! number of neighbours to look for
        allocate(nearestNeighbours(numberOfNeighbours))

        do iBoundary=1,totalPerBound

            iProc = this%boundaries%perBound(iBoundary)
            iGhost = this%boundaries%getCyclicPatch(iBoundary)
            is = this%boundaries%startFace(iProc)
            ie = is+this%boundaries%nFace(iProc)-1
            pNeigh = 0

            allocate(ghosts(3, this%boundaries%nFace(iProc)+1))
            allocate(new_ghosts(this%boundaries%nFace(iProc), 3))
            allocate(new_neigh(this%boundaries%nFace(iProc), 1))

            ghosts(1, 1:this%boundaries%nFace(iProc)) = this%ghost(1:this%boundaries%nFace(iProc), 1, iGhost)
            ghosts(2, 1:this%boundaries%nFace(iProc)) = this%ghost(1:this%boundaries%nFace(iProc), 2, iGhost)
            ghosts(3, 1:this%boundaries%nFace(iProc)) = this%ghost(1:this%boundaries%nFace(iProc), 3, iGhost)
            new_ghosts = 0.0

            tree => kdtree2_create(ghosts, rearrange=.true., sort=.true.)

            do iBFace = is,ie

                pNeigh = pNeigh+1
                iOwner = this%owner(iBFace)

                ghosts(1:3,this%boundaries%nFace(iProc)+1) = this%centroid(iOwner, 1:3)

                call kdtree2_n_nearest_around_point(tree, this%boundaries%nFace(iProc)+1, numberOfNeighbours, numberOfNeighbours, nearestNeighbours)

                ! Rearrange ghosts
                new_ghosts(pNeigh, 1:3) = this%ghost(nearestNeighbours(1)%idx, 1:3, iGhost)
                new_neigh(pNeigh, 1) = this%neigh(nearestNeighbours(1)%idx, iGhost)
                this%boundaries%periodicBoundaryMapping(pNeigh, iBoundary) = nearestNeighbours(1)%idx

            enddo

            this%ghost(1:this%boundaries%nFace(iProc), 1:3, iGhost) = new_ghosts(1:this%boundaries%nFace(iProc),:)
            this%neigh(1:this%boundaries%nFace(iProc), iGhost) = new_neigh(1:this%boundaries%nFace(iProc),1)

            ! destroy and re-create for the new boundary
            deallocate(ghosts)
            deallocate(new_ghosts)
            deallocate(new_neigh)
            call kdtree2_destroy(tree)

        enddo

    end subroutine syncProcessorBoundaries

!**********************************************************************************************************************

    subroutine createMeshSearchTree(this)

        implicit none

        class(fvMesh) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iBFace, is, ie, iB, iRank, pNeigh, numberOfNeighbours
    !------------------------------------------------------------------------------------------------------------------

        real :: cellCenters(3, numberOfElements+numberOfBFaces+1)
    !------------------------------------------------------------------------------------------------------------------

        call flubioMsg('FLUBIO: creating mesh kd-tree...')

        ! Centroids
        this%cellCenters(1,1:numberOfElements) = this%centroid(1:numberOfElements,1)
        this%cellCenters(2,1:numberOfElements) = this%centroid(1:numberOfElements,2)
        this%cellCenters(3,1:numberOfElements) = this%centroid(1:numberOfElements,3)

        ! Real boundaries
        iB = 0
        do iBoundary=1,numberOfRealBound

            is = this%boundaries%startFace(iBoundary)
            ie = is+this%boundaries%nFace(iBoundary)-1

            do iBFace=is,ie
                iB = iB+1
                this%cellCenters(1,numberOfElements+iB) = this%fcentroid(iBFace,1)
                this%cellCenters(2,numberOfElements+iB) = this%fcentroid(iBFace,2)
                this%cellCenters(3,numberOfElements+iB) = this%fcentroid(iBFace,3)
            end do

        end do

        ! Processor boundaries, at the end
        do iBoundary=1,numberOfProcBound

            iRank = this%boundaries%procBound(iBoundary)
            is = this%boundaries%startFace(iRank)
            ie = is+this%boundaries%nFace(iRank)-1

            pNeigh = 0
            do iBFace=is,ie
                iB = iB+1
                pNeigh = pNeigh +1
                this%cellCenters(1,numberOfElements+iB) = this%ghost(pNeigh,1,iBoundary)
                this%cellCenters(2,numberOfElements+iB) = this%ghost(pNeigh,2,iBoundary)
                this%cellCenters(3,numberOfElements+iB) = this%ghost(pNeigh,3,iBoundary)
            end do

        end do

        ! Create the tree, ask for internally rearranged data for speed,
        ! and for output sorted by increasing distance from the query vector
        this%meshTree => kdtree2_create(this%cellCenters, rearrange=.true., sort=.true.)

    end subroutine createMeshSearchTree

! *********************************************************************************************************************

    function findPointOwnership(this, point) result(index)

    !==================================================================================================================
    ! Description:
    !! findPointOwnership runs the post processing task for the target line probing object.
    !==================================================================================================================

        class(fvMesh) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: index

        type(kdtree2_result) :: nearestNeighbour(1)
    !------------------------------------------------------------------------------------------------------------------

        real :: point(3)
    !------------------------------------------------------------------------------------------------------------------

        this%cellCenters(:, numberOfElements+numberOfBFaces+1) = point

        ! Check point ownership
        call kdtree2_n_nearest_around_point(this%meshTree, numberOfElements+numberOfBFaces+1, 1, 1, nearestNeighbour)

        if(nearestNeighbour(1)%idx>numberOfElements+sum(this%boundaries%nFace(1:numberOfRealBound))) then
            index = -1
        else
            index = nearestNeighbour(1)%idx
        end if

    end function findPointOwnership

end module flubioMesh
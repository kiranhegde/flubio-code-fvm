!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____ 	        !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|	        !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |     	        !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |     	        !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____ 	        !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|	        !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module baseRansModel

!==================================================================================================================
! Description:
!! baseRansModel is the base class for the turbulence models. EVery RANS model must inherit form this class.
!==================================================================================================================

      use flubioFields
      use math
      use meshvar
      use fieldvar
      use gradient
      use physicalConstants
      use wallDistance
      use velocityTensors

      implicit none

      type, public :: baseModelClass

        integer :: ransModel
        !! Flag for the RANS model

        integer :: productionType
        !! Turbulent production calcualtion method

        integer :: wallFunctionBlending
        !! Wall function blending between viscous and logarithmic layers

        integer :: outputYPlus
        !! Flag to output y+

        character(len=:), allocatable :: modelName
        !! Name for the RANS model

        real, dimension(:), allocatable :: Pk
        !! Turbulent production field field (Moukalled et al, pp.764)

        real, dimension(:), allocatable :: yPlus
        !! Wall distance in wall units

        real, dimension(:,:), allocatable :: utau
        !! Friction velocity

        real, dimension(:,:), allocatable :: wallYplus
        !! Minimum wall distance from a wall boundary

        real, dimension(:,:), allocatable :: nuWall
        !! Effective wall viscosity

        real, dimension(:,:), allocatable :: tauWall
        !! Wall shear stress

        type(flubioField) :: nut
        !! Eddy viscosity

        type(flubioWallDist) :: wd
        !! Wall distance

        type(flubioField) :: tkeField
        !! tke field to host model tke

        type(flubioField) :: tdrField
        !! tdr field to host model tdr

        real :: Cmu
        !! Common costant found in turbulence models

        real :: kappa
        !! Universal constant

        real :: E
        !! Boundary layer E constant

        real :: B
        !! Boundary layer shift constant

        real :: yplusLaminar
        !! Value of the laminar y+

        real :: tke_amb
        !! Free strem tke

        real ::  tdr_amb
        !! Free stream tdr

      contains

          procedure :: createModel => createBaseModel
          procedure :: setModelName
          procedure :: createCommonQuantities
          procedure :: modelCoeffs => baseModelCoeffs

          procedure :: updateEddyViscosity => updateBaseEddyViscosity
          procedure :: nutWallFunction => doNothing

          procedure :: tkeDiffusivity => doNothing
          procedure :: tdrDiffusivity => doNothing

          procedure :: computeFrictionVelocityU
          procedure :: computeFrictionVelocityK => doNothing
          procedure :: computeFrictionVelocity
          procedure :: turbulentProduction => doNothing

          procedure :: laminarYplus

          procedure :: tkeSourceTerm => doNothing
          procedure :: tdrSourceTerm => doNothing

          procedure :: turbulenceModel => doNothing

          procedure :: printresidual => doNothing
          procedure :: writeToFile => doNothing
          procedure :: verbose => doNothing

          procedure :: writeWallYPlus

      end type baseModelClass

contains

    subroutine createBaseModel(this)

    !==================================================================================================================
    ! Description:
    !! createBaseModel is the class constructor.
    !==================================================================================================================

        class(baseModelClass) :: this
     !------------------------------------------------------------------------------------------------------------------

        ! DNS Model does nothing

    end  subroutine createBaseModel

! ******************************************************************************************************************************

    subroutine setModelName(this)

    !==================================================================================================================
    ! Description:
    !! setModelName sets the name fo the turbulence model.
    !==================================================================================================================

        class(baseModelClass) :: this
    !------------------------------------------------------------------------------------------------------------------

        this%modelName = flubioTurbModel%modelName
        this%ransModel = flubioTurbModel%ransModel
        this%productionType = flubioTurbModel%productionType
        this%wallFunctionBlending = flubioTurbModel%wallFunctionBlending
        this%outputYPlus = flubioTurbModel%outputYPlus

        ! Ambient turbulence if any
        if(flubioTurbModel%sustTerm) then
            this%tke_amb = flubioTurbModel%tke_amb
            this%tdr_amb = flubioTurbModel%tdr_amb
        else
            this%tke_amb = 0.0
            this%tdr_amb = 0.0
        end if

    end subroutine setModelName

! *********************************************************************************************************************

    subroutine createCommonQuantities(this)

    !==================================================================================================================
    ! Description:
    !! createCommonQuantities creates common fields used by all turbulence models.
    !==================================================================================================================

        class(baseModelClass) :: this
    !------------------------------------------------------------------------------------------------------------------

        this%ransModel = flubioTurbModel%ransModel

        call this%nut%createField(fieldName='nut', classType='scalar', nComp=1)
        call this%nut%setBcFlagsNeumann0()
        call this%nut%updateBoundaryField()
        call this%nut%updateGhosts()

        ! Compute Wall Distance
        call this%wd%computeWallDistance()

        ! Common quantities
        allocate(this%Pk(numberOfElements+numberOfBFaces))
        allocate(this%yplus(numberOfElements+numberOfBFaces))
        allocate(this%utau(mesh%boundaries%maxWallFaces,numberOfWallBound))
        allocate(this%nuWall(mesh%boundaries%maxWallFaces,numberOfWallBound))
        allocate(this%tauWall(mesh%boundaries%maxWallFaces,numberOfWallBound))
        allocate(this%wallYplus(mesh%boundaries%maxWallFaces,numberOfWallBound))

        this%Pk = 0.0
        this%nuWall = viscos
        this%tauWall = 0.0
        this%utau = 0.0
        this%wallYplus = 0.0
        this%yplus = 0.0 ! non-zero at the wall cells only

    end subroutine createCommonQuantities

! *********************************************************************************************************************

    subroutine baseModelCoeffs(this)

    !==================================================================================================================
    ! Description:
    !! baseModelCoeffs sets the values of base constants, common to all the turbulence models.
    !==================================================================================================================

        class(baseModelClass) :: this
    !------------------------------------------------------------------------------------------------------------------
           
        this%Cmu = 0.09
        this%kappa = 0.41
        this%E = 9.80
        this%B = 5.25

        call this%laminarYPlus()

    end subroutine baseModelCoeffs

! *********************************************************************************************************************

    subroutine updateBaseEddyViscosity(this)

    !==================================================================================================================
    ! Description:
    !! updateBaseEddyViscosity updates the eddy visocsity at boundaries and ghost cells,
    !==================================================================================================================

        class(baseModelClass) :: this
    !------------------------------------------------------------------------------------------------------------------

        this%nut%phi(:,1) = viscos

        ! Update nu and nut at boundaries
        call this%nut%updateBoundaryField()
        call this%nut%updateGhosts()

        call nu%updateBoundaryField()
        call nu%updateGhosts()

    end  subroutine updateBaseEddyViscosity

! *********************************************************************************************************************

    subroutine computeFrictionVelocityU(this)

    !==================================================================================================================
    ! Description:
    !! computeFrictionVelocityU computes the friction velocity from the velocity wall function.
    !==================================================================================================================

        class(baseModelClass) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iBoundary, iBFace, iElement, is, ie, iOwner, pFace

        integer :: it, itmax, iWall
    !------------------------------------------------------------------------------------------------------------------

        real :: kappa, B, E, wallYplusLim

        real :: f, fprime, d, ypl, ut, res, tol, dot, SMALL

        real :: Up(3), nf(3), deltaU(3), magUp
    !------------------------------------------------------------------------------------------------------------------

        ! Prepare
        kappa = this%kappa
        E = this%E
        wallYplusLim = this%yplusLaminar

        itmax = 1000
        tol = 0.001d0
        SMALL = 1e-10

        ! Loop over wall boundaries
        do iBoundary=1,numberOfWallBound

            iWall = 0
            i = mesh%boundaries%wallBound(iBoundary)

            is = mesh%boundaries%startFace(i)
            ie = is+mesh%boundaries%nFace(i)-1

            pFace = numberOfElements + (mesh%boundaries%startFace(i) - numberOfIntFaces) -1

            do iBFace=is,ie

                iWall = iWall+1
                iOwner = mesh%owner(iBFace)
                pFace = pFace + 1

                ! Velocity parallel to the wall
                nf = mesh%Sf(iBFace,:)/mesh%area(iBFace)
                deltaU = velocity%phi(iOwner,:) - velocity%phi(pFace,:)
                dot = dot_product(deltaU,nf)

                ! Centroid velocity - wall velocity parallel to the wall
                Up = deltaU-dot*nf
                magUp = sqrt(Up(1)**2+Up(2)**2+Up(3)**2)

                !------------!
                ! Find yplus !
                !------------!

                it = 0
                res = 100000000.0d0
                d = mesh%area(iBFace)/mesh%wallDist(iBFace)

                do while(res>=tol)

                    it = it+1

                    if(it==1) then
                        ypl = wallYplusLim
                    else
                        ypl = this%wallYplus(iWall,iBoundary)
                    endif

                    f = ypl + kappa*magUp*d/viscos
                    fprime = 1.0+log(E*ypl+SMALL)

                    this%wallYplus(iWall,iBoundary) = f/fprime
                    this%utau(iWall,iBoundary) = viscos*(this%wallYplus(iWall,iBoundary)/d)
                    res = abs(this%wallYplus(iWall,iBoundary) - ypl)/abs(ypl)

                enddo

                !--------------------!
                ! Modified viscosity !
                !--------------------!

                ut = this%utau(iWall,iBoundary)
                ypl = this%wallYplus(iWall,iBoundary)
                this%yplus(iOwner) = ypl

                ! Total viscosity (nu+nut); nut=viscos*(nuWall-1)
                if(ypl > wallYplusLim) then
                  this%nuWall(iWall,iBoundary) = kappa*ypl*viscos/log(E*ypl)
                else
                  this%nuWall(iWall,iBoundary) = viscos
                end if

                this%tauWall(iWall,iBoundary) = this%nuWall(iWall,iBoundary)*magUp/d

            enddo

        enddo

    end subroutine computeFrictionVelocityU

! *********************************************************************************************************************

    subroutine computeFrictionVelocity(this)

    !==================================================================================================================
    ! Description:
    !! computeFrictionVelocity computes the friction velocity using the selected method
    !==================================================================================================================

        class(baseModelClass) :: this
    !------------------------------------------------------------------------------------------------------------------

        if(flubioTurbModel%frictionVelocityMethod==0) then
            call this%computeFrictionVelocityU()
        elseif(flubioTurbModel%frictionVelocityMethod==1) then
            call this%computeFrictionVelocityK()
        else
            call this%computeFrictionVelocityU()
        end if

    end subroutine computeFrictionVelocity

! *********************************************************************************************************************

    subroutine productionLimiter(P, limiter)

    !==================================================================================================================
    ! Description:
    !! productionLimiter limits the turbulent production. This procedure is used in some version of k-omega model.
    !==================================================================================================================

        implicit none

        real :: P, limiter
    !------------------------------------------------------------------------------------------------------------------

        if(P>limiter) P=limiter

    end subroutine productionLimiter

! *********************************************************************************************************************

    subroutine laminarYPlus(this)

    !==================================================================================================================
    ! Description:
    !! laminarYPlus computes the treshold for the laminar yPlus given the values of E and kappa.
    !==================================================================================================================

        class(baseModelClass) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: i
    !------------------------------------------------------------------------------------------------------------------

        real :: ypl
    !------------------------------------------------------------------------------------------------------------------

        ypl = 11.0

        do i=1,10
            ypl = log(max(this%E*ypl, 1.0))/this%kappa
        end do

        this%yplusLaminar = ypl

    end subroutine laminarYPlus

! *********************************************************************************************************************

    function blend(v, l, opt, yplus, threshold, n) result(blendedValue)

    !==================================================================================================================
    ! Description:
    !! blend uses a blending method to compute the a target value.
    !==================================================================================================================

        integer :: opt
    !------------------------------------------------------------------------------------------------------------------

        real v, l, blendedValue, gamma, yPlus

        real :: threshold, n
    !------------------------------------------------------------------------------------------------------------------

        if(opt == 0) then
            blendedValue = sqrt(v**2+l**2)
        elseif(opt == 1) then
            if(yPlus > threshold) then
                blendedValue = l
            else
                blendedValue = v
            end if
        elseif(opt == 2) then
            blendedValue = max(v, l)
        elseif(opt == 3) then
            blendedValue = (v**n + l**n)**(1/n)
        elseif(opt == 4) then
            gamma = 0.01*yPlus**4/(1.0+5*yPlus)
            blendedValue = v*exp(-gamma) + l*exp(-1.0/gamma)
        elseif(opt == 5) then
            gamma = tanh((yPlus/10.0)**2)
            blendedValue = (v+l)*gamma + (1.0-gamma)*(v**1.2 + l*1.2)**(1.0/1.2)
        else
            blendedValue = sqrt(v**2+l**2)
        end if

    end function blend

! *********************************************************************************************************************

    subroutine writeWallYPlus(this)

        class(baseModelClass) :: this

        type(flubioField) :: wallYPlus
    !------------------------------------------------------------------------------------------------------------------

        call wallYPlus%createWorkField(fieldName='wallYPlus', classType='scalar', ncomp=1)

        call wallYPlus%setBcFlagsNeumann0()

        wallYPlus%phi(:,1) = this%yplus

        call wallYPlus%updateBoundaryField()

        call wallYPlus%writeToFile()

    end subroutine writeWallYPlus

! *********************************************************************************************************************

    subroutine doNothing(this)

        class(baseModelClass) :: this
    !------------------------------------------------------------------------------------------------------------------

        call flubioStopMsg('FLUBIO: Something went wrong. The turbulence model has not been selected correctly!')

    end subroutine doNothing

! *********************************************************************************************************************

end module baseRansModel





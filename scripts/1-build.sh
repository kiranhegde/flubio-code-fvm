# 1-build.sh

# define env vars: home_dir_flubio and home_dir_thirdparty
source ../flubio_env.sh

export njobs=4

${home_dir_flubio}/scripts/bash/build-renumber.sh
${home_dir_flubio}/scripts/bash/build-parallelMeshConversion.sh

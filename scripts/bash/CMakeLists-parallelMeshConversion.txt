
cmake_minimum_required( VERSION 3.15 )

project( foam2flubio )

include( $ENV{home_dir_flubio}/cmake/configure/3-compilers.cmake )

include( $ENV{home_dir_flubio}/cmake/thirdparty/configure_mpi )

set(scripts_dir $ENV{home_dir_flubio}/scripts/parallelMeshConversion)

set( CMAKE_Fortran_MODULE_DIRECTORY ${CMAKE_BINARY_DIR}/mod )

set( src
     ${scripts_dir}/precmod.f03
     ${scripts_dir}/stringmod.f03
     ${scripts_dir}/mstrings.f03
     ${scripts_dir}/userDefinedTypes.f03
     ${scripts_dir}/converters.f03
     ${scripts_dir}/fileHandling.f03
     ${scripts_dir}/foam2Flubio.f03
)

# Executable
set( app "foam2Flubio" )

message(STATUS ${CMAKE_Fortran_MODULE_DIRECTORY} )

set_source_files_properties( ${src} PROPERTIES LANGUAGE Fortran )

add_executable( ${app} ${src} )

target_include_directories( ${app} PRIVATE
  ${inc_dir_mpi}
)

target_link_libraries( ${app} PRIVATE
  ${lib_dir_mpi}
)

target_link_libraries( ${app} PRIVATE
  ${lib_names_mpi}
)




set_target_properties( ${app} PROPERTIES COMPILE_FLAGS ${CMAKE_Fortran_FLAGS_RELEASE} )

src_dir: ./src/
output_dir: ./doc
project: FLUBIO
project_github: https://gitlab.com/alie89/flubio-code-fvm
project_website: https://gitlab.com/alie89/flubio-code-fvm
summary: FLUBIO - An unstructured, parallel, finite volume Navier-Stokes solver for teaching and research purposes
author: Edoardo Alinovi and Joel Guerrero
author_description: 
github: https:https://gitlab.com/alie89/
email: edoardo.alinovi@gmail.com 
fpp_extensions: fpp
extensions: f03
exclude_dir: src/IO/qcontainers_f/
exclude_dir: src/utilities/kdtree/ 
exclude_dir: src/FortranExpressionEvaluator/
exclude_dir: src/pre_post_proc/
predocmark: >
media_dir: ./media
docmark_alt: #
predocmark_alt: <
display: public
         protected
         private
source: false
graph: true
search: true
macro: TEST
       LOGIC=.true.
extra_mods: json_module: http://jacobwilliams.github.io/json-Fortran/
            futility: http://cmacmackin.github.io
license: by-nc
extra_filetypes: sh #


Hello and welcome to FLUBIO!  

FLUBIO is a CFD solver targeted at students, academics, personal users and practitioners to help them understand the general theory behind modern CFD solution methods and discretization techniques. The solver is general and capable enough so it can deal with simple problems involving the solution of the convection-diffusion equation, or the solution of complex academic and industrial problems involving turbulence modeling. The solver can be run in serial and parallel environments, and thanks to the use of the PETSc library, FLUBIO is suitable for large problems and potentially exascale computing.  

FLUBIO is written using plain Fortran 2003 standard, with object orientation and code re-usability in mind, making it easier to understand and modify. The source code is commented extensively, so users can know the scope of the functions, classes, and derived types without the need of translating the source code into actionable information. %In addition, easy to browse documentation is automatically generated using Ford.

FLUBIO addresses many of the frustrations and difficulties found by the authors when using legacy solvers, and solvers lacking documentation. We aim with this contribution to address the need of many students and researchers to have a code easy to understand and to modify. A solver to use to test hypotheses but still able to deal with non-trivial geometries and complex flow physics.  %From the point of view of an instructor, we aim at using this solver for teaching purposes and as a small numerical playground for students where they can explore new concepts, and contribute to the continuous development of FLUBIO.

We have made a great effort into translating the standard FVM and CFD theory found in the literature \citep{moukalled,ferzinger,blazek,bernard,malalasekera,hirsch,leveque,tucker,laney,patankar,sandip,wilcox}, into an easy to read computer code, with no tricks, which maintains a close link to the fundamentals of CFD and the FVM. 

From a future perspective, plenty of exciting new developments are in the pipeline and we are happy to accept contribution from the community interested in this new framework.

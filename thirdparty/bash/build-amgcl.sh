
# build-amgcl

unzip tarballs/amgcl.zip

mv amgcl 0-amgcl

#cp bash/CMakeLists.txt-amgcl 0-amgcl/CMakeLists.txt

cd 0-amgcl

rm -rf 0--build && mkdir 0--build && cd 0--build

cmake  .. \
 -DCMAKE_INSTALL_PREFIX=${thirdparty_install_dir}/amgcl \
 -DCMAKE_BUILD_TYPE=Release \

make -j${njobs}
make install

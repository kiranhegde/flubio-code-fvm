
# build-kahip

tar -xzf tarballs/KaHIP-3.10.tar.gz

mv KaHIP-3.10 0-kahip

cp bash/CMakeLists.txt-kahip 0-kahip/CMakeLists.txt
cp bash/1-build.sh-kahip     0-kahip/1-build.sh

cd 0-kahip

mkdir build
cd build

cmake ../ -DCMAKE_BUILD_TYPE=Release \
	  -DCMAKE_INSTALL_PREFIX=${thirdparty_install_dir}/kahip \
          -DCMAKE_BINARY_DIR=${thirdparty_install_dir}/kahip/bin
make -j $njobs
make install
cd ..

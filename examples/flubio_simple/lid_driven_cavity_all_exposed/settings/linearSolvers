!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

{

!-------------------!
! Momentum Equation !
!-------------------!

"Ueqn":{

	"solver": "bcgs",

	"preconditioner": "bjacobi",

	"absTol": 0.000001,

	"relTol": 0.0,

    !"options":{
         !"save_linear_system": "yes", 
         !"viewer_type": "ascii", 
         !"save_linear_system_every": 5, ! save every N iterations 
         !"save_linear_system_ncorr_every": 2 ! within an iteration, it saves every N non-orthogonal corrections 
	     !"ksp_option1": "option_name1"
	     !"ksp_option2": "option_name2"
    !}

},

!-------------------!
! Pressure Equation !
!-------------------!

"Peqn":{

"solver": "cg",

"preconditioner": "bjacobi",

"absTol": 0.000001,

"relTol": 0.0,

}, 

!-----------------!
! energy Equation !
!-----------------!

"Eeqn":{

"solver": "bcgs",

"preconditioner": "bjacobi",

"absTol": 0.000001,

"relTol": 0.0

},

"MaxIter": 10000,

!-------------------!
! General options   !
!-------------------!

"RecomputePCEvery": 1,

"RenumberMesh": "reversedCuthillMckee"

}

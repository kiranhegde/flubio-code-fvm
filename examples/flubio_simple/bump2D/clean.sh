#! /bin/bash

# Folders 
casedir=${PWD}
griddir=$casedir/grid
postDir=$casedir/postProc
fieldDir=$postDir/fields
monDir=$postDir/monitors
vtkDir=$postDir/VTKfields

#clean all

echo
echo "Cleaning case..."
echo

rm -rf $monDir/*
rm -rf $fieldDir/*
rm -rf $vtkDir/*
rm -rf $griddir/bin/*

touch  $monDir/.gitkeep
touch  $fieldDir/.gitkeep
touch  $vtkDir/.gitkeep
touch  $griddir/bin/.gitkeep 

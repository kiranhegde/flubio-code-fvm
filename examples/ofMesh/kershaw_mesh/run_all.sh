#!/bin/bash

foamCleanTutorials

#Skew block - V1 - Orthogonality problems in checkmesh
blockMesh -dict system/blockMeshDict.0 | tee log.blockMesh

#Skew block - V2 - No problems in checkmesh
#blockMesh -dict system/blockMeshDict.1 | tee log.blockMesh

checkMesh | tee log.checkMesh

#icoFoam | tee log.icoFoam
pisoFoam | tee log.pisoFoam
#pimpleFoam | tee log.pisoFoam

postProcess -func sampleDict -latestTime

gnuplot gnuplot/gnuplot_script 

!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

{

!--------------------!
! Time-Step controls !
!--------------------!

!For steady simualtions this value is arbitrary
!It will do one iterative marching one iteration at a time
"TimeStep": 1,			

"MaxiumTime": 100.0,

!--------------!
! I/O controls !
!--------------!

!"FieldsToKeep": 0,	!Saves only one
"FieldsToKeep": 100,


!"SaveEvery": -1,	!-1 will save all iterations		
"SaveEvery": 1,		!Will save every N iterations	

!To save gradients
!"SaveGradients": ["T", "U"],
"SaveGradients": ["T"],

!To save residuals
!Default bahaviour is to save residuals every iteration
!"SaveResidualsEvery": 2,
!"SaveResiduals": "no",
!"Verbosity": "low",

!--------------!
! Restart from !
!--------------!

"RestartFrom": -1

}

module CoField

    use globalMeshVar
    use globalTimeVar
    use meshvar, only: mesh
    use fieldvar, only: velocity
    use flubioMpi
    use physicalConstants, only: rho, nu
    use json_module
    use runTimeObject

    implicit none

    type, public, extends(fieldObj) :: Co

    contains
        procedure :: run => computeCo
    end type Co

contains
    
! *********************************************************************************************************************

    subroutine computeCo(this)

    !==================================================================================================================
    ! Description:
    !! computeCo computes the CFL number.
    !==================================================================================================================

        class(Co):: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, iFace, iBFace, iBoundary, iOwner, iNeighbour, iProc, is, ie, pNeigh
    !------------------------------------------------------------------------------------------------------------------

        real :: CFL(numberOfElements)
        !! total CFL

        real :: CFLc(numberOfElements)
        !! convective CFL

        real :: CFLv(numberOfElements)
        !! viscous CFL

        real :: Ueqv(numberOfElements)
        !! equivalnt velocity scale
    !------------------------------------------------------------------------------------------------------------------

        real :: LambdaC(3), LambdaV(3), u(3), dS(numberOfElements,3), coef, nuLocal, vol, Cvis
    !------------------------------------------------------------------------------------------------------------------

        if(mod(itime, this%runAt)==0) then

            Cvis = 2.0

            !-----------------------!
            ! Compute average areas !
            !--------------- --------!

            dS = 0.0

            do iFace=1,numberOfIntFaces
                iOwner = mesh%owner(iFace)
                iNeighbour = mesh%neighbour(iFace)
                dS(iOwner,:) = dS(iOwner,:) + 0.5*abs(mesh%Sf(iFace,:))
                dS(iNeighbour,:) = dS(iNeighbour,:) + 0.5*abs(mesh%Sf(iFace,:))
            enddo

            ! Processor boundaries
            do iBoundary=1,numberOfProcBound

                iProc = mesh%boundaries%procBound(iBoundary)
                is = mesh%boundaries%startFace(iProc)
                ie = is+mesh%boundaries%nFace(iProc)-1
                pNeigh=0

                do iBFace=is,ie
                    pNeigh=pNeigh+1
                    iOwner = mesh%owner(iBFace)
                    dS(iOwner,:) = dS(iOwner,:) + 0.5*abs(mesh%Sf(iBFace,:))
                enddo
            enddo

            ! Real boundaries
            do iBoundary=1,mesh%boundaries%numberOfRealBound
                is = mesh%boundaries%startFace(iBoundary)
                ie = is+mesh%boundaries%nFace(iBoundary)-1
                do iBFace=is,ie
                    iOwner = mesh%owner(iBFace)
                    dS(iOwner,:) = dS(iOwner,:) + 0.5*abs(mesh%Sf(iBFace,:))
                enddo
            enddo

            !-------------------!
            ! Compute CFL field !
            !-------------------!

            do iElement=1,numberOfElements

                vol = mesh%volume(iElement)

                !----------------!
                ! Convective CFL !
                !----------------!

                u = abs(velocity%phi(iElement,:))

                LambdaC(1) = u(1)*dS(iElement,1)
                LambdaC(2) = u(2)*dS(iElement,2)
                LambdaC(3) = u(3)*dS(iElement,3)

                CFLc(iElement) = dtime*(LambdaC(1)+LambdaC(2)+LambdaC(3))/vol

                !-------------!
                ! Viscous CFL !
                !-------------!

                coef = 4.0/(3.0*rho%phi(iElement,1))

                nuLocal = nu%phi(iElement,1)/rho%phi(iElement,1)

                LambdaV(1)=(coef*nuLocal*dS(iElement,1)**2)/vol
                LambdaV(2)=(coef*nuLocal*dS(iElement,2)**2)/vol
                LambdaV(3)=(1-bdim)*(coef*nuLocal*dS(iElement,3)**2)/vol

                CFLv(iElement) = Cvis*dtime*(LambdaV(1)+LambdaV(2)+LambdaV(3))/vol

                !---------------------!
                ! Equivalent velocity !
                !---------------------!

                Ueqv(iElement) = (LambdaC(1)+LambdaC(2)+LambdaC(3)) + Cvis*(LambdaV(1)+LambdaV(2)+LambdaV(3))

            enddo

            !-----------!
            ! Total CFL !
            !-----------!

            this%taskField%phi(1:numberOfElements,1) = CFLc(1:numberOfElements) + CFLv(1:numberOfElements)

            ! write to file
            call this%write()

        endif

    end subroutine computeCo

!**********************************************************************************************************************

end module CoField
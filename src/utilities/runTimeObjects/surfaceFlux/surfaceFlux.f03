module surfaceFlux

    use flubioDictionaries, only: flubioMonitors, raiseKeyErrorJSON
    use fieldvar, only: fieldRegistry,getFieldLocationInRegistry
    use interpolation, only: boundaryFlux
    use flubioMpi
    use json_module
    use runTimeObject

    implicit none

    type, public, extends(fieldObj) :: surfaceFluxObj
        character(len=:), dimension(:), allocatable :: fields
        character(len=:), dimension(:), allocatable :: boundaries
        integer :: nFields
        integer :: nBoundaries
        real, dimension(:,:), allocatable :: fieldFlux
    contains
        procedure :: initialise => createSurfaceFluxObj
        procedure :: run => computeFlux
        procedure :: appendValue
    end type surfaceFluxObj

contains
        
subroutine createSurfaceFluxObj(this)

    !==================================================================================================================
    ! Description:
    !! createSurfaceFluxObj is the task constructor.
    !==================================================================================================================
    
        class(surfaceFluxObj) :: this
    !-------------------------------------------------------------------------------------------------------------------
    
        logical :: found, monitorsFound, monitorFound
    !------------------------------------------------------------------------------------------------------------------
    
        type(json_core) :: jcore
    
        type(json_value), pointer :: dictPointer, monitorsPointer, monitorPointer
    !------------------------------------------------------------------------------------------------------------------
    
        integer :: iBoundary

        integer, dimension(:), allocatable :: ilen
    !------------------------------------------------------------------------------------------------------------------

        ! Get the pointer to the dictionary
        call flubioMonitors%json%get(dictPointer)
    
        ! Initialize json factory
        call jcore%initialize()
        call jcore%get(dictPointer, 'Monitors', monitorsPointer, monitorsFound)
    
        ! Get monitor pointer
        call jcore%get_child(monitorsPointer, this%objName, monitorPointer, monitorFound)
    
        call jcore%get(monitorPointer, 'runEvery', this%runAt, found)
        if(.not. found) this%runAt = 1

        call jcore%get(monitorPointer, 'saveEvery', this%saveEvery, found)
        call raiseKeyErrorJSON('saveEvery', found, 'settings/monitors/'//this%objName)
        
        call jcore%get(monitorPointer, 'boundaries', this%boundaries, ilen, found)
        call raiseKeyErrorJSON('boundaries', found, 'settings/monitors/'//this%objName)

        call jcore%get(monitorPointer, 'fields', this%fields, ilen, found)
        call raiseKeyErrorJSON('fields', found, 'settings/monitors/'//this%objName)
    
        this%nFields = size(this%fields)
        this%nBoundaries = size(this%boundaries)
        allocate(this%fieldFlux(this%nFields, this%nBoundaries))    

        ! Open a file to output the flux value
        if(id==0) then
            do iBoundary=1,this%nBoundaries
                open(1,file='postProc/monitors/'//trim(this%objName)//'_'//trim(this%boundaries(iBoundary))//'.dat')
                    write(1,*) '#   Time', this%fields
                close(1)
            enddo
        endif

        ! Clean up
        nullify(dictPointer)
        if(monitorsFound) nullify(monitorsPointer)
        if(monitorFound) nullify(monitorPointer)
        call jcore%destroy()
    
    end subroutine createSurfaceFluxObj
    
! *********************************************************************************************************************

    subroutine computeFlux(this)

    !==================================================================================================================
    ! Description:
    !! computeFlux computes a field flux through a target boundary.
    !==================================================================================================================

        class(surfaceFluxObj):: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iField, fieldIndex, ierr
    !------------------------------------------------------------------------------------------------------------------
    
        real, dimension(:), allocatable :: f

        real :: localFlux, globalFlux
    !------------------------------------------------------------------------------------------------------------------

        if(mod(itime, this%runAt)==0) then

            do iField=1,this%nFields

                fieldIndex = getFieldLocationInRegistry(trim(this%fields(iField)))

                do iBoundary=1,this%nBoundaries
                   
                    f = boundaryFlux(fieldRegistry(fieldIndex)%field, trim(this%boundaries(iBoundary)), 1.0)

                    ! Sum the flux
                    localFlux = sum(f)

                    ! Sum contribution form all the processors
                    call mpi_reduce(localFlux, globalFlux, 1, MPI_REAL8, MPI_SUM, 0, MPI_COMM_WORLD, ierr)
                    this%fieldFlux(iField, iBoundary) = globalFlux

                enddo    

            enddo

            call this%appendValue()

        endif    
        
    end subroutine computeFlux

!**********************************************************************************************************************

    subroutine appendValue(this)

        class(surfaceFluxObj) this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iField
    !------------------------------------------------------------------------------------------------------------------

        if(id==0 .and. (mod(itime,this%saveEvery)==0 .or. abs(this%saveEvery)==1)) then

            do iBoundary=1,this%nBoundaries
                open(1,file='postProc/monitors/'//this%objName//'_'//trim(this%boundaries(iBoundary))//'.dat',access='append') 
                    write(1,'(9999(g0))') time, (this%fieldFlux(iField, iBoundary), " ", iField=1,this%nFields)
                close(1)
            enddo

        endif    

    end subroutine appendValue    

end module surfaceFlux
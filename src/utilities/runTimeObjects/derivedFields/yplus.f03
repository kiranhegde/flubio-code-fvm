module yPlusField

    use globalMeshVar
    use globalTimeVar
    use flubioMpi
    use runTimeObject
    use turbulenceModels, only: trb

    implicit none

    type, public, extends(fieldObj) :: yPlus

    contains
        procedure :: run =>  computeYPlus
    end type yPlus

contains

! *********************************************************************************************************************

    subroutine computeyPlus(this)

    !==================================================================================================================
    ! Description:
    !! computeyPlus save the wall yPlus a field.
    !==================================================================================================================

        class(yPlus):: this
    !------------------------------------------------------------------------------------------------------------------

        if(mod(itime, this%runAt)==0) then
            this%taskField%phi(:,1) = trb%yPlus
            call this%write()
        endif

    end subroutine computeyPlus

!**********************************************************************************************************************

end module yPlusField

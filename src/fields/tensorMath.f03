module tensorMath

    use flubioTensors
    use tensorMultiply, only: operator(**)

    implicit none 

    interface tr
        module procedure tr2
        module procedure tr2s
    end interface tr
    
    interface det
        module procedure det2
        module procedure det2s
    end interface det 
    
    interface dev
        module procedure dev2
        module procedure dev2s
    end interface dev
    
    interface unimodular
        module procedure unimod2
        module procedure unimod2s
        module procedure unimod2d
        module procedure unimod2sd
    end interface unimodular
    
    interface inv
        module procedure inv2
        module procedure inv2s
        module procedure inv_2d
        module procedure inv_2sd
    end interface inv
    
    interface norm
        module procedure norm1
        module procedure norm2d
        module procedure norm2s
    end interface norm
    
    interface exp
        module procedure exp2
        module procedure exp2s
    end interface exp
    
    interface derivativeexp
        module procedure dexp2
        module procedure dexp2s
    end interface derivativeexp
    
    interface ln
        module procedure ln2
        module procedure ln2s
    end interface ln

    interface sqrt
        module procedure sqrt1
        module procedure stretch2
        module procedure stretch2s
    end interface sqrt
    
    interface dsqrt
        module procedure sqrt1
        module procedure stretch2
        module procedure stretch2s
    end interface dsqrt
    
    interface rotationmatrix
        module procedure rotation2
    end interface rotationmatrix

    interface identity2
        module procedure ident2
        module procedure ident2s
    end interface identity2
    
    interface identity4
        module procedure ident4
        module procedure ident4s
    end interface identity4

    interface transpose
        module procedure transp_2
        module procedure transp_2s
        module procedure transp_4
        module procedure transp_4s
    end interface transpose

    interface sym
        module procedure symstore2
        module procedure symstore4
    end interface sym

    interface symm
        module procedure sym2
    end interface symm

    interface skw
        module procedure skew2 
    end interface skw

    interface omega
        module procedure omega2 
    end interface omega

contains 

!**********************************************************************************************************************

    function tr2(T) result(trace)
        
        type(ttbTensor2), intent(IN) :: T
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: trace(T%dim)
    !------------------------------------------------------------------------------------------------------------------   

        trace = 0.0
        do iElement=1,T%dim 
            trace(iElement) = tr(T%T(iElement))
        enddo

    end function tr2

!**********************************************************************************************************************

    function tr2s(T) result(trace)
        
        type(ttbTensor2s), intent(IN) :: T
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: trace(T%dim)
    !------------------------------------------------------------------------------------------------------------------   

        trace = 0.0
        do iElement=1,T%dim 
            trace(iElement) = tr(T%T(iElement))
        enddo

    end function tr2s

!**********************************************************************************************************************

    function det2(T) result(determinant)
        
        type(ttbTensor2), intent(IN) :: T
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: determinant(T%dim)
    !------------------------------------------------------------------------------------------------------------------   

        determinant = 0.0
        do iElement=1,T%dim 
            determinant(iElement) = det(T%T(iElement))
        enddo

    end function det2

!**********************************************************************************************************************

    function det2s(T) result(determinant)
        
        type(ttbTensor2s), intent(IN) :: T
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: determinant(T%dim)
    !------------------------------------------------------------------------------------------------------------------   

        determinant = 0.0
        do iElement=1,T%dim 
            determinant(iElement) = det(T%T(iElement))
        enddo

    end function det2s

!**********************************************************************************************************************

    function dev2(T) result(Tdev)
        
        type(ttbTensor2), intent(IN) :: T

        type(ttbTensor2) :: Tdev
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tdev%create(T%dim)

        do iElement=1,T%dim 
            Tdev%T(iElement) = dev(T%T(iElement))
        enddo

    end function dev2

!**********************************************************************************************************************

    function dev2s(T) result(Tdev)
        
        type(ttbTensor2s), intent(IN) :: T

        type(ttbTensor2s) :: Tdev
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tdev%create(T%dim)

        do iElement=1,T%dim 
            Tdev%T(iElement) = dev(T%T(iElement))
        enddo

    end function dev2s
    
!**********************************************************************************************************************

    function unimod2(T) result(Tuni)
        
        type(ttbTensor2), intent(IN) :: T

        type(ttbTensor2) :: Tuni
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tuni%create(T%dim)

        do iElement=1,T%dim 
            Tuni%T(iElement) = unimodular(T%T(iElement))
        enddo

    end function unimod2

!**********************************************************************************************************************

    function unimod2s(T) result(Tuni)
        
        type(ttbTensor2s), intent(IN) :: T

        type(ttbTensor2s) :: Tuni
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tuni%create(T%dim)

        do iElement=1,T%dim 
            Tuni%T(iElement) = unimodular(T%T(iElement))
        enddo

    end function unimod2s
    
!**********************************************************************************************************************

    function unimod2d(T, detT) result(Tuni)
        
        type(ttbTensor2), intent(IN) :: T

        type(ttbTensor2) :: Tuni
    !------------------------------------------------------------------------------------------------------------------

        real :: detT
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tuni%create(T%dim)

        do iElement=1,T%dim 
            Tuni%T(iElement) = unimodular(T%T(iElement), detT)
        enddo

    end function unimod2d

!**********************************************************************************************************************

    function unimod2sd(T, detT) result(Tuni)
        
        type(ttbTensor2s), intent(IN) :: T

        type(ttbTensor2s) :: Tuni
    !------------------------------------------------------------------------------------------------------------------

        real :: detT
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tuni%create(T%dim)

        do iElement=1,T%dim 
            Tuni%T(iElement) = unimodular(T%T(iElement), detT)
        enddo

    end function unimod2sd
    
!**********************************************************************************************************************

    function inv2(T) result(Tinv)
        
        type(ttbTensor2), intent(IN) :: T

        type(ttbTensor2) :: Tinv
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tinv%create(T%dim)

        do iElement=1,T%dim 
            Tinv%T(iElement) = inv(T%T(iElement))
        enddo

    end function inv2

!**********************************************************************************************************************

    function inv2s(T) result(Tinv)
        
        type(ttbTensor2s), intent(IN) :: T

        type(ttbTensor2s) :: Tinv
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tinv%create(T%dim)

        do iElement=1,T%dim 
            Tinv%T(iElement) = inv(T%T(iElement))
        enddo

    end function inv2s
    
!**********************************************************************************************************************

    function inv_2d(T, detT) result(Tinv)
        
        type(ttbTensor2), intent(IN) :: T

        type(ttbTensor2) :: Tinv
    !------------------------------------------------------------------------------------------------------------------

        real :: detT
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tinv%create(T%dim)

        do iElement=1,T%dim 
            Tinv%T(iElement) = inv(T%T(iElement), detT)
        enddo

    end function inv_2d

!**********************************************************************************************************************

    function inv_2sd(T, detT) result(Tinv)
        
        type(ttbTensor2s), intent(IN) :: T

        type(ttbTensor2s) :: Tinv
    !------------------------------------------------------------------------------------------------------------------

        real :: detT
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tinv%create(T%dim)

        do iElement=1,T%dim 
            Tinv%T(iElement) = inv(T%T(iElement), detT)
        enddo

    end function inv_2sd
    
!**********************************************************************************************************************

    function norm1(T) result(tnorm)
        
        type(ttbTensor1), intent(IN) :: T
    !------------------------------------------------------------------------------------------------------------------

        real :: tnorm(T%dim)
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        tnorm = 0.0
        do iElement=1,T%dim 
            tnorm(iElement) = norm(T%T(iElement))
        enddo

    end function norm1
    
!**********************************************************************************************************************

    function norm2d(T) result(tnorm)
        
        type(ttbTensor2), intent(IN) :: T
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: tnorm(T%dim)
    !------------------------------------------------------------------------------------------------------------------

        do iElement=1,T%dim 
            tnorm(iElement) = norm(T%T(iElement))
        enddo

    end function norm2d
    
!**********************************************************************************************************************

    function norm2s(T) result(tnorm)
        
        type(ttbTensor2s), intent(IN) :: T
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: tnorm(T%dim)
    !------------------------------------------------------------------------------------------------------------------

        do iElement=1,T%dim 
            tnorm(iElement) = norm(T%T(iElement))
        enddo

    end function norm2s
    
!**********************************************************************************************************************

    function exp2(T) result(Texp)
        
        type(ttbTensor2), intent(IN) :: T

        type(ttbTensor2) :: Texp
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Texp%create(T%dim)

        do iElement=1,T%dim 
            Texp%T(iElement) = exp(T%T(iElement))
        enddo

    end function exp2

!**********************************************************************************************************************

    function exp2s(T) result(Texp)
        
        type(ttbTensor2s), intent(IN) :: T

        type(ttbTensor2s) :: Texp
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Texp%create(T%dim)

        do iElement=1,T%dim 
            Texp%T(iElement) = exp(T%T(iElement))
        enddo

    end function exp2s

!**********************************************************************************************************************

    function dexp2(T) result(Texp)
        
        type(ttbTensor2), intent(IN) :: T

        type(ttbTensor4) :: Texp
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Texp%create(T%dim)

        do iElement=1,T%dim 
            Texp%T(iElement) = derivative_exp(T%T(iElement))
        enddo

    end function dexp2

!**********************************************************************************************************************

    function dexp2s(T) result(Texp)
        
        type(ttbTensor2s), intent(IN) :: T

        type(ttbTensor4s) :: Texp
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Texp%create(T%dim)

        do iElement=1,T%dim 
            Texp%T(iElement) = derivative_exp(T%T(iElement))
        enddo

    end function dexp2s

!**********************************************************************************************************************

    function ln2(T) result(Tln)
        
        type(ttbTensor2), intent(IN) :: T

        type(ttbTensor2) :: Tln
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tln%create(T%dim)

        do iElement=1,T%dim 
            Tln%T(iElement) = ln(T%T(iElement))
        enddo

    end function ln2

!**********************************************************************************************************************

    function ln2s(T) result(Tln)
        
        type(ttbTensor2s), intent(IN) :: T

        type(ttbTensor2s) :: Tln
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tln%create(T%dim)

        do iElement=1,T%dim 
            Tln%T(iElement) = ln(T%T(iElement))
        enddo

    end function ln2s

!**********************************************************************************************************************

    function sqrt1(T) result(Tsqrt)
        
        type(ttbTensor1), intent(IN) :: T

        type(ttbTensor1) :: Tsqrt
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tsqrt%create(T%dim)

        do iElement=1,T%dim 
            Tsqrt%T(iElement) = sqrt(T%T(iElement))
        enddo

    end function sqrt1

!**********************************************************************************************************************

    function stretch2(T) result(Tsqrt)
        
        type(ttbTensor2), intent(IN) :: T

        type(ttbTensor2) :: Tsqrt
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tsqrt%create(T%dim)

        do iElement=1,T%dim 
            Tsqrt%T(iElement) = sqrt(T%T(iElement))
        enddo

    end function stretch2

!**********************************************************************************************************************

    function stretch2s(T) result(Tsqrt)
        
        type(ttbTensor2s), intent(IN) :: T

        type(ttbTensor2s) :: Tsqrt
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tsqrt%create(T%dim)

        do iElement=1,T%dim 
            Tsqrt%T(iElement) = sqrt(T%T(iElement))
        enddo

    end function stretch2s

!**********************************************************************************************************************

    function rotation2(angle, i, dim) result(Trot)

        type(ttbTensor2) :: Trot
    !------------------------------------------------------------------------------------------------------------------

        integer, intent(IN) :: i, dim

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real, intent(IN) :: angle
    !------------------------------------------------------------------------------------------------------------------

        call Trot%create(dim)

        do iElement=1,dim 
            Trot%T(iElement) = rotation_matrix(angle, i)
        enddo

    end function rotation2

!**********************************************************************************************************************

    function ident2(T) result(I)
        
        type(ttbTensor2), intent(IN) :: T

        type(ttbTensor2) :: I
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call I%create(T%dim)

        do iElement=1,T%dim 
            I%T(iElement) = identity2(T%T(iElement))
        enddo

    end function ident2

!**********************************************************************************************************************

    function ident2s(T) result(I)
        
        type(ttbTensor2s), intent(IN) :: T

        type(ttbTensor2s) :: I
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call I%create(T%dim)

        do iElement=1,T%dim 
            I%T(iElement) = identity2(T%T(iElement))
        enddo

    end function ident2s

!**********************************************************************************************************************

    function ident4(T) result(I)
        
        type(ttbTensor2), intent(IN) :: T

        type(ttbTensor4) :: I
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call I%create(T%dim)

        do iElement=1,T%dim 
            I%T(iElement) = identity4(T%T(iElement))
        enddo

    end function ident4

!**********************************************************************************************************************

    function ident4s(T) result(I)
        
        type(ttbTensor2s), intent(IN) :: T

        type(ttbTensor4s) :: I
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call I%create(T%dim)

        do iElement=1,T%dim 
            I%T(iElement) = identity4(T%T(iElement))
        enddo

    end function ident4s

!**********************************************************************************************************************

    function transp_2(T) result(Ttrasp)
        
        type(ttbTensor2), intent(IN) :: T

        type(ttbTensor2) :: Ttrasp
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Ttrasp%create(T%dim)

        do iElement=1,T%dim 
            Ttrasp%T(iElement) = transpose(T%T(iElement))
        enddo

    end function transp_2

!**********************************************************************************************************************

    function transp_2s(T) result(Ttrasp)
        
        type(ttbTensor2s), intent(IN) :: T

        type(ttbTensor2s) :: Ttrasp
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Ttrasp%create(T%dim)

        do iElement=1,T%dim 
            Ttrasp%T(iElement) = transpose(T%T(iElement))
        enddo

    end function transp_2s

!**********************************************************************************************************************

    function transp_4(T) result(Ttrasp)
        
        type(ttbTensor4), intent(IN) :: T

        type(ttbTensor4) :: Ttrasp
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Ttrasp%create(T%dim)

        do iElement=1,T%dim 
            Ttrasp%T(iElement) = transpose(T%T(iElement))
        enddo

    end function transp_4

!**********************************************************************************************************************

    function transp_4s(T) result(Ttrasp)
        
        type(ttbTensor4S), intent(IN) :: T

        type(ttbTensor4S) :: Ttrasp
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Ttrasp%create(T%dim)

        do iElement=1,T%dim 
            Ttrasp%T(iElement) = transpose(T%T(iElement))
        enddo

    end function transp_4s

!**********************************************************************************************************************

    function symstore2(T) result(Tsym)
        
        type(ttbTensor2), intent(IN) :: T

        type(ttbTensor2S) :: Tsym
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tsym%create(T%dim)

        do iElement=1,T%dim 
            Tsym%T(iElement) = symstore(T%T(iElement))
        enddo

    end function symstore2

!**********************************************************************************************************************
    
    function symstore4(T) result(Tsym)
        
        type(ttbTensor4), intent(IN) :: T

        type(ttbTensor4S) :: Tsym
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tsym%create(T%dim)

        do iElement=1,T%dim 
            Tsym%T(iElement) = symstore(T%T(iElement))
        enddo

    end function symstore4

!**********************************************************************************************************************

    function sym2(T) result(Tsym)
        
        type(ttbTensor2), intent(IN) :: T

        type(ttbTensor2S) :: Tsym
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tsym%create(T%dim)

        do iElement=1,T%dim 
            Tsym%T(iElement) = 0.5*(T%T(iElement) + transpose(T%T(iElement)))
        enddo

    end function sym2

!**********************************************************************************************************************

    function skew2(T) result(Tskew)
        
        type(ttbTensor2), intent(IN) :: T

        type(ttbTensor2) :: Tskew
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tskew%create(T%dim)

        do iElement=1,T%dim 
            Tskew%T(iElement) = 0.5*(T%T(iElement) - transpose(T%T(iElement)))
        enddo

    end function skew2

!**********************************************************************************************************************

    function omega2(T) result(Tskew)
        
        type(ttbTensor2), intent(IN) :: T

        type(ttbTensor2) :: Tskew
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tskew%create(T%dim)

        do iElement=1,T%dim 
            Tskew%T(iElement) = 0.5*(transpose(T%T(iElement)) - T%T(iElement))
        enddo

    end function omega2

!**********************************************************************************************************************

!**********************************************************************************************************************
!               NOTE: Original TTB libraries has a bunch more interfaces, add them here if you need them
!**********************************************************************************************************************
    
end module tensorMath
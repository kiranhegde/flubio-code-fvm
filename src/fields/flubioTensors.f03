!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module flubioTensors

!==================================================================================================================
! Description: flubioTensors implements the data structures and the method operating on general "tensors" defined in FLUBIO.
!! Tensors are two dimensional array where the second components has 6 or 9 componentes is dependence of the tensor property.
!! The tensor componets are allocated this way for each i:
!! \begin{eqnarray*}
!!   flubioT(i,1) = T(1,1),  \\
!!   flubioT(i,2) = T(2,2),  \\
!!   flubioT(i,3) = T(3,3),  \\
!!   flubioT(i,4) = T(1,2),  \\
!!   flubioT(i,5) = T(1,3),  \\
!!   flubioT(i,6) = T(2,3),  \\
!!   flubioT(i,7) = T(2,1),  \\
!!   flubioT(i,8) = T(3,1),  \\
!!   flubioT(i,9) = T(3,2),  \\
!! \end{eqnarray*}
!! Symmtric tensors has just six components.
!==================================================================================================================

    use tensor 

    implicit none

    ! Full storage (numberOfElements, 9)
    type, public :: flubioTensor

        integer :: tSize
        !! tensor size

        integer :: nComp
        !! number of components

        real, dimension(:,:), allocatable :: cmpt
        !! components of the tensor

        real, dimension(:), allocatable :: mag
        !! values of the tensor

        contains

            procedure :: createTensor
            procedure :: destroyTensor
            procedure :: setFromMatrix
            procedure :: setFromVector
            procedure :: magnitude

    end type flubioTensor

    ! TTB tensor wrappers

    ! Rank 1 tensor
    type, public :: ttbTensor1

        integer :: dim 

        type(Tensor1), dimension(:), allocatable :: T

        contains 
            procedure :: create => createTensor1
            procedure :: destroy => destroyTensor1

    end type ttbTensor1

    ! Rank 2 tensor
    type, public :: ttbTensor2

        integer :: dim 

        type(Tensor2), dimension(:), allocatable :: T

        contains
            procedure :: create => createTensor2
            procedure :: destroy => destroyTensor2
    end type ttbTensor2

    ! Rank 4 tensor
    type, public :: ttbTensor4

        integer :: dim 

        type(Tensor4), dimension(:), allocatable :: T

        contains 
            procedure :: create => createTensor4
            procedure :: destroy => destroyTensor4

    end type ttbTensor4

    ! Rank 2 symmetric tensor
    type, public :: ttbTensor2S

        integer :: dim 

        type(Tensor2S), dimension(:), allocatable :: T

        contains 
            procedure :: create => createTensor2S
            procedure :: destroy => destroyTensor2S

    end type ttbTensor2S

    ! Rank 4 symmetric tensor
    type, public :: ttbTensor4S

        integer :: dim 

        type(Tensor4S), dimension(:), allocatable :: T

        contains
            procedure :: create => createTensor4S
            procedure :: destroy => destroyTensor4S
    end type ttbTensor4s

contains

! ********************************************************************************************************************!
!                                               TTB TENSORS                                                           !
! ********************************************************************************************************************!

    subroutine createTensor1(this, dim, phi)

        class(ttbTensor1) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: dim
        !! Tensor Field dimension

        integer :: iElement 
        !! loop index
    !------------------------------------------------------------------------------------------------------------------

        real, optional :: phi(dim,3)

        real :: assignedValue(dim,3)
    !------------------------------------------------------------------------------------------------------------------

        this%dim = dim

        if(present(phi)) then 
            assignedValue = phi
        else 
            assignedValue = 0.0
        endif

        allocate(this%T(dim))

        do iElement=1,dim 
            this%T(iElement)%a = assignedValue(iElement,1:3)
        enddo 

    end subroutine createTensor1

! ********************************************************************************************************************!

    subroutine destroyTensor1(this)

        class(ttbTensor1) :: this
    !------------------------------------------------------------------------------------------------------------------

        deallocate(this%T)

    end subroutine destroyTensor1

!**********************************************************************************************************************

    subroutine createTensor2(this, dim, phi)

        class(ttbTensor2) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: dim
        !! Tensor Field dimension

        integer :: iElement 
        !! loop index
    !------------------------------------------------------------------------------------------------------------------

        real, optional :: phi(dim, 3, 3)

        real :: assignedValue(dim, 3, 3)
    !------------------------------------------------------------------------------------------------------------------

        this%dim = dim
    
        if(present(phi)) then 
            assignedValue = phi
        else 
            assignedValue = 0.0
        endif

        allocate(this%T(dim))

        do iElement=1,dim 
            this%T(iElement)%ab = assignedValue(iElement, 1:3, 1:3)
        enddo 

    end subroutine createTensor2

! ********************************************************************************************************************!

    subroutine destroyTensor2(this)

        class(ttbTensor2) :: this
    !------------------------------------------------------------------------------------------------------------------

        deallocate(this%T)

    end subroutine destroyTensor2

!**********************************************************************************************************************

    subroutine createTensor4(this, dim, phi)

        class(ttbTensor4) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: dim
        !! Tensor Field dimension

        integer :: iElement 
        !! loop index
    !------------------------------------------------------------------------------------------------------------------

        real, optional :: phi(dim, 3, 3, 3, 3)

        real :: assignedValue(dim, 3, 3, 3, 3)
    !------------------------------------------------------------------------------------------------------------------

        this%dim = dim

        if(present(phi)) then 
            assignedValue = phi
        else 
            assignedValue = 0.0
        endif

        allocate(this%T(dim))

        do iElement=1,dim 
            this%T(iElement)%abcd = assignedValue(iElement, 1:3, 1:3, 1:3, 1:3)
        enddo 

    end subroutine createTensor4

!**********************************************************************************************************************

    subroutine destroyTensor4(this)

        class(ttbTensor4) :: this
    !------------------------------------------------------------------------------------------------------------------

        deallocate(this%T)

    end subroutine destroyTensor4

!**********************************************************************************************************************

    subroutine createTensor2S(this, dim, phi)

        class(ttbTensor2S) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: dim
        !! Tensor Field dimension

        integer :: iElement 
        !! loop index
    !------------------------------------------------------------------------------------------------------------------

        real, optional :: phi(dim, 6)

        real :: assignedValue(dim, 6)
    !------------------------------------------------------------------------------------------------------------------

        this%dim = dim

        if(present(phi)) then 
            assignedValue = phi
        else 
            assignedValue = 0.0
        endif

        allocate(this%T(dim))

        do iElement=1,dim 
            this%T(iElement)%a6 = assignedValue(iElement, 1:6)
        enddo 

    end subroutine createTensor2S

!**********************************************************************************************************************

    subroutine destroyTensor2S(this)

        class(ttbTensor2S) :: this
    !------------------------------------------------------------------------------------------------------------------

        deallocate(this%T)

    end subroutine destroyTensor2S

!**********************************************************************************************************************

    subroutine createTensor4S(this, dim, phi)

        class(ttbTensor4S) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: dim
        !! Tensor Field dimension

        integer :: iElement 
        !! loop index
    !------------------------------------------------------------------------------------------------------------------

        real, optional :: phi(dim, 6, 6)

        real :: assignedValue(dim, 6, 6)
    !------------------------------------------------------------------------------------------------------------------

        this%dim = dim

        if(present(phi)) then 
            assignedValue = phi
        else 
            assignedValue = 0.0
        endif

        allocate(this%T(dim))

        do iElement=1,dim 
            this%T(iElement)%a6b6 = assignedValue(iElement, 1:6, 1:6)
        enddo 

    end subroutine createTensor4S

!**********************************************************************************************************************

    subroutine destroyTensor4S(this)

        class(ttbTensor4S) :: this
    !------------------------------------------------------------------------------------------------------------------

        deallocate(this%T)

    end subroutine destroyTensor4S

!**********************************************************************************************************************

! ********************************************************************************************************************!
!                                               FLUBIO TENSORS                                                        !
! ********************************************************************************************************************!

    subroutine createTensor(this, tSize, nComp)

    !===================================================================================================================
    ! Description:
    !! createTensor is the constructor for standard tensor.
    !===================================================================================================================

        class(flubioTensor) :: this
    !-------------------------------------------------------------------------------------------------------------------

        integer :: tSize
        !! tensor size

        integer :: nComp
        !! number of components of the tensor (6 = symmetric, 9 = asymmetric)
    !-------------------------------------------------------------------------------------------------------------------

        this%tSize = tSize
        this%nComp = nComp

        allocate(this%cmpt(tsize,nComp))
        allocate(this%mag(tsize))
        this%cmpt = 0.0
        this%mag = 0.0

    end subroutine createTensor

! **********************************************************************************************************************

    subroutine destroyTensor(this)

    !===================================================================================================================
    ! Description:
    !! destroyTensor is the deconstructor of a standard tensor.
    !===================================================================================================================

        class(flubioTensor) :: this
    !-------------------------------------------------------------------------------------------------------------------

        deallocate(this%cmpt)
        deallocate(this%mag)

    end subroutine destroyTensor

! **********************************************************************************************************************

    subroutine setFromMatrix(this, T)

    !===================================================================================================================
    ! Description:
    !!  setFromMatrix assigns the tensors values form  a given input matrix.
    !===================================================================================================================

        class(flubioTensor) :: this
    !-------------------------------------------------------------------------------------------------------------------

        integer :: i
        !! loop index to fill the tensor

    !-------------------------------------------------------------------------------------------------------------------

        real :: T(3, 3)
        !! input tensor to be assigned
    !-------------------------------------------------------------------------------------------------------------------

        do i=1,3
    !       this%cmpt(i,:) = T(i,:)
        end do

    end subroutine setFromMatrix

! **********************************************************************************************************************

    subroutine setFromVector(this, T)

    !===================================================================================================================
    ! Description:
    !! setFromVector assigns the tensors values form a given input vector.
    !===================================================================================================================

        class(flubioTensor) :: this
    !-------------------------------------------------------------------------------------------------------------------

        integer :: i
        !! loop index to fill the tensor
    !-------------------------------------------------------------------------------------------------------------------

        real :: T(this%tSize, this%nComp)
        !! input tensor to be assigned
    !-------------------------------------------------------------------------------------------------------------------

        do i=1,this%tSize
            this%cmpt(i,:) = T(i,:)
        end do

    end subroutine setFromVector

! *********************************************************************************************************************

    subroutine magnitude(this)

    !==================================================================================================================
    ! Description:
    !! set assigns the tensors values form a given input.
    !==================================================================================================================

        class(flubioTensor) :: this
    !-------------------------------------------------------------------------------------------------------------------

        ! magnitude is the inner product of the tensor with itself, multplied by sqrt(2)
        this%mag = sqrt(2.0*innerProduct(this, this))

    end subroutine magnitude

! **********************************************************************************************************************
!                                         Functions operating on tensors
! **********************************************************************************************************************

    function innerProduct(A, B) result(P)

    !===================================================================================================================
    ! Description:
    !! innerProductAsym computes the inner product of two tensors as:
    !! \begin{equation*}  P = T^1_{ij}T^2_{ij} \end{equation*}
    !===================================================================================================================

        type(flubioTensor) :: A
        !! First Tensor

        type(flubioTensor) :: B
        !! second Tensor
    !-------------------------------------------------------------------------------------------------------------------

        integer :: i
    !-------------------------------------------------------------------------------------------------------------------

        real :: P(A%tSize)
        !! Inner prodcut between two tensors

        real :: c1, c2, c3
    !-------------------------------------------------------------------------------------------------------------------

        P = 0.0

        if(A%nComp==6) then
            do i=1,A%tSize
                c1 = A%cmpt(i,1)*B%cmpt(i,1) + A%cmpt(i,4)*B%cmpt(i,4) + A%cmpt(i,5)*B%cmpt(i,5)
                c2 = A%cmpt(i,4)*B%cmpt(i,4) + A%cmpt(i,2)*B%cmpt(i,2) + A%cmpt(i,6)*B%cmpt(i,6)
                c3 = A%cmpt(i,5)*B%cmpt(i,5) + A%cmpt(i,6)*B%cmpt(i,6) + A%cmpt(i,3)*B%cmpt(i,3)
                P(i) = c1 + c2 + c3
            end do
        else
            do i=1,A%tSize
                c1 = A%cmpt(i,1)*B%cmpt(i,1) + A%cmpt(i,4)*B%cmpt(i,4) + A%cmpt(i,5)*B%cmpt(i,5)
                c2 = A%cmpt(i,7)*B%cmpt(i,7) + A%cmpt(i,2)*B%cmpt(i,2) + A%cmpt(i,6)*B%cmpt(i,6)
                c3 = A%cmpt(i,8)*B%cmpt(i,8) + A%cmpt(i,9)*B%cmpt(i,9) + A%cmpt(i,3)*B%cmpt(i,3)
                P(i) = c1 + c2 + c3
            end do
        end if

    end function innerProduct

! **********************************************************************************************************************

    function symmetric(A) result(S)

    !===================================================================================================================
    ! Description:
    !! symmetric extracts the symmetric part of a tensor as:
    !! \begin{equation*}  S = \frac{1}{2}(A_{ij} + A_{ji}) = \frac{1}{2}(T + T^{T}) \end{equation*}
    !! Attention: the assigned tensor must be symmtryc, in the sens that it must have (*,6) components!
    !===================================================================================================================

        type(flubioTensor) :: A
        !! First Tensor

        type(flubioTensor) :: S
        !! symmetric Tensor
    !-------------------------------------------------------------------------------------------------------------------

        integer :: i
    !-------------------------------------------------------------------------------------------------------------------

        call S%createTensor(A%tSize,6)

        do i=1,A%tSize
            S%cmpt(i,1) = A%cmpt(i,1) ! dudx
            S%cmpt(i,2) = A%cmpt(i,2) ! dvdy
            S%cmpt(i,3) = A%cmpt(i,3) ! dwdz

            ! Off-diagonal
            S%cmpt(i,4) = 0.5*(A%cmpt(i,4)+A%cmpt(i,7))
            S%cmpt(i,5) = 0.5*(A%cmpt(i,5)+A%cmpt(i,8))
            S%cmpt(i,6) = 0.5*(A%cmpt(i,6)+A%cmpt(i,9))
        end do

    end function symmetric

! **********************************************************************************************************************

    function antiSymmtric(A) result(S)

    !===================================================================================================================
    ! Description:
    !! antiSymmtric returns the asymmetric part of a tensor as:
    !! \begin{equation*}  S = \frac{1}{2}(A_{ij} - A_{ji}) = \frac{1}{2}(T - T^{T})\end{equation*}
    !===================================================================================================================

        type(flubioTensor) :: A
        !! First Tensor

        type(flubioTensor) :: S
        !! symmetric Tensor
    !-------------------------------------------------------------------------------------------------------------------

        integer :: i
    !-------------------------------------------------------------------------------------------------------------------

        call S%createTensor(S%tSize,9)

        do i=1,A%tSize

            ! Off-diagonal
            S%cmpt(i,1) = 0.0
            S%cmpt(i,2) = 0.0
            S%cmpt(i,3) = 0.0
            S%cmpt(i,4) = 0.5*(A%cmpt(i,4)-A%cmpt(i,7))
            S%cmpt(i,5) = 0.5*(A%cmpt(i,5)-A%cmpt(i,8))
            S%cmpt(i,6) = 0.5*(A%cmpt(i,6)-A%cmpt(i,9))
            S%cmpt(i,7) = -S%cmpt(i,4)
            S%cmpt(i,8) = -S%cmpt(i,5)
            S%cmpt(i,9) = -S%cmpt(i,6)

        end do

    end function antiSymmtric

! **********************************************************************************************************************

    function Hodge(A) result(H)

    !===================================================================================================================
    ! Description:
    !! Hodge computes the Hodge's decomposition a tensor as:
    !! \begin{equation*}  H = (A_{12}, -A_{13}, A_{23})\end{equation*}
    !===================================================================================================================

        type(flubioTensor) :: A
        !! First Tensor
    !-------------------------------------------------------------------------------------------------------------------

        real :: H(A%tSize,3)
        !! Hodge dual
    !-------------------------------------------------------------------------------------------------------------------

        integer :: i
    !-------------------------------------------------------------------------------------------------------------------

        do i=1,A%tSize

            H(i,1) = A%cmpt(i,6)
            H(i,2) = -A%cmpt(i,5)
            H(i,3) = A%cmpt(i,4)

        end do

    end function Hodge

! **********************************************************************************************************************

    function trace(A) result(T)

    !===================================================================================================================
    ! Description:
    !! trace returns the tensor trace:
    !! \begin{equation*}T = A_{ij}\delta_{i,j}\end{equation*}
    !===================================================================================================================

        type(flubioTensor) :: A
        !! First Tensor

    !-------------------------------------------------------------------------------------------------------------------

        real :: T(A%tSize)
        !! Tensor trace
    !-------------------------------------------------------------------------------------------------------------------

        integer :: i
    !-------------------------------------------------------------------------------------------------------------------

        do i=1,A%tSize
            T(i) = A%cmpt(i,1) + A%cmpt(i,2) + A%cmpt(i,3)
        end do

    end function trace

! **********************************************************************************************************************

end module flubioTensors
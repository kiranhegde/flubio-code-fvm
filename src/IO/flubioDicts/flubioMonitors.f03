!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module runTimeProc

!===================================================================================================================
! Description:
!! This module implements the dictionary storing the entries of settings/runTimeProc and associated flags.
!===================================================================================================================

    use generalDictionaries

    implicit none

    type, extends(generalDict) :: monitorsDict

        logical :: found
        !! flag to check the presence of the dictionary

        integer :: numberOfMonitors
        !! numberOfMonitors

    contains
        procedure :: set => setRunTimeProc

    end type monitorsDict

contains

    subroutine setRunTimeProc(this)

    !===================================================================================================================
    ! Description:
    !! setRunTimeProc fills the the run time processing dictionary.
    !===================================================================================================================

        class(monitorsDict) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: dictName, typeValue, monitorName, path
        !! First array element lenght

        character(len=50), dimension(:), allocatable  :: splitPath
    !------------------------------------------------------------------------------------------------------------------

        integer :: iMonitor
    !------------------------------------------------------------------------------------------------------------------

        logical :: keyFound, typeFound, monitorFound, monitorsFound, nameFound
    !------------------------------------------------------------------------------------------------------------------

        type(json_core) :: jcore

        type(json_value), pointer :: dictPointer, monitorsPointer, monitorPointer
    !------------------------------------------------------------------------------------------------------------------

        dictName = 'settings/monitors'

        ! Initialise the JSON dictionary
        inquire(file=trim(dictName), exist=this%found)

        if (this%found) then
            call this%initJSON(dictName)
            call this%json%info('Monitors', n_children=this%numberOfMonitors)
        else
            this%numberOfMonitors = 0
        end if

    end subroutine setRunTimeProc

end module runTimeProc

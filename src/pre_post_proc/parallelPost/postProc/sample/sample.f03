!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module sample

    ! Samplig objects
    use generalDictionaries
    use probes
    use lineProbes

    implicit none

    ! Dictionary creation
    type, extends(generalDict) :: sampleDict
        integer :: numberOfTasks
        logical :: found
    contains
        procedure :: set => setSampleDict
    end type sampleDict

    character(len=15), dimension(:), allocatable :: tasksTypes
    type(sampleDict) :: flubioSample

contains

    subroutine setSampleDict(this)

    !===================================================================================================================
    ! Description:
    !! setSampleDict parses and stores the sample dictioanary from settings/sampling.
    !==================================================================================================================

        class(sampleDict) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: dictName
        !! dictionary name

        character(len=:), allocatable :: optName
        !! option name

        character(len=:), allocatable :: val
        !! value
    !------------------------------------------------------------------------------------------------------------------

        logical :: found, tasksFound
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, error_cnt, n
    !------------------------------------------------------------------------------------------------------------------

        dictName = 'settings/sampling'
        n = 0

        ! Initialise the JSON dictionary
        inquire(file=trim(dictName), exist=found)

        if (.not. found) then
            this%found = .false.
        else
            this%found = .true.
            call this%initJSON(dictName)
            call raiseKeyErrorJSON('samplingTasks', tasksFound)
            call this%json%info('samplingTasks', n_children=this%numberOfTasks)
        end if

    end subroutine setSampleDict

! *********************************************************************************************************************

    subroutine sampling(is, ie, iStep)

    !==================================================================================================================
    ! Description:
    !! sampleFields create the regions with the mesha as defined in flubiSampling.
    !==================================================================================================================

        character(len=10) :: file_number, rank_number, procID

        character(len=10), dimension(:), allocatable :: fieldList

        character(len=:), allocatable :: dirName
    !------------------------------------------------------------------------------------------------------------------

        integer :: fnumber, is, ie, iStep
    !------------------------------------------------------------------------------------------------------------------

        logical :: dirExists
    !------------------------------------------------------------------------------------------------------------------

        do fnumber=is,ie,iStep

            write(file_number, '(i0)') fnumber
            write(rank_number, '(i0)') id

            call flubioMsg('FLUBIO: processing field number '//trim(file_number))

            ! Create a directory for the field. If it does not exists create it.
            dirName = 'postProc/sampling/'//file_number
            inquire(file=trim(dirName)//'/.', exist=dirExists)

            if(.not. dirExists) call execute_command_line ('mkdir -p ' // adjustl(trim(dirName)))

            ! Sample fields
            call sampleFields(fnumber)

        enddo

    end subroutine sampling

! *********************************************************************************************************************

    subroutine sampleFields(iTime)

    !==================================================================================================================
    ! Description:
    !! sampleFields create the regions with the mesha as defined in flubiSampling.
    !==================================================================================================================

        character(len=:), allocatable :: taskType, taskName, interpType, fields
        !! First array element lenght

        character(len=15), dimension(:), allocatable :: fields_array
    !------------------------------------------------------------------------------------------------------------------

        integer :: n, nn, iTask, iTime, isize
    !------------------------------------------------------------------------------------------------------------------

        real, dimension(:), allocatable :: rvec
    !------------------------------------------------------------------------------------------------------------------

        logical :: found, keyFound
    !------------------------------------------------------------------------------------------------------------------

        type(json_core) :: jcore

        type(json_value), pointer :: dictPointer, tasksPointer, taskPointer

        type(probeObj) :: probes

        type(lineProbeObj) :: line
    !------------------------------------------------------------------------------------------------------------------

        isize = storage_size(n)/8
        call probes%ownedProbes%new(1, isize, QVECTOR_RESIZE_LINEAR)
        call line%ownedProbes%new(1, isize, QVECTOR_RESIZE_LINEAR)

        call flubioSample%set()

        ! Get the pointer to the dictionary
        call flubioSample%json%get(dictPointer)

        ! Initialize json factory
        call jcore%initialize()
        call jcore%get(dictPointer, 'samplingTasks', tasksPointer, found)

        do iTask = 1, flubioSample%numberOfTasks

            ! Get tasks pointer
            call jcore%get_child(tasksPointer, iTask, taskPointer, found)

            ! Get task name
            call jcore%get(taskPointer, 'name', taskName, found)
            if(.not. found) call flubioStopMsg('ERROR: cannot find the task name.')

            ! Get interpolation type
            call jcore%get(taskPointer, 'interpolation', interpType, found)
            if(.not. found) call flubioStopMsg('ERROR: cannot find the task interpolation type.')

            ! Get interpolation type
            call jcore%get(taskPointer, 'numberOfNeighbours', nn, found)
            if(.not. found) nn = 4

            ! Get fields to post-process
            call jcore%get(taskPointer, 'fields', fields, found)
            if(found) then
                call split_in_array(fields, fields_array,',')
            else
                call flubioStopMsg('ERROR: cannot find the fields to process.')
            end if

            ! Get task type
            call jcore%get(taskPointer, 'type', taskType, found)
            if(.not. found) call flubioStopMsg('ERROR: cannot find the task type.')

            if(taskType == 'probes') then

                probes%name = taskName
                probes%type = taskType
                probes%interpType = interpType
                probes%nn = nn
                allocate(probes%fields(size(fields_array)))
                probes%fields = fields_array

                ! Probe coordinates (not sure I can leave them as allocatable)
                call jcore%get(taskPointer, 'nProbes', n, keyFound)
                if(.not. keyFound) call flubioStopMsg('ERROR: cannot find the number of probes for task "'//taskName//'"')
                probes%nProbes = n

                allocate(probes%x(n))
                allocate(probes%y(n))
                allocate(probes%z(n))

                call jcore%get(taskPointer, 'x', rvec, keyFound)
                if(.not. keyFound) call flubioStopMsg('ERROR: cannot find the x coordiantes for task "'//taskName//'"')
                probes%x = rvec

                call jcore%get(taskPointer, 'y', rvec, keyFound)
                if(.not. keyFound) call flubioStopMsg('ERROR: cannot find the y coordiantes for task "'//taskName//'"')
                probes%y = rvec

                call jcore%get(taskPointer, 'z', rvec, keyFound)
                if(.not. keyFound) call flubioStopMsg('ERROR: cannot find the z coordiantes for task "'//taskName//'"')
                probes%z = rvec

                ! Find out the probes hosted by the processor
                call probes%findOwnedProbes()

                ! Execute the probing
                call probes%execute(iTime)

                ! Deallocate for the next probe if any
                call probes%clear()

            elseif(taskType == 'lineProbes') then

                line%name = taskName
                line%type = taskType
                line%interpType = interpType

                allocate(line%fields(size(fields_array)))
                line%fields = fields_array

                ! Parse start and end points
                call jcore%get(taskPointer, 'startPoint', rvec, keyFound)
                if(.not. keyFound) call flubioStopMsg('ERROR: cannot find the start point for task "'//taskName//'"')
                line%startPoint = rvec

                call jcore%get(taskPointer, 'endPoint', rvec, keyFound)
                if(.not. keyFound) call flubioStopMsg('ERROR: cannot find the end point for task "'//taskName//'"')
                line%endPoint = rvec

                ! Probe coordinates (not sure I can leave them as allocatable)
                call jcore%get(taskPointer, 'nProbes', n, keyFound)
                if(.not. keyFound) call flubioStopMsg('ERROR: cannot find the number of probes for task "'//taskName//'"')
                line%nProbes = n

                allocate(line%x(n))
                allocate(line%y(n))
                allocate(line%z(n))

                ! Create probes
                call line%createProbePoints()

                ! Find out the probes hosted by the processor
                call line%findOwnedProbes()

                ! Execute the probing
                call line%execute(iTime)

                ! Deallocate for the next probe if any
                call line%clear()

            else
                call flubioMsg('FLUBIO WARNING: unknown task type "'//taskType//'", will skip this.')
            end if

        end do

        ! Clean up
        nullify(dictPointer)
        nullify(tasksPointer)
        nullify(taskPointer)
        call jcore%destroy()

        call flubioMsg('DONE!')

    end subroutine sampleFields

! *********************************************************************************************************************

end module sample

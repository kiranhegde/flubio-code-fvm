!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module flubioFields

!==================================================================================================================
! Description:
!! flubioFields implements the data structures and the methods operating on a "field".
!==================================================================================================================

    use flubioMpi
    use meshvar

    implicit none

    type, public :: flubioField

        character(len=30), allocatable :: fieldName(:)
        !! Name of the field

        character(10) :: bcClassType
        !! Set this paramenter to specifay which set of boundary condition the fields should point to

        integer nComp
        !! Number of components

        real, dimension(:,:), allocatable :: phi
        !! Vector field variable

        real, dimension(:,:), allocatable :: phif
        !! Vector field variable

        real, dimension(:,:,:), allocatable :: phiGrad
        !!  Vector field gradient

        real, dimension(:,:,:), allocatable :: ghosts
        !! Vector field at ghost cells

        real, dimension(:,:,:), allocatable :: cellNeighbours
        !! For each element, cellNeighbours contains the value of the neighbours

    contains

        procedure :: createField
        procedure :: destroyField
        procedure :: setUpField
        procedure :: updateGhosts
        procedure :: readFromFile
        procedure :: linearInterpolation
        procedure :: getBoundaryField

    end type flubioField

contains

    subroutine createField(field, nComp)

    !===================================================================================================================
    ! Description:
    !! createField is the class constructor.
    !===================================================================================================================

        class(flubioField) :: field
    !-------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: dummy_str
        !! dummy string

        character(len=30) :: fname(nComp)
        !! field name
    !------------------------------------------------------------------------------------------------------------------

        integer :: iComp
        !!  target component

        integer :: nComp
        !! number of field components
    !------------------------------------------------------------------------------------------------------------------

        field%nComp = nComp

        allocate(field%fieldName(nComp))

        allocate(field%phi(numberOfElements+numberOfBfaces,nComp))
        allocate(field%phif(numberOfFaces,nComp))
        allocate(field%phiGrad(numberOfElements+numberOfBfaces,3,nComp))
        allocate(field%ghosts(maxProcFaces,nComp,numberOfProcBound1))

        field%phi = 0.0
        field%phif = 0.0
        field%phiGrad = 0.0
        field%ghosts = 0.0

    end subroutine createField

! *********************************************************************************************************************

    subroutine destroyField(field)

    !==================================================================================================================
    ! Description:
    !! destroyField is the field deconstructor. It can be used to delete a field from the momory.
    !==================================================================================================================

        class(flubioField) :: field
    !-------------------------------------------------------------------------------------------------------------------

        deallocate(field%fieldName)

        deallocate(field%phi)
        deallocate(field%phif)
        deallocate(field%phiGrad)
        deallocate(field%ghosts)

    end subroutine destroyField

! *********************************************************************************************************************

    subroutine updateGhosts(this)

    !==================================================================================================================
    ! Description:
    !! updateGhosts updates the ghost values at a each processor boundary.
    !! If you have updated a field, this subroutine MUST be called, otherwise ghosts points will not be updated.
    !! Some important operations such as interpolation and gradient compuation call this subroutine automatically.
    !==================================================================================================================

        class(flubioField) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: ierr
        !! error flag

        integer :: stat(MPI_STATUS_SIZE)
        !! mpi status

        integer :: ireq(2*numberOfProcBound1)
        !! requests array
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, j, iBoundary, iElement, iBFace, iFace, iOwner, is, ie, iB, pNeigh

        integer :: nbuf

        integer :: nFace
        !! number of faces

        integer :: fComp
        !! number of field components
    !------------------------------------------------------------------------------------------------------------------

        integer :: iSend
        !! target sent message

        integer :: iRecv
        !! target received message

        integer :: istat(MPI_STATUS_SIZE)
        !! target request status

        integer :: nSend
        !! number of sent messages

        integer :: nRecv
        !! inumber of received messages

        integer :: idr
        !! receiving process

        integer :: buffSize
        !! buffer size
    !------------------------------------------------------------------------------------------------------------------

        real, dimension(:,:,:), allocatable :: sbuf
        !! sent buffer

        real, dimension(:,:,:), allocatable :: rbuf
        !! received buffer
    !------------------------------------------------------------------------------------------------------------------

        ! ---------------------------------------------------------------------------!
        ! Send and Receive the cell center field value from the neighbour processors !
        ! ---------------------------------------------------------------------------!

        fComp = this%nComp

        ! Prepare the send buffers
        buffSize = maxProcFaces

        allocate(sbuf(buffSize,fComp,numberOfProcBound1))
        allocate(rbuf(buffSize,fComp,numberOfProcBound1))

        sbuf = 0.0
        rbuf = 0.0

        do iBoundary=1,numberOfProcBound

            i = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(i)
            ie = is+mesh%boundaries%nFace(i)-1

            ! Fill the send buffer
            nbuf = 0
            do iFace=is,ie
                nbuf = nbuf+1
                iOwner = mesh%owner(iFace)
                sbuf(nbuf,:,iBoundary) = this%phi(iOwner,:)
            enddo

        enddo

        !-------------------------------------------------!
        ! Perform the comunication between the processors !
        !-------------------------------------------------!

        nSend = numberOfProcBound
        nRecv = numberOfProcBound

        ! Open senders
        do iSend=1,nSend
            i = mesh%boundaries%procBound(iSend)
            idr = mesh%boundaries%neighProc(i)
            buffSize = maxProcFaces*fComp
            call mpi_isend(sbuf(1:maxProcFaces,:,iSend),buffSize,MPI_REAL8,idr,0,MPI_COMM_WORLD,ireq(iSend),ierr)
        enddo

        ! Open receivers
        do iRecv=1,nRecv
            i = mesh%boundaries%procBound(iRecv)
            idr = mesh%boundaries%neighProc(i)
            buffSize = maxProcFaces*fComp
            call mpi_irecv(rbuf(1:maxProcFaces,:,iRecv),buffSize,MPI_REAL8,idr,0,MPI_COMM_WORLD,ireq(nSend+iRecv),ierr)
        enddo

        ! Wait the end of the communications
        call mpi_waitAll(nSend+nRecv,ireq,MPI_STATUSES_IGNORE,ierr)

        this%ghosts = rbuf

        deallocate(sbuf)
        deallocate(rbuf)

        !------------------------------------------------!
        ! Processor faces stores the value of the ghosts !
        !------------------------------------------------!

        iB = numberOfElements + sum(mesh%boundaries%nFace(1:numberOfRealBound))
        do iBoundary=1,numberOfProcBound

            i = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(i)
            ie = is+mesh%boundaries%nFace(i)-1

            pNeigh = 0
            do iBFace=is,ie
                iB = iB+1
                pNeigh = pNeigh +1
                this%phi(iB, :) = this%ghosts(pNeigh,:,iBoundary)
            end do

        end do

    end subroutine updateGhosts

!**********************************************************************************************************************

    subroutine setUpField(this, nComp)

    !==================================================================================================================
    ! Description:
    !! setUpField sets up a field, calling some mandatory produres defined in this class.
    !==================================================================================================================

        class(flubioField) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: nComp
        !! number of components
    !------------------------------------------------------------------------------------------------------------------

        call this%createField(nComp)
        call this%updateGhosts()

    end subroutine setUpField

! *********************************************************************************************************************

    subroutine readFromFile(this, timeToRead)

    !==================================================================================================================
    ! Description:
    !! readFromFile reads the field from a file.
    !==================================================================================================================

        class(flubioField) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: postDir
        !! post processing directory

        character(len=:), allocatable :: fieldDir
        !! field directory

        character(len=20) :: timeStamp
        !! time stamp

        character(len=20) ::  procID
        !! processor ID
    !------------------------------------------------------------------------------------------------------------------

        logical :: dirExists
        !! boolean to check if the directory exists
    !------------------------------------------------------------------------------------------------------------------

        integer :: iComp
        !! target components

        integer :: timeToRead
        !! time to read
    !------------------------------------------------------------------------------------------------------------------

        write(procID,'(i0)') id

        postDir='postProc/fields/'
        write(timeStamp,'(i0)') timeToRead
        fieldDir = postDir//trim(timeStamp)//'/'

        call flubioMsg('FLUBIO: recovering ' // trim(this%fieldName(1)) // ' from file...')

        ! Check if the directorty exists.
        inquire(file=trim(fieldDir)//'/.', exist=dirExists)

        if(dirExists) then

            ! Write Field
            open(1,file=fieldDir//trim(this%fieldName(1))//'-d'//trim(procID)//'.bin', form='unformatted')

                read(1) this%nComp
                read(1) this%fieldName

                do iComp=1,this%nComp
                    read(1) this%phi(:,iComp)
                    read(1)
                    read(1)
                end do

            close(1)

        else

        call flubioMsg('FLUBIO: field folder does not exists... something went wrong!')

        end if

        ! Update ghosts after read
        call this%updateGhosts()

    end subroutine readFromFile

! *********************************************************************************************************************

    function getFieldSize(fieldName, timeToRead) result(nComp)

    !==================================================================================================================
    ! Description:
    !! readFromFile reads the field from a file.
    !==================================================================================================================

        character(len=*) :: fieldName
        !! Name of the field

        character(len=:), allocatable :: postDir
        !! post processing directory

        character(len=:), allocatable :: fieldDir
        !! field directory

        character(len=20) :: timeStamp
        !! time stamp

        character(len=20) :: procID
        !! processor ID
    !------------------------------------------------------------------------------------------------------------------

        logical :: dirExists
        !! boolean to check if the directory exists
    !------------------------------------------------------------------------------------------------------------------

        integer :: timeToRead
        !! time to read

        integer :: nComp
        !! number of components
    !------------------------------------------------------------------------------------------------------------------

        write(procID,'(i0)') id

        postDir='postProc/fields/'
        write(timeStamp,'(i0)') timeToRead
        fieldDir = postDir//trim(timeStamp)//'/'

        call flubioMsg('FLUBIO: recovering ' // trim(fieldName) // ' from file...')

        ! Check if the directorty exists.
        inquire(file=trim(fieldDir)//'/.', exist=dirExists)

        if(dirExists) then
            ! Write Field
            open(1,file=fieldDir//trim(fieldName)//'-d'//trim(procID)//'.bin', form='unformatted')
                read(1) nComp
            close(1)
        else
            call flubioMsg('FLUBIO: field folder does not exists... something went wrong!')
        end if

    end function getFieldSize

!**********************************************************************************************************************

    subroutine linearInterpolation(field)

    !==================================================================================================================
    ! Description:
    !! interpolateElementToFaceStd performs a standard linear interpolation to find the face values of a variable.
    !==================================================================================================================

        class(flubioField) :: field
        !! field to interpolate
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iBFace,  iFace, Owner, Neighbour

        integer :: i, pNeigh, is, ie, opt, patchFace
    !------------------------------------------------------------------------------------------------------------------

        real :: corr
        !! skewness correction

        real :: gC
        !! linear interpolation weight

        real :: magSf
        !! face area
   !------------------------------------------------------------------------------------------------------------------

        field%phif = 0.0

        call field%updateGhosts()

        do iFace=1,numberOfIntFaces

            Owner = mesh%owner(iFace)
            Neighbour = mesh%neighbour(iFace)

            gC = mesh%gf(iFace)

            field%phif(iFace,:) = gC*field%phi(Neighbour,:) + (1-gC)*field%phi(Owner,:)

        enddo

        !----------------!
        ! Boundary Faces !
        !----------------!

        patchFace = 0
        i = 0
        do iBoundary=1,numberOfBoundaries

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1
            pNeigh = 0

            if(mesh%boundaries%realBound(iBoundary)==-1) i=i+1

            do iBFace=is, ie

                patchFace = patchFace+1
                Owner = mesh%owner(iBFace)

                if(mesh%boundaries%realBound(iBoundary)/=-1) then
                    ! Real boundaries
                    field%phif(iBFace,:) = field%phi(numberOfElements+patchFace,:)
                else
                    ! Processor Boundaries
                    pNeigh = pNeigh+1
                    gC = mesh%gf(iBFace)
                    field%phif(iBFace,:) = gC*field%ghosts(pNeigh,:,i) + (1.0-gC)*field%phi(Owner,:)
                endif

            enddo

        enddo

    end subroutine linearInterpolation

! *********************************************************************************************************************

    function getBoundaryField(field, boundaryName) result(boundaryField)

    !==================================================================================================================
    ! Description:
    !! getBoundaryField returns a field at a target boundary
    !==================================================================================================================

        class(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        character(len=*) boundaryName
    !------------------------------------------------------------------------------------------------------------------

        integer :: is, ie, nFaces, startFace, endFace

        integer :: targetBoundary(3)
    !------------------------------------------------------------------------------------------------------------------

        real, dimension(:,:), allocatable :: boundaryField
    !------------------------------------------------------------------------------------------------------------------

        targetBoundary = mesh%boundaries%findBoundaryByName(boundaryName)
        is = targetBoundary(1)
        ie = targetBoundary(2)
        nFaces = targetBoundary(3)

        allocate(boundaryField(nFaces, field%nComp))
        boundaryField = 0.0

        startFace = numberOfElements + (is-numberOfIntFaces)
        endFace = numberOfElements + (ie-numberOfIntFaces)
        boundaryField(1:nFaces,:) = field%phi(startFace:endFace, :)

    end function getBoundaryField

! *********************************************************************************************************************

end module flubioFields
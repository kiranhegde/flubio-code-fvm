!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

	program meshView

		use serialmeshvar

		implicit none

		character(len=10), dimension(:), allocatable :: args

		character(len=6) :: fmt
	!------------------------------------------------------------------------------------------------------------------

		integer :: fnumber, dataForm, nargs, stat
	!------------------------------------------------------------------------------------------------------------------

		!------------------------!
		! command line arugments !
		!------------------------!

		nargs = command_argument_count()
		allocate(args(nargs))

		call parseCommandLine(args, nargs, fmt)

		!------------------!
		! read serial mesh !
		!------------------!

		call mesh%readMeshFromFiles()

		!------------------!
		! Write data files !
		!------------------!

		call writeMeshVTK(trim(fmt))

	end program meshView

! *********************************************************************************************************************

	subroutine writeMeshVTK(fmt)

		use penf
		use vtk_fortran, only : vtk_file
		use serialVTKFormat
		
		implicit none

		type(vtk_file) :: vtk_field_file

		character(len=*) :: fmt

		character(len=:), allocatable :: fname
	!------------------------------------------------------------------------------------------------------------------

		integer :: ierr
	!------------------------------------------------------------------------------------------------------------------

		fname = 'postProc/VTKfields/mesh.vtu'
	
		ierr = vtk_field_file%initialize(format=fmt, filename=fname, mesh_topology='UnstructuredGrid')

		call writeCoordinatesVTK(vtk_field_file)

		ierr = vtk_field_file%xml_writer%write_dataarray(location='cell', action='close')

		ierr = vtk_field_file%xml_writer%write_piece()
		ierr = vtk_field_file%finalize()

		write(*,*) 'FLUBIO: mesh has been written!'

	end subroutine writeMeshVTK

! *********************************************************************************************************************

	subroutine parseCommandLine(args, nargs, fmt)

		implicit none

		integer :: p, nargs, stat

		character(len=*) :: args(nargs)

		character(len=6) :: fmt
	!------------------------------------------------------------------------------------------------------------------

		! Get the arguments
		do p=1,nargs
			call get_command_argument(number=p, value=args(p), status=stat)
		enddo

		! Default value for fmt
		fmt = 'binary'

		! Process the options
		do p=1,nargs

			if(args(p)=='-format' .or. args(p)=='-f') then
				fmt = trim(args(p+1))
			elseif(args(p)=='-help') then
				write(*,*) 'Command line options:'
				write(*,*) '-format: format of the vtu file (ascii or binary)'
				write(*,*)
				write(*,*) 'example: flubio_meshview'
				stop
			endif

		enddo

	end subroutine parseCommandLine

! *********************************************************************************************************************

!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module auxilaries

    implicit none

contains

! *********************************************************************************************************************

    subroutine getFace(f1, f2, Sf, iFaces, iElement, dirFlag, nFaces)

        integer :: i, iElement, iFace, iMax(2), dirFlag, nFaces

        integer :: f, f1, f2, iFaces(6)
    !------------------------------------------------------------------------------------------------------------------

        real :: dot, dir(3), Sf(nFaces,3), nf(3), aux(6), aux2(6), low
    !------------------------------------------------------------------------------------------------------------------

        if (dirFlag==0) then
            dir(1) = 1.0
            dir(2) = 0.0
            dir(3) = 0.0
        elseif (dirFlag==1) then
            dir(1) = 0.0
            dir(2) = 1.0
            dir(3) = 0.0
        else
            dir(1) = 0.0
            dir(2) = 0.0
            dir(3) = 1.0
        endif

        low = -1000.0
        aux = low

        do iFace=1,6
            f = iFaces(iFace)
            nf = Sf(f,1:3)/sqrt(Sf(f,1)**2+Sf(f,2)**2+Sf(f,3)**2)
            dot = dot_product(nf,dir)
            aux(iFace) = abs(dot)
        enddo

        ! find out the largest value and its index
        call maxLocInd(aux,6, 10, f1)

        ! find out the second largest value and its index
        aux2 = aux
        aux2(f1) = low

        call maxLocInd(aux2, 6, f1, f2)

    end subroutine getFace

! *********************************************************************************************************************

    function distance(a, b) result(r)

        real :: a(3), b(3), dx, dy, dz, r
    !------------------------------------------------------------------------------------------------------------------

        dx = a(1)-b(1)
        dy = a(2)-b(2)
        dz = a(3)-b(3)

        r = sqrt(dx**2+dy**2+dz**2)

    end function distance

! *********************************************************************************************************************

    subroutine globalConn(local2global,totSizeE,nid)

        character(10) :: procID

        character :: file_number*4
    !------------------------------------------------------------------------------------------------------------------

        integer :: numberOfPoints(nid), numberOfBFaces(nid), numberOfElements(nid), &
                numberOfFaces(nid), numberOfIntFaces(nid), numberOfBElements(nid)

        integer :: i, ii, id, nid, anumber, totSizeE, patchFaceE

        integer :: label(totSizeE,nid), local2global(totSizeE)
    !------------------------------------------------------------------------------------------------------------------

        !-------------------------------------------!
        ! Pass from actual labeling to original one !
        !-------------------------------------------!

        local2global = -1
        patchFaceE = 0

        do ii=1,nid

            id=ii-1
            write(procID,'(i0)') id

            open(1,file='grid/bin/mesh-d'//trim(procID)//'.bin',form='unformatted')

                read(1) numberOfPoints(ii), numberOfFaces(ii), numberOfIntFaces(ii), &
                        numberOfBFaces(ii), numberOfElements(ii), numberOfBElements(ii)

            close(1)

            ! Read local to global connectivities
            open(1,file='grid/processor'//trim(procID)//'/cellProcAddressing')

            read(1,*)

            do i=1,numberOfElements(ii)

                read(1,*) label(i,ii)

                ! Fill array
                patchFaceE = patchFaceE+1
                local2global(patchFaceE) = label(i,ii)+1
            enddo

            close(1)

        enddo

    end subroutine globalConn

! *********************************************************************************************************************

    subroutine cross_prod_aux(a,b,c)

        real :: a(3), b(3), c(3)
    !------------------------------------------------------------------------------------------------------------------

        c = 0.0
        c(1) = a(2)*b(3) - a(3)*b(2)
        c(2) = a(3)*b(1) - a(1)*b(3)
        c(3) = a(1)*b(2) - b(1)*a(2)

    end subroutine cross_prod_aux

! *********************************************************************************************************************

    subroutine maxLocInd(a,isize,iskip,imax)

        integer :: i, imax, isize, iskip
    !------------------------------------------------------------------------------------------------------------------

        real :: a(isize), mx
    !------------------------------------------------------------------------------------------------------------------

        mx=-10000

        do i=1,isize

            if(abs(a(i))>mx .and. i/=iskip) then
                mx=abs(a(i))
                imax=i
            endif

        enddo

    end  subroutine maxLocInd

! *********************************************************************************************************************

    !function mag(a) result(b)

    !    real :: a(3), b
    !------------------------------------------------------------------------------------------------------------------

    !    b = sqrt(a(1)**2+a(2)**2+a(3)**2)

    !end function mag

! ***************************************************************************************************************************

    subroutine intersection(I1, I2, point, evrt)

        integer :: i, j, k, iNode, I1(4), I2(4), point, dime

        integer :: inter(2), evrt
    !------------------------------------------------------------------------------------------------------------------

        ! Intersect the faces nodes I1 nad I2
        k=0
        i=1
        j=1

        dime=4

        do while (i<= dime .and. j<=dime)

            if(I1(i)<I2(j)) then

                i=i+1

            elseif(I2(j)<I1(i)) then

                j=j+1

            else ! I1(i)=I2(j)

                k=k+1

                inter(k)=I2(j)

                i=i+1
                j=j+1

            endif

        enddo

        ! Exclude from inter the point belonging also to the front face
        k=0

        do i=1,2
            if(inter(i)/=point) then
                evrt=inter(i)
                k=k+1
            endif
        enddo

        ! Check
        if(k>1)  then
            write(*,*) 'ERROR in mesh.F95::Intersection : evrt cannot be grater than 1'
            stop
        endif

    end subroutine intersection

! *********************************************************************************************************************

    subroutine intervoid(I1, I2, k)

        integer :: i, j, k, iNode, I1(4), I2(4), flag, dime

        integer :: inter(2)
    !------------------------------------------------------------------------------------------------------------------

        ! Intersect the faces nodes I1 and I2
        k=0
        i=1
        j=1

        inter=-1

        dime=4

        do while (i<= dime .and. j<=dime)

            if(I1(i)<I2(j)) then
                i=i+1
            elseif(I2(j)<I1(i)) then
                j=j+1
            else ! I1(i)=I2(j)
                k=k+1
                inter(k)=I2(j)

                i=i+1
                j=j+1
            endif
        enddo

    end subroutine intervoid

! *********************************************************************************************************************

    subroutine sort(v1, dime)

        integer :: i, j, dime, v1(dime), v2(dime)
    !------------------------------------------------------------------------------------------------------------------

        ! Sort the array in ascending order. Overwrite the input vector.
        v2=-1

        do i=1,dime
            j = minloc(v1, dime)
            v2(i) = v1(j)
            v1(j) = 1000000000
        enddo

        v1 = v2

    end subroutine sort

! *********************************************************************************************************************

    subroutine intersect(I1, I2, inter, dime)

        integer :: i, j, k, iNode, dime

        integer :: I1(dime), I2(dime), inter(2)
    !------------------------------------------------------------------------------------------------------------------

        ! Intersect the faces nodes I1 and I2
        k=0
        i=1
        j=1

        inter=-1

        do while (i<= dime .and. j<=dime)

            if(I1(i)<I2(j)) then
                i=i+1
            elseif(I2(j)<I1(i)) then
                j=j+1
            else ! I1(i)=I2(j)

                k=k+1
                inter(k)=I2(j)

                i=i+1
                j=j+1

            endif
        enddo

    end subroutine intersect

! *********************************************************************************************************************

    subroutine find_vertex_tetra(I1, I2, I3, vertex)

        integer :: i, j, k, iNode, I1(3), I2(3), I3(3), dummy(3), dime

        integer :: inter(2), vertex
    !-------------------------------------------------------------------------------------------------------------------

        ! Intesect two faces
        call sort(I1,3)
        call sort(I2,3)
        call intersect(I1, I2, inter, 3)

        ! Intersect the result with the third face, the common point is the vertex
        dummy(1:2) = inter
        dummy(3) = -10

        call sort(dummy,3)
        call sort(I3,3)
        call intersect(I3, dummy, inter, 3)

        vertex = inter(1)

    end subroutine find_vertex_tetra

! *********************************************************************************************************************

end module auxilaries
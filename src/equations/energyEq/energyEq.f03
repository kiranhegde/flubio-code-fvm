!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module energyEq

!==================================================================================================================
! Description:
!! This modules implements the energy equation and its data structure.
!==================================================================================================================

    use globalTimeVar
    use meshvar
    use flubioFields
    use gradient
    use transportEq

    implicit none

    !type(flubioField) :: temperature
    type(flubioField) :: kEff
    type(flubioField) :: specificHeat

    type, public, extends(transportEqn) :: energyEqn

        real :: Pr, Pr_t

    contains

        procedure :: updateEddyDiffusivity
        procedure :: energySourceTerm
        procedure :: addDissipationTerm
        procedure :: addViscousDissipationTerm
        procedure :: addSpecificHeatTerm
        procedure :: addDPDt

    end type

    type(energyEqn), target :: Eeqn
    
contains

    subroutine createEnergyEquation()

    !=================================================================================================================
    ! Description:
    !! createEnergyEquation sets up the energy equation
    !=================================================================================================================

        integer :: ierr
    !------------------------------------------------------------------------------------------------------------------

        real :: myspecificHeat

        real :: myPr, myPr_t
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        call temperature%setUpField('T', 'scalar', 1)
        call kEff%setUpCoeff('k', appendToList=.false.)
        call specificHeat%setUpCoeff('specificHeat', appendToList=.false.)

        call physical%json%get('specificHeat', myspecificHeat, found)
        call raiseKeyErrorJSON('specificHeat', found, 'physical/constant')
        specificHeat%phi = myspecificHeat

        if(lower(flubioTurbModel%modelName)=='dns') then
            call physical%json%get('Pr', myPr, found)
            call raiseKeyErrorJSON('Pr', found, 'physical/constant')
            kEff%phi = viscos/myPr
            myPr_t = 1.0
        else
            call physical%json%get('Pr', myPr, found)
            call raiseKeyErrorJSON('Pr', found, 'physical/constant')
            call physical%json%get('Pr_t', myPr_t, found)
            call raiseKeyErrorJSON('Pr_t', found, 'physical/constant')
            Eeqn%Pr_t = myPr_t
            kEff%phi = viscos/myPr + (nu%phi-viscos/myPr)/myPr_t
        end if

        Eeqn%Pr = myPr
        Eeqn%Pr_t = myPr_t

        call computeGradient(kEff)
        call computeGradient(specificHeat)

        call Eeqn%createEq('Eeqn', temperature%nComp)
        call Eeqn%setEqOptions(trim(Eeqn%eqName) // '_')

    end subroutine createEnergyEquation

! *********************************************************************************************************************

    subroutine energyEquation()

    !=================================================================================================================
    ! Description:
    !! energyEquation is the driver for the energy equation.
    !=================================================================================================================
        
        integer :: ierr, iElement
    !------------------------------------------------------------------------------------------------------------------
        
        !----------------------!
        ! Assemble and solve E !
        !----------------------!

        call Eeqn%updateEddyDiffusivity()

        call Eeqn%assembleEqWithCoeff(temperature, kEff, specificHeat)

        !call Eeqn%energySourceTerm()

        ! Relax transport equation
        if(steadySim==1) call Eeqn%relaxEq(temperature)

        call Eeqn%solve(temperature, 1, 0, 0)

        call temperature%updateBoundaryField()
        
        call computeGradient(temperature)

        ! Update temperature dependent  physical constants
        !call this%sunderlandLaw()

    end subroutine energyEquation

! *********************************************************************************************************************

    subroutine updateEddyDiffusivity(this)

        class(energyEqn):: this
    !------------------------------------------------------------------------------------------------------------------

        call kEff%updateOldFieldValues()
        call specificHeat%updateOldFieldValues()

        if(lower(flubioTurbModel%modelName)=='dns') then
            kEff%phi = viscos/Eeqn%Pr
        else
            kEff%phi = viscos/Eeqn%Pr + (nu%phi-viscos)/Eeqn%Pr_t
        end if

    end subroutine updateEddyDiffusivity
    
! *********************************************************************************************************************

    subroutine energySourceTerm(this)

        class(energyEqn):: this
    !------------------------------------------------------------------------------------------------------------------

        call this%addDissipationTerm()
        call this%addViscousDissipationTerm()
        call this%addSpecificHeatTerm()
        call this%addDPDt()

    end subroutine energySourceTerm
    
 ! *********************************************************************************************************************

    subroutine addDissipationTerm(this)

        class(energyEqn):: this
        !! target Element
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: vol
        !! cell volume

        real :: mu
        !! cell center viscosity

        real :: dudx, dudy, dudz

        real :: dvdx, dvdy, dvdz

        real :: dwdx, dwdy, dwdz

        real :: traceSqr
        !! Velocity gradient trace squared

        real :: crossTerm_xy
        !! cross term xy

        real :: crossTerm_xz
        !! cross term xz

        real :: crossTerm_yz
        !! cross term yz
    !------------------------------------------------------------------------------------------------------------------

        do iElement=1,numberOfElements

            vol = mesh%volume(iElement)
            mu = nu%phi(iElement,1)

            dudx = velocity%phiGrad(iElement,1,1)
            dvdy = velocity%phiGrad(iElement,2,2)
            dwdz = velocity%phiGrad(iElement,3,3)

            dudy = velocity%phiGrad(iElement,2,1)
            dudz = velocity%phiGrad(iElement,3,1)

            dvdx = velocity%phiGrad(iElement,1,2)
            dvdz = velocity%phiGrad(iElement,3,2)

            dwdx = velocity%phiGrad(iElement,1,3)
            dwdz = velocity%phiGrad(iElement,1,3)

            traceSqr = (dudx + dvdy + dwdz)**2
            crossTerm_xy = dudy+dvdx
            crossTerm_xz = dudz+dwdx
            crossTerm_yz = dvdz+dwdy

            this%bC(iElement,1) = this%bC(iElement,1) + mu*(2.0*traceSqr+crossTerm_xy**2 + crossTerm_xz**2 + crossTerm_yz**2)*vol

        end do

    end subroutine addDissipationTerm

! *********************************************************************************************************************

    subroutine addViscousDissipationTerm(this)

        class(energyEqn):: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
        !! target cell
    !------------------------------------------------------------------------------------------------------------------

        real :: vol
        !! cell volume

        real :: mu
        !! cell center viscosity

        real :: dudx, dudy, dudz

        real :: dvdx, dvdy, dvdz

        real :: dwdx, dwdy, dwdz

        real :: traceSqr
        !! Velocity gradient trace squared
    !------------------------------------------------------------------------------------------------------------------

        do iElement=1,numberOfElements

            vol = mesh%volume(iElement)
            mu = nu%phi(iElement,1)

            dudx = velocity%phiGrad(iElement,1,1)
            dvdy = velocity%phiGrad(iElement,2,2)
            dwdz = velocity%phiGrad(iElement,3,3)

            traceSqr = (dudx + dvdy + dwdz)**2

            this%bC(iElement,1) = this%bC(iElement,1) + traceSqr*vol

        end do

    end subroutine addViscousDissipationTerm

! *********************************************************************************************************************

    subroutine addSpecificHeatTerm(this)

        use operators, only : totalDerivative

        class(energyEqn):: this
    !------------------------------------------------------------------------------------------------------------------

        real :: DspecificHeatDt(numberOfElements,1)
        !! substantial derivative
    !------------------------------------------------------------------------------------------------------------------

        DspecificHeatDt = totalDerivative(specificHeat)
        this%bC(:,1) = this%bC(:,1) + &
                       rho%phi(1:numberOfElements,1)*temperature%phi(1:numberOfElements,1)*DspecificHeatDt(:,1)*mesh%volume

    end subroutine addSpecificHeatTerm

! *********************************************************************************************************************

    subroutine addDPDt(this)

        use operators, only : totalDerivative

        class(energyEqn):: this
    !------------------------------------------------------------------------------------------------------------------

        real :: DPDt(numberOfElements,1)
        !! substantial derivative
    !------------------------------------------------------------------------------------------------------------------

        DPDt = totalDerivative(pressure)
        this%bC(:,1) = this%bC(:,1) + DPDt(:,1)*mesh%volume

    end subroutine addDPDt

! *********************************************************************************************************************

end module energyEq
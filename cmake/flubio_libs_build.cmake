
#######
# MPI #
#######

# - mpi
add_subdirectory("${flubio_src}/mpi")

################
# FLUBIO Types #
################

add_subdirectory("${flubio_src}/include/userDefinedTypes")

######
# IO #
######

# global varaibles
add_subdirectory("${flubio_src}/include/globalVar")

# - strings
add_subdirectory("${flubio_src}/IO/strings")

# - qcontainers
add_subdirectory("${flubio_src}/IO/qcontainers_f")

# - flubioDicts
add_subdirectory("${flubio_src}/IO/flubioDicts")
add_subdirectory("${flubio_src}/include/flubioDictionaries")

# - flubioSettings
add_subdirectory("${flubio_src}/IO")

#############
# Utilities #
#############

# - fortranExpressionEvaluator
add_subdirectory("${flubio_src}/utilities/fortranExpressionEvaluator")
add_subdirectory("${flubio_src}/utilities/fortranExpressionEvaluator/parse")

# - kdtree
add_subdirectory("${flubio_src}/utilities/kdtree")

# - vecfor
add_subdirectory("${flubio_src}/utilities/vecfor")

# - math
add_subdirectory("${flubio_src}/utilities/math")

# - velocityTensors
add_subdirectory("${flubio_src}/utilities/velocityTensors/")

# - runTimeObjects
add_subdirectory("${flubio_src}/utilities/runTimeObjects/")

# - rigid body dynamics
add_subdirectory("${flubio_src}/utilities/rigidBodyDynamics/")

########
# Mesh #
########

add_subdirectory("${flubio_src}/mesh")
add_subdirectory("${flubio_src}/include/meshvar")


##############
# Boundaries #
##############

add_subdirectory("${flubio_src}/bcs")

##########
# Fields #
##########

add_subdirectory("${flubio_src}/fields")
add_subdirectory("${flubio_src}/include/massFlowVar")
add_subdirectory("${flubio_src}/include/fieldvar")

######################
# Physical constants #
######################

add_subdirectory("${flubio_src}/include/physicalConstants")
add_subdirectory("${flubio_src}/thermophysicalModels/")

#################
# Interpolation #
#################

add_subdirectory("${flubio_src}/interpolation")

#############
# Gradients #
#############

add_subdirectory("${flubio_src}/gradients")

##################
# Discretization #
##################

add_subdirectory("${flubio_src}/discretization")

#############
# Equations #
#############

add_subdirectory("${flubio_src}/equations/transportEq")
add_subdirectory("${flubio_src}/equations/energyEq")
add_subdirectory("${flubio_src}/equations/momentumEq")
add_subdirectory("${flubio_src}/equations/pressureEq")

add_subdirectory("${flubio_src}/include/equations/transportEquation")
add_subdirectory("${flubio_src}/include/equations/momentumEquation")
add_subdirectory("${flubio_src}/include/equations/pressureCorrectionEquation")

add_subdirectory("${flubio_src}/equations/burgersEqn")
add_subdirectory("${flubio_src}/equations/poissonEqn")
add_subdirectory("${flubio_src}/equations/potentialEqn")
add_subdirectory("${flubio_src}/equations/convectionDiffusionEqn")

add_subdirectory("${flubio_src}/include/equations/equationRegistry")

add_subdirectory("${flubio_src}/equations/amgcl/")

#####################
# Turbulence Models #
#####################

add_subdirectory("${flubio_src}/turbulence/wallDist")
add_subdirectory("${flubio_src}/turbulence/baseModel")
add_subdirectory("${flubio_src}/turbulence/DNS")
add_subdirectory("${flubio_src}/turbulence/LES")
add_subdirectory("${flubio_src}/turbulence/RANS")

add_subdirectory("${flubio_src}/include/turbulenceModels")

#####################
# Immersed boundary #
#####################

add_subdirectory("${flubio_src}/immersedBoundaries/MLS")
add_subdirectory("${flubio_src}/include/ibm")

###################
# Solvers builder #
###################

add_subdirectory("${flubio_src}/solversBuild")

#################
# Navier-Stokes #
#################

add_subdirectory("${flubio_src}/NavierStokes")

####################
# FLUBIO UTILITIES #
####################

add_subdirectory("${flubio_src}/pre_post_proc/orderPack")
add_subdirectory("${flubio_src}/pre_post_proc/common")
add_subdirectory("${flubio_src}/pre_post_proc/formats")
add_subdirectory("${flubio_src}/pre_post_proc/mesh")
add_subdirectory("${flubio_src}/pre_post_proc/mesh/include")
add_subdirectory("${flubio_src}/pre_post_proc/meshPartitioning")

add_subdirectory("${flubio_src}/pre_post_proc/parallelPost/formats")

#-----------------------------
# Installation
#-----------------------------

include( ${cmake_local}/flubio_libs_install.cmake )


set( home_dir_flubio $ENV{home_dir_flubio} )

set( install_dir_flubio      ${home_dir_flubio}/0---install )
set( install_dir_flubio_mod  ${home_dir_flubio}/0---install/mod )

set( flubio_src  ${home_dir_flubio}/src )

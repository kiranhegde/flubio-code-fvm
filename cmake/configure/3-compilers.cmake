
if( $ENV{build_type_flubio} STREQUAL "Debug" )
  set( CMAKE_BUILD_TYPE Debug )
endif()

set( CMAKE_CXX_STANDARD 17 )

enable_language( Fortran )

set( flags  "")
set( flags  "${flags}  -cpp ")
set( flags  "${flags}  -mcmodel=medium ")
set( flags  "${flags}  -ffree-line-length-512 ")
set( flags  "${flags}  -fdefault-real-8 ")

if( CMAKE_BUILD_TYPE STREQUAL "Debug" )
    message(STATUS "Compiling in DEBUG mode.")
    set( flags  "${flags}  -Og ")
    set( flags  "${flags}  -g ")
    set( flags  "${flags}  -fcheck=all ")
    set( CMAKE_CXX_FLAGS  "-Og -g" )
else()
    message( STATUS "Compiling in RELEASE mode." )
    set( flags  "${flags}  -ffast-math ")
    set( flags  "${flags}  -O3 ")
    set( CMAKE_CXX_FLAGS "-O3" )
endif()

set( CMAKE_Fortran_FLAGS  ${flags} )
